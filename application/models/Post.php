<?php

/**
 * Description of Post
 *
 * @author jon
 */
class Application_Model_Post {

    public $title;
    public $link;
    public $dateModified;
    public $dateCreated;
    public $description;
    public $content;


    public static function getUrl() {
        return 'http://www.gkszukowo.com/';
    }

    public static function getPosts() {
        // this would normally hit a database
        $posts = array();
        $Article = new Application_Model_DbTable_Article();
        $article = $Article->getArticlesWithFoto();

        foreach ($article as $post) {
            $posts[] = Application_Model_Post::create($post['title'], $post['text'], $post['article_id'],$post['dtime']);
        }
//        die(var_dump($posts));
        return $posts;
    }

    public static function create($title, $content, $article_id, $dtime) {
        $p = new Application_Model_Post();
        $p->content = $content;
        $description = str_split($content, 50);
        $p->description = $description[0] . '...';
        $p->link = self::getUrl() . strtolower('index/show/article_id/' . $article_id);
        $p->title = $title;
        $date = new Zend_Date($dtime);
        $p->dateCreated = $date->toValue();
        $p->dateModified = $date->toValue();
        return $p;
    }

    public function getFieldsAsArray() {
        $properties = get_class_vars(get_class($this));
        $fields = array();
        foreach ($properties as $property => $defaultValue)
            $fields[$property] = $this->$property;
        return $fields;
    }

}

