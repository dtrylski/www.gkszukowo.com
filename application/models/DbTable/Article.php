<?php

class Application_Model_DbTable_Article extends Zend_Db_Table_Abstract {

    protected $_name = 'article';

//    protected $_dependentTables = array('Application_Model_DbTable_Foto');

    public function getArticlesWithFoto() {
        $select = $this->select()
                ->from(array('a' => 'article'))
                ->joinLeft(array('f' => 'foto'), 'a.article_id = f.article_id',
                           array('name' => new Zend_Db_Expr('GROUP_CONCAT(f.name)') , 
                           'foto_id' => new Zend_Db_Expr('GROUP_CONCAT(f.foto_id)')))
                ->group('a.article_id')
                ->order('dtime DESC')
                ->setIntegrityCheck(false);
        $articles = $this->fetchAll($select)->toArray();
        
        return $articles;
    }
    public function getArticlesWithFotobyId($id) {
        $select = $this->select()
                ->from(array('a' => 'article'))
                ->joinLeft(array('f' => 'foto'), 'a.article_id = f.article_id',array('foto_id' => 'foto_id' , 'names' => new Zend_Db_Expr('GROUP_CONCAT(f.name)')))
                ->group('a.article_id')
                ->where('a.article_id=?',$id)
                ->setIntegrityCheck(false);
        $articles = $this->fetchRow($select)->toArray();

        return $articles;
    }

    public function getNews($id) {

        $id = (int) $id;
        $row = $this->fetchRow('article_id = ' . $id);
        if (!$row) {
            throw new Exception("Could not find row $id");
        }
        return $row->toArray();
    }

    public function addNews($title, $text,$article_lead) {

        $data = array(
            'dtime' => new Zend_Db_Expr('NOW()'),
            'title' => $title,
            'article_lead' => $article_lead,
            'text' => $text,
        );
        $this->insert($data);
    }

    public function updateNews($id, $artist, $title,$article_lead) {
        $data = array(
            'title' => $artist,
            'article_lead' => $article_lead,
            'text' => $title,
        );
        $this->update($data, 'id = ' . (int) $id);
    }

    public function deleteNews($id) {
        $this->delete('id =' . (int) $id);
    }

}
