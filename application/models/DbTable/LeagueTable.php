<?php

class Application_Model_DbTable_LeagueTable extends Zend_Db_Table_Abstract {

    protected $_name = 'league_table14';

    public function getTable() {
        $select = $this->select()
                ->from('league_table14')
                ->order(array('points DESC','goals_scored DESC','goals_allowed ASC'));
        $table = $this->fetchAll($select)->toArray();
        return $table;
    }
    public function fillupTable($array){
//        die(var_dump($array));
        foreach ($array as $k => $v) {
        $data = array(
            'points' => $v['points'],
            'wins' => $v['wins'],
            'losses' => $v['losses'],
            'ties' => $v['ties'],
            'goals_scored' => $v['goals_scored'],
            'goals_allowed' => $v['goals_allowed'],
        );
        $where = array('team = ?' =>$k);
//        die(var_dump($where));
        $this->update($data, $where);
        }
        
        
    }
     

}

