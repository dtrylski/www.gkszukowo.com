<?php

class Application_Model_DbTable_Player extends Zend_Db_Table_Abstract {

    protected $_name = 'player';

    public function getPlayerbyId($id) {
        $select = $this->select()
                ->from(array('p' => 'player'))
                ->join(array('po' => 'position'), 'p.position = po.position_id', 'position')
                ->join(array('pts' => 'player_to_season'), 'p.player_id = pts.player_id')
                ->join(array('s' => 'season'), 's.season_id = pts.season_id')
                ->where('p.player_id=?', $id)
                ->where('s.season_id=?', '1')
                ->where('p.status=1')
                ->order('p.sname')
                ->setIntegrityCheck(false);
        $player = $this->fetchRow($select);
        $objp = new My_League_Player(
                $player['player_id'], $player['fname'], $player['sname'], $player['position'], $player['email'], $player['height'], $player['games'], $player['number'], $player['status'], $player['season_id'], $player['season']);
        return $objp;
    }

    public function getAllplayers() {
        $select = $this->select()
                ->from(array('p' => 'player'))
                ->join(array('po' => 'position'), 'p.position = po.position_id', 'position')
                ->join(array('pts' => 'player_to_season'), 'p.player_id = pts.player_id')
                ->join(array('s' => 'season'), 's.season_id = pts.season_id')
                ->where('s.season_id=?', '1')
                ->where('p.status=1')
                ->order('p.sname')
                ->setIntegrityCheck(false);
        $players = $this->fetchAll($select)->toArray();
        return $players;
    }

}