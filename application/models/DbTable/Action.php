<?php

class Application_Model_DbTable_Action extends Zend_Db_Table_Abstract
{

    protected $_name = 'action';

    public function userHasAccess($identity = '', $module = '', $controller = '', $action = '')
    {
        $select = $this->select()
            ->where('module = ?', $module)
            ->where('controller = ?', $controller)
            ->where('action = ?', $action);

        $action = $this->fetchRow($select);
        
        if (!$action) {
            return false;
        }

        if (!$action['is_secure']) {
            return true;
        }

        if (!$identity) {
            return false;
        }

        $User = new Application_Model_DbTable_User();
//        die(var_dump($identity));
        $select = $User->select()->where('username = ?', $identity);
        
        $u = $User->fetchRow($select);
//die(var_dump($u));
        if (!$u) {
            return false;
        }

        $ahu = new Application_Model_DbTable_ActionHasUser();
        $select = $ahu->select()
            ->where('action_id = ?', $action['action_id'])
            ->where('user_id = ?', $u['user_id']);

        $row = $ahu->fetchRow($select);

        return (bool)$row;
    }

}