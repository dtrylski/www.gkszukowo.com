<?php

class Application_Model_DbTable_Schedule extends Zend_Db_Table_Abstract {

    protected $_name = 'schedule';

    public function getGames() {
        $select = $this->select()
                ->from(array('sc' => 'schedule'))
                ->join(array('se' => 'season'), 'sc.season_id = se.season_id', array('season_id' => 'se.season'))
                ->join(array('lta' => 'teams'), 'sc.host_id = lta.position_id', array('host_id' => 'lta.team','host_thmb'=> 'thumbnail'))
                ->join(array('ltb' => 'teams'), 'sc.visitor_id = ltb.position_id', array('visitor_id' => 'ltb.team','visitor_thmb'=> 'thumbnail'))
                ->setIntegrityCheck(false)
                ->where('sc.season_id >= ?', 2);
        $schedule = $this->fetchAll($select)->toArray();
//        die(var_dump($schedule));
        return $schedule;
    }
    public function getNextRound(){
        $date = new Zend_Date($locale = "pl_PL");
        $date->now();
//        die(var_dump($date->get()));
        $select = $this->select()
                ->from(array('sc' => 'schedule'))
                ->join(array('se' => 'season'), 'sc.season_id = se.season_id', array('season_id' => 'se.season'))
                ->join(array('lta' => 'teams'), 'sc.host_id = lta.position_id', array('host_id' => 'lta.team','host_thmb'=> 'thumbnail'))
                ->join(array('ltb' => 'teams'), 'sc.visitor_id = ltb.position_id', array('visitor_id' => 'ltb.team','visitor_thmb'=> 'thumbnail'))
                ->setIntegrityCheck(false)
                ->where('date >= ?', $date->toString('YYYY-MM-dd'))
                ->limit('7');
        $schedule = $this->fetchAll($select)->toArray();
        return $schedule;
    }

    public function addGame() {
        $data = array(
            'dtime' => new Zend_Db_Expr('NOW()'),
            'title' => $title,
            'article_lead' => $article_lead,
            'text' => $text,
        );
        $this->insert($data);
    }

    public function getMygames() {
        $select = $this->select()
                ->from(array('sc' => 'schedule'))
                ->join(array('se' => 'season'), 'sc.season_id = se.season_id', array('season_id' => 'se.season'))
                ->join(array('lta' => 'teams'), 'sc.host_id = lta.position_id', array('host_id' => 'lta.team'))
                ->join(array('ltb' => 'teams'), 'sc.visitor_id = ltb.position_id', array('visitor_id' => 'ltb.team'))
                ->join(array('ltc' => 'teams'), array('thumbnail' => 'ltc.thumbnail'))
                ->where('')
                ->setIntegrityCheck(false);
        $schedule = $this->fetchAll($select)->toArray();
        return $schedule;
    }
}

