<?php

class Zend_View_Helper_Menu extends Zend_View_Helper_Abstract {

    public function menu() {
        $Menu = new Application_Model_DbTable_Menu();
        $select = $Menu->select()
                ->from(array('m'=>'menu'), array('title'=> new Zend_Db_Expr('GROUP_CONCAT(m.title)'),'numer'))
                ->group('numer');   
        $menu = $Menu->fetchAll($select);
        $menu = $menu->toArray();
        $auth = Zend_Auth::getInstance();
        $identity = $auth->getIdentity();
        return $this->view->partial('menu.phtml', array('menu' => $menu, 'identity'=>$identity));
    }

}