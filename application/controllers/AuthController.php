<?php

class AuthController extends Zend_Controller_Action {

    public function indexAction() {
        $this->view->form = new Application_Form_Login();
    }

    public function loginAction() {
        $this->_helper->viewRenderer('index');
        $form = new Application_Form_Login();
        
        if ($form->isValid($this->getRequest()->getPost())) {

            $adapter = new Zend_Auth_Adapter_DbTable(
                            null,
                            'user',
                            'username',
                            'pass',
                            'SHA1(CONCAT(salt, ?, salt))'
            );

            
            $adapter->setIdentity($form->getValue('username'));
            $adapter->setCredential($form->getValue('pass'));

            $auth = Zend_Auth::getInstance();

            $result = $auth->authenticate($adapter);

            if ($result->isValid()) {
                return $this->_helper->redirector(
                                'index',
                                'admin',
                                'default'
                );
            }
            $form->password->addError('Błędna próba logowania!');
        }
        $this->view->form = $form;
    }

    public function logoutAction() {
        $auth = Zend_Auth::getInstance();
        $auth->clearIdentity();
        return $this->_helper->redirector(
                        'index', 'index'
        );
    }

    public function unauthorizedAction() {
        $this->view->identity = false;
        $auth = Zend_Auth::getInstance();
        if ($auth->hasIdentity()) {
            $this->view->identity = $auth->getIdentity();
        }
    }

}