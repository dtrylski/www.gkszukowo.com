<?php
class UserController extends My_Crud_Auth_Controller
{

    public function init()
    {
        $this->_db_table_class = 'Application_Model_DbTable_User';
        $this->_form_class = 'Application_Form_User';
    }

}
?>