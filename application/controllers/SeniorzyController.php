<?php

class SeniorzyController extends My_Crud_Auth_Controller {

    public function init() {
        /* Initialize action controller here */
    }

    public function indexAction() {
        $players = new My_League_Players();
        $player_arr = $players->playersposition();
        array_unshift($player_arr, 'Drużyna');

        $this->view->players = $players->getplayers();
        $form = new Application_Form_Players(array(
                    'players' => $player_arr));
        $this->view->form = $form;

        $schedule = new My_League_Schedule();
        $allrounds = $schedule->getAllrounds();
        $nextround = $schedule->nextRound();
        
//        die(var_dump($allrounds[$id]));
        if (isset($id['round_id'])) {
            $this->view->object = $allrounds[$id['round_id']];
        } else {
//            die(var_dump($nextround));
            $id = $nextround[0]['round_id'];
            $this->view->object = $nextround;
        }
//        $form = new Application_Form_Round(array(
//                    'schedule' => $schedule->getkeyRounds(), 'id' => $id));
//        $this->view->form = $form;
    }

    public function ajaxAction() {
        //get post request (standart approach)
        $request = $this->getRequest()->getPost();
        //referring to the index
        //gets value from ajax request
        $message = $request['position'];
        // makes disable renderer

        $this->_helper->viewRenderer->setNoRender();

        //makes disable layout
        $this->_helper->getHelper('layout')->disableLayout();

        //return callback message to the function javascript
        echo $message;
    }

}