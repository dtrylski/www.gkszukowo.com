<?php

class AdminController extends My_Crud_Auth_Controller {

    public $title;
    public $link;
    public $dateModified;
    public $dateCreated;
    public $description;
    public $content;

    public function init() {
        /* Initialize action controller here */
    }

    public function indexAction() {
        $Article = new Application_Model_DbTable_Article();
        $article = $Article->getArticlesWithFoto();
        $this->view->article = $article;

//        $article = new Application_Model_DbTable_Article();
//        $article->findApplication_Model_DbTable_Foto;
//        $article->findApplication_Model_DbTable_Foto;
//        $article->findApplication_Model_DbTable_Foto;
//        die(var_dump($article));
    }


    public function addAction() {

        $form = new Application_Form_News();
        $this->view->form = $form;
        if ($this->getRequest()->isPost()) {

            $formData = $this->getRequest()->getPost();

            if ($form->isValid($formData)) {
                $title = $form->getValue('title');
                $text = $form->getValue('text');

                $article_lead = My_Slugs::cutText($text);
                $news = new Application_Model_DbTable_Article();
                $news->addNews($title, $text, $article_lead);
                $this->_helper->redirector('index');
            } else {
                $form->populate($formData);
            }
        }
    }

    public function editAction() { {
            $id = $this->getRequest()->getParam('article_id');
            $DbTable = new Application_Model_DbTable_Article();
            $obj = $DbTable->find($id)->current();
            if (!$obj) {
                throw new Zend_Controller_Action_Exception('Błędny adres!', 404);
            }
            $this->view->form = new Application_Form_News();
            $this->view->form->populate($obj->toArray());
            $url = $this->view->url(array('action' => 'update', 'id' => $id));
            $this->view->form->setAction($url);
            $this->view->object = $obj;
        }
    }

    public function updateAction() {
        $id = $this->getRequest()->getParam('article_id');

        $DbTable = new Application_Model_DbTable_Article();
        $obj = $DbTable->find($id)->current();
        if (!$obj) {
            throw new Zend_Controller_Action_Exception('Błędny adres!', 404);
        }

        if ($this->getRequest()->isPost()) {
            $form = new Application_Form_News();
            if ($form->isValid($this->getRequest()->getPost())) {
                $data = $form->getValues();
                $data["article_lead"] = My_Slugs::cutText($data["text"]);
                $obj->setFromArray($data);
                $obj->save();
                return $this->_helper->redirector('index');
            }
            $this->view->form = $form;
        } else {
            throw new Zend_Controller_Action_Exception('Błędny adres!', 404);
        }
    }

    public function deleteAction() {
        $id = $this->getRequest()->getParam('article_id');
        $DbTable = new Application_Model_DbTable_Article();
        $obj = $DbTable->find($id)->current();
        if (!$obj) {
            throw new Zend_Controller_Action_Exception('Błędny adres!', 404);
        }
        $obj->delete();
        return $this->_helper->redirector('index');
    }

    public function formfotoAction() { {
            $this->view->form = new Application_Form_Foto();
            $url = $this->view->url(array('action' => 'savefoto'));
            $this->view->form->setAction($url);
        }
    }

    public function savefotoAction() {

        if ($this->getRequest()->isPost()) {
            $form = new Application_Form_Foto();
            if ($form->isValid($this->getRequest()->getPost())) {

                $dane = $form->getValues();
                $article_id = $this->getRequest()->getParam('article_id');
                $dane['article_id'] = $article_id;

                $Foto = new Application_Model_DbTable_Foto();
                $Foto->insert($dane);

                $fld = realpath(APPLICATION_PATH . '/../public/uploads');

                My_Thumbnail::gdThumbnailFile(
                        $fld . '/' . $dane['name'], 75, 75, $fld . '/mini/' . $dane['name']
                );
                My_Thumbnail::gdThumbnailFile(
                        $fld . '/' . $dane['name'], 400, 300, $fld . '/popup/' . $dane['name']
                );
                My_Thumbnail::gdThumbnailFile(
                        $fld . '/' . $dane['name'], 800, 600, $fld . '/show/' . $dane['name']
                );

                return $this->_helper->redirector('index');
            }
            $this->view->form = $form;
        } else {
            throw new Exception('Błędny adres!', 404);
        }
    }

    public function deletefotoAction() {
        $id = $this->getRequest()->getParam('foto_id');
        $DbTable = new Application_Model_DbTable_Foto();
        $obj = $DbTable->find($id)->current();
        if (!$obj) {
            throw new Zend_Controller_Action_Exception('Błędny adres!', 404);
        }

        $fld = realpath(APPLICATION_PATH . '/../public/uploads');

        $np1 = realpath($fld . '/' . $obj['name']);
        $np2 = realpath($fld . '/mini/' . $obj['name']);
        $np3 = realpath($fld . '/show/' . $obj['name']);
        $np4 = realpath($fld . '/popup/' . $obj['name']);
        unlink($np1);
        unlink($np2);
        unlink($np3);
        unlink($np4);

        $obj->delete();

        return $this->_helper->redirector('index');
    }

    /* wyswietlamy kolejki */

    public function scheduleAction() {
        $schedule = new My_League_Schedule();
        $allrounds = $schedule->getAllrounds();
        $id = $this->getRequest()->getParams();
//        die(var_dump($id));

        $form = new Application_Form_Round(array(
                    'schedule' => $schedule->getkeyRounds()));
        $this->view->form = $form;
        $url = $this->view->url(array('action' => 'schedule'));
        $this->view->form->setAction($url);

//        die(var_dump($allrounds[$id]));
        if (isset($id['round_id'])) {
            $this->view->object = $allrounds[$id['round_id']];
        } else {
            $this->view->object = end($allrounds);
        }
    }

    /* Pobieramy mecz do edycji */

    public function editgameAction() {
        $id = $this->getRequest()->getParam('round_id');
        $schedule = new My_League_Schedule();
        $allrounds = $schedule->getAllrounds();
//        die(var_dump($obj->toArray()));
        if (!$allrounds) {
            throw new Zend_Controller_Action_Exception('Błędny adres!', 404);
        }
//        die(var_dump($obj->toArray()));
        $this->view->form = new Application_Form_Game();
        $url = $this->view->url(array('action' => 'updategame'));
        $this->view->form->setAction($url);
        $this->view->object = $allrounds[$id];
    }

    public function updategameAction() {
        $games = $this->getRequest()->getPost();
        $game = array();
        array_pop($games);
//        die(var_dump($games));
        foreach ($games as $k => $v) {
            $game_id = preg_replace("/[^0-9]/", '', $k);
            $team = preg_replace("/[^a-z_]/", '', $k);
            if (!isset($game[$game_id][$team])) {
                $game[$game_id][$team] = $v;
                $game[$game_id]['game_id'] = $game_id;
            }
        }
        $DbTable = new Application_Model_DbTable_Schedule();
//        die(var_dump($obj));
        foreach ($game as $k => $v) {
            $obj = $DbTable->find($k)->current();
            $obj->setFromArray($v);
            $obj->save();
        }
        $table = new Application_Model_DbTable_LeagueTable();
        $schedule = new My_League_Schedule();
        $table->fillupTable($schedule->countGames());
    }

}

