<?php

class IndexController extends My_Crud_Auth_Controller {

    public function init() {
        /* Initialize action controller here */
        $this->_helper->ajaxContext->addActionContext('index', 'html')
                ->initContext();
    }

    public function indexAction() {
        $Article = new Application_Model_DbTable_Article();
        $articles = $Article->getArticlesWithFoto();
        $slides = array_slice($articles, 0, 3);
        $this->view->article = $articles;
        $this->view->slides = $slides;

        $paginator = new Zend_Paginator(new Zend_Paginator_Adapter_Array($articles));
        $paginator->setCurrentPageNumber(1);
        $paginator->setDefaultItemCountPerPage(5);
        $paginator->setCurrentPageNumber($this->_getParam('page'));
        $this->view->paginator = $paginator;

        $table = new Application_Model_DbTable_LeagueTable();
        $this->view->table = $table->getTable();

        $schedule = new My_League_Schedule();
        $this->view->lastnext = $schedule->LastNextGame();
        
//        $m = sha1('handball123');
//        die(var_dump($m));
//        die(var_dump($salt->getSalt2()));
    }

    public function showAction() {
        $Article = new Application_Model_DbTable_Article();

        $id = $this->getRequest()->getParam('article_id');
        $this->view->article = $Article->getArticlesWithFotobyId($id);

        $table = new Application_Model_DbTable_LeagueTable();
        $this->view->table = $table->getTable();

        $schedule = new My_League_Schedule();
        $this->view->lastnext = $schedule->LastNextGame();

        $schedule = new My_League_Schedule();
        $this->view->lastnext = $schedule->LastNextGame();

        if (!$this->view->article) {
            throw new Zend_Controller_Action_Exception(sprintf('MenuController/showAction: błędne id: "%s" ', $id), 404);
        }
    }

}

