<?php
class FeedController extends Zend_Controller_Action {

    public function init() {
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->getHelper('layout')->disableLayout();
    }

    private function getFeed() {
        $feed = new Zend_Feed_Writer_Feed();
        $feed->setTitle('GKS Żukowo - Piłka Ręczna 2 Liga');
        $feed->setLink(Application_Model_Post::getUrl().'feed/rss');
        $feed->setDescription('GKS Żukowo 2 Liga piłki ręcznej');
        $feed->setImage(array('uri'=>'http://www.gkszukowo.com/images/handball_zukowo.png','link'=>'http://www.gkszukowo.com','title'=>'GKS Żukowo'));

        foreach(Application_Model_Post::getPosts() as $post)
        {
//            die(var_dump($post));
            $entry = $feed->createEntry();
            foreach($post->getFieldsAsArray() as $field => $value)
            {
                $method = 'set'.ucfirst($field);
                $entry->$method($value);
            }
            $feed->addEntry($entry);
        }
        return $feed;
    }

    public function rssAction() {
        $feed = $this->getFeed();
        $feed->setFeedLink(Application_Model_Post::getUrl() . 'feed/rss', 'rss');
        echo $feed->export('rss');
    }

    public function atomAction() {
        $feed = $this->getFeed();
        $feed->addAuthor("Daniel Trylski", "gkszukowo@gmail.com", "http://www.gkszukowo.com");
        $feed->setFeedLink(Application_Model_Post::getUrl() . 'feed/atom', 'atom');
        $feed->export('atom');
    }

}

?>
