<?php

class NewsController extends My_Crud_Auth_Controller {

    public function init() {
        /* Initialize action controller here */
    }

    public function indexAction() {
        $news = new Application_Model_DbTable_Article();
        $this->view->news = $news->getArticlesWithFoto();

//        $article = new Application_Model_DbTable_Article();
//        $article->findApplication_Model_DbTable_Foto;
//        die(var_dump($article));
    }

    public function addAction() {

        $form = new Application_Form_News();

        $this->view->form = $form;
        if ($this->getRequest()->isPost()) {

            $formData = $this->getRequest()->getPost();

            if ($form->isValid($formData)) {
                $title = $form->getValue('title');
                $text = $form->getValue('text');
//                die(var_dump($text));
                $news = new Application_Model_DbTable_Article();
                $news->addNews($title, $text);
                $this->_helper->redirector('index');
            } else {
                $form->populate($formData);
            }
        }
    }

    public function formfotoAction() { {
            $this->view->form = new Application_Form_Foto();
            $url = $this->view->url(array('action' => 'savefoto'));
            $this->view->form->setAction($url);
        }
    }

    public function savefotoAction() {

        if ($this->getRequest()->isPost()) {
            $form = new Application_Form_Foto();
            if ($form->isValid($this->getRequest()->getPost())) {

                $dane = $form->getValues();
                $article_id = $this->getRequest()->getParam('article_id');
                $dane['article_id'] = $article_id;

                $Foto = new Application_Model_DbTable_Foto();
                $Foto->insert($dane);

                $fld = realpath(APPLICATION_PATH . '/../public/uploads');

                My_Thumbnail::gdThumbnailFile(
                        $fld . '/' . $dane['name'], 150, 100, $fld . '/mini/' . $dane['name']
                );
                My_Thumbnail::gdThumbnailFile(
                        $fld . '/' . $dane['name'], 400, 300, $fld . '/popup/' . $dane['name']
                );
                My_Thumbnail::gdThumbnailFile(
                        $fld . '/' . $dane['name'], 800, 600, $fld . '/show/' . $dane['name']
                );

                return $this->_helper->redirector('index');
            }
            $this->view->form = $form;
        } else {
            throw new Exception('Błędny adres!', 404);
        }
    }

    public function editAction() {
        
    }

    public function deletefotoAction() { {
            $id = $this->getRequest()->getParam('foto_id');
            $DbTable = new Application_Model_DbTable_Foto();
            $obj = $DbTable->find($id)->current();
            if (!$obj) {
                throw new Zend_Controller_Action_Exception('Błędny adres!', 404);
            }

            $fld = realpath(APPLICATION_PATH . '/../public/uploads');

            $np1 = realpath($fld . '/' . $obj['name']);
            $np2 = realpath($fld . '/mini/' . $obj['name']);
            $np3 = realpath($fld . '/show/' . $obj['name']);
            $np4 = realpath($fld . '/popup/' . $obj['name']);
            unlink($np1);
            unlink($np2);
            unlink($np3);
            unlink($np4);

            $obj->delete();

            return $this->_helper->redirector('index');
        }
    }

}

