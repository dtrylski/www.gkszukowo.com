<?php

class Application_Form_Players extends Zend_Form {
    
    private $players;
    
    public function init() {
        $this->setAttrib('name', 'position');
        $this->addElement('select','position',array(
                    'label' => 'Pozycja w grze:',
                    'multiOptions' => $this->players
                ));
        $this->setMethod('post');
//        $this->addElement('submit', 'Wybierz');
    }
    public function setplayers($players) {
    $this->players = $players;
  }

}