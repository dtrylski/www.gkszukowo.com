<?php

class Application_Form_Round extends Zend_Form {

    private $schedule;
    private $id;

    public function init() {
        $this->addElement('select', 'round_id', array(
            'multiOptions' => $this->schedule,
            'label' => 'Kolejka',
            'value' => $this->id
        ));
        $this->clearDecorators();
        $this->addDecorator('FormElements')
                ->addDecorator('HtmlTag', array('tag' => 'div'))
                ->addDecorator('Form');
        $this->setElementDecorators(array(
            array('ViewHelper'),
            array('Errors'),
            array('Label'),
            array('HtmlTag', array('tag' => 'div', 'class' => 'element-group'))
        ));

        $this->setMethod('post');
    }

    public function setschedule($schedule) {
        $this->schedule = $schedule;
    }

    public function setid($id) {
        $this->id = $id;
    }

}