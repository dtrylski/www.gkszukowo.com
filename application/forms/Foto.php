<?php

class Application_Form_Foto extends Zend_Form
{

  public function init()
  {
    $this->setMethod('post');

    $element = new Zend_Form_Element_File('name', array('required' => true));
    $element
      ->setLabel('Dodaj zdjęcie:')
      ->setDestination(realpath(APPLICATION_PATH . '/../public/uploads'))
      ->addValidator('NotEmpty', true)
      ->addValidator('Count', true, 1)
      ->addValidator('Size', true, 204800)
      ->addValidator('NotExists', realpath(APPLICATION_PATH . '/../public/uploads'))
      ->addValidator('Extension', false, 'jpg,png,gif');
    $this->addElement($element, 'name');

    $this->name->getValidator('NotExists')->setMessages(array(
      Zend_Validate_File_NotExists::DOES_EXIST => "foto '%value%' już istnieje!",
    ));
    $this->name->getValidator('Upload')->setMessages(array(
      Zend_Validate_File_Upload::NO_FILE => "Nazwa foto nie może być pusta!",
    ));
    $this->name->getValidator('Size')->setMessages(array(
      Zend_Validate_File_Size::TOO_BIG => "Maksymalny...",
    ));

    $this->addElement('submit', 'submit', array(
      'label' => 'Zapisz',
    ));
  }

}