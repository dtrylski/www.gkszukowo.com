<?php

class Application_Form_Game extends Zend_Form {

    public $games;

    public function init() {
        $this->setMethod('post');

        $this->addElement('submit', 'Wybierz');

    }

    public function setGames($games) {
        $this->games = $games;
    }

}