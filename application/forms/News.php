<?php

class Application_Form_News extends Zend_Form {

    public function init() {
        $this->addPrefixPath('Sozfo_Form_Element_', 'Sozfo/Form/Element/', Zend_Form::ELEMENT);
        $this->setMethod('post');
        $this->addElement('text', 'title', array('label' => 'Tytuł:'));
        $this->addElement('tinyMce', 'text', array(
            'required' => true,
            'cols' => '50',
            'rows' => '10',
            'editorOptions' => new Zend_Config_Ini(APPLICATION_PATH . '/configs/tinymce.ini', 'moderator')));
        $this->addElement('submit', 'Dodaj');


        $this->setElementDecorators(array(
            'ViewHelper',
            array(array('data' => 'HtmlTag'), array('tag' => 'td', 'class' => 'element')),
            array('Label', array('tag' => 'td')),
            array(array('row' => 'HtmlTag'), array('tag' => 'tr'))
        ));

        $this->setDecorators(array(
            'FormElements',
            array('HtmlTag', array('tag' => 'table')),
            'Form'
        ));
    }

}

?>
