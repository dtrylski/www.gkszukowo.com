CREATE DATABASE umocolor_gkszuk DEFAULT CHARACTER SET utf8 COLLATE utf8_polish_ci;
grant all on umocolor_gkszuk.* to 'umocolor_gkszuk'@'localhost' identified by 'handball';

DROP TABLE IF EXISTS article;
CREATE TABLE article (
article_id int(11) NOT NULL auto_increment,
dtime datetime default NULL,
article_lead varchar(350) NOT NULL,
title varchar(255) NOT NULL,
text text NOT NULL,
PRIMARY KEY (article_id)
);

CREATE TABLE foto (
foto_id int(11) NOT NULL auto_increment,
name varchar(255) NOT NULL,
article_id int NOT NULL,
PRIMARY KEY(foto_id),
FOREIGN KEY (article_id) 
REFERENCES article (article_id) 
ON UPDATE CASCADE 
ON DELETE CASCADE);

DROP TABLE IF EXISTS user;
CREATE TABLE user (
user_id INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
fname VARCHAR(100) NOT NULL,
sname VARCHAR(100) NOT NULL,
email VARCHAR(100) NOT NULL,
pass VARCHAR(40) NOT NULL,
salt VARCHAR(128) NOT NULL,
dtime DATETIME NOT NULL,
username VARCHAR(128) NOT NULL,
PRIMARY KEY (user_id)
);

CREATE TABLE player (
player_id INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
fname VARCHAR(100) NOT NULL,
sname VARCHAR(100) NOT NULL,
position VARCHAR(100) NOT NULL,
email VARCHAR(100),
height SMALLINT(3) NOT NULL,
games SMALLINT(3) NOT NULL,
number SMALLINT(3) NOT NULL,
status TINYINT(3) NULL,
PRIMARY KEY (player_id)
);

INSERT INTO player (fname,sname,position,height,number,status) VALUES
('Krzysztof','Cirocki','1','178','6','1'),
('Robert','Dymek','5','184','21','1'),
('Maciej','Hara','2','176','22','1'),
('Błażej','Jankowski','1','185','10','1'),
('Rafał','Jereczek','2','183','7','1'),
('Artur','Kitowski','4','182','11','1'),
('Mateusz','Kochański','5','180','12','1'),
('Arkadiusz','Kosznik','4','184','9','1'),
('Sławomir','Lica','1','195','13','1'),
('Łukasz','Plichta','2','180','','1'),
('Szymon','Marchewicz','2','184','14','1'),
('Filip','Ratkowski','3','180','4','1'),
('Marcin','Seń','4','178','8','1'),
('Adrian','Suchenek','1','190','15','1'),
('Paweł','Szlas','4','178','3','1'),
('Dawid','Szmidka','4','176','5','1'),
('Maciej','Szynszecki','5','183','1','1'),
('Mateusz','Wenzel','1','186','17','1'),
('Paweł','Wikarjusz','3','188','8','1'),
('Patryk','Załuski','1','187','','1'),
('Maciej','Zieliński','3','175','','1'); 


CREATE TABLE position (
position_id INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
position VARCHAR(100) NOT NULL,
PRIMARY KEY (position_id)
);

INSERT INTO position (position) VALUES ('rozgrywający'),('środkowy'),('obrotowy'),('skrzydłowy'),('bramkarz');

CREATE TABLE season (
season_id INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
season VARCHAR(100) NOT NULL,
PRIMARY KEY (season_id)
);

INSERT INTO season (season) VALUES ('2012/2013'),('2011/2012');

CREATE TABLE player_to_season (
player_id INT NOT NULL,
season_id SMALLINT(3) NOT NULL,
PRIMARY KEY (player_id)
);

INSERT INTO player_to_season VALUES 
('1','1'),
('2','1'),
('3','1'),
('4','1'),
('5','1'),
('6','1'),
('7','1'),
('8','1'),
('9','1'),
('10','1'),
('11','1'),
('12','1'),
('13','1'),
('14','1'),
('15','1'),
('16','1'),
('17','1'),
('18','1'),
('19','1'),
('20','1'),
('21','1');


CREATE TABLE player_to_goals (
game_id INT NOT NULL,
player_id INT(3),
season_id INT NOT NULL,
goals TINYINT(1),
PRIMARY KEY (game_id)
);

CREATE TABLE game (
game_id INT NOT NULL,
host_id INT(3),
visitor_id INT(3),
PRIMARY KEY (game_id)
);

CREATE TABLE game_to_round (
round_id INT NOT NULL,
game_id INT NOT NULL,
PRIMARY KEY (round_id)
);

DROP TABLE IF EXISTS league_table;
CREATE TABLE league_table(
position_id INTEGER UNSIGNED NOT NULL,
team VARCHAR(100) NOT NULL,
points INT(2) NOT NULL,
wins INT (2) NOT NULL,
losses INT (2) NOT NULL,
ties INT (2) NOT NULL,
goals_scored INT (3) NOT NULL,
goals_allowed INT (3) NOT NULL,
thumbnail VARCHAR(100) NOT NULL,
PRIMARY KEY (position_id)
);

DROP TABLE IF EXISTS schedule;
CREATE TABLE schedule(
game_id INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
round_id INTEGER UNSIGNED NOT NULL,
host_id INT(2) NOT NULL,
host_goals INT(2) NOT NULL,
visitor_id INT(2) NOT NULL,
visitor_goals INT(2) NOT NULL,
season_id INT (2) NOT NULL,
date  VARCHAR (100) NOT NULL,
PRIMARY KEY (game_id),
KEY fk_season_id (season_id)
);

DROP TABLE IF EXISTS menu;
CREATE TABLE menu (
menu_id INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
title VARCHAR(128) NOT NULL,
numer INTEGER UNSIGNED NOT NULL,
PRIMARY KEY (menu_id)
);

CREATE TABLE action_has_user (
action_id INTEGER,
user_id INTEGER,
);

DROP TABLE IF EXISTS action;
CREATE TABLE IF NOT EXISTS action (
  action_id int(11) NOT NULL AUTO_INCREMENT,
  module varchar(45) COLLATE utf8_polish_ci DEFAULT NULL,
  controller varchar(45) COLLATE utf8_polish_ci DEFAULT NULL,
  action varchar(45) COLLATE utf8_polish_ci DEFAULT NULL,
  is_secure tinyint(1) DEFAULT NULL,
  PRIMARY KEY (action_id)
);

DROP TABLE IF EXISTS action_has_user;
CREATE TABLE IF NOT EXISTS action_has_user (
  action_id int(11) NOT NULL,
  user_id int(11) NOT NULL,
  PRIMARY KEY (action_id,user_id),
  KEY fk_action_has_user_action1 (action_id),
  KEY fk_action_has_user_user1 (user_id)
);

INSERT INTO schedule(round_id,host_id,visitor_id,season_id,date) VALUES
('3','2','14','1','2012-09-29'),	
('3','3','1','1','2012-09-29'),	
('3','4','13','1','2012-09-29'),
('3','5','12','1','2012-09-29'),
('3','6','11','1','2012-09-29'),
('3','7','10','1','2012-09-29'),
('3','8','9','1','2012-09-29'),
('4','14','9','1','2012-10-06'),
('4','10','8','1','2012-10-06'),
('4','11','7','1','2012-10-06'),
('4','12','6','1','2012-10-06'),
('4','13','5','1','2012-10-06'),
('4','1','4','1','2012-10-06'),
('4','2','3','1','2012-10-06');

INSERT INTO schedule(round_id,host_id,visitor_id,season_id,date) VALUES
('5','3','14','1','2012-10-13'),
('5','4','2','1','2012-10-13'),
('5','5','1','1','2012-10-13'),
('5','6','13','1','2012-10-13'),
('5','7','12','1','2012-10-13'),
('5','8','11','1','2012-10-13'),
('5','9','10','1','2012-10-13');


INSERT INTO schedule(round_id,host_id,visitor_id,season_id,date) VALUES
('6','14','10','1','2012-10-20'),
('6','11','9','1','2012-10-20'),
('6','12','8','1','2012-10-20'),
('6','13','7','1','2012-10-20'),
('6','1','6','1','2012-10-20'),
('6','2','5','1','2012-10-20'),
('6','3','4','1','2012-10-20');

INSERT INTO schedule(round_id,host_id,visitor_id,season_id,date) VALUES
('7','4','14','1','2012-10-27'),
('7','5','3','1','2012-10-27'),
('7','6','2','1','2012-10-27'),
('7','7','1','1','2012-10-27'),
('7','8','13','1','2012-10-27'),
('7','9','12','1','2012-10-27'),
('7','10','11','1','2012-10-27');

INSERT INTO schedule(round_id,host_id,visitor_id,season_id,date) VALUES
('8','14','11','1','2012-11-10'),
('8','12','10','1','2012-11-10'),
('8','13','9','1','2012-11-10'),
('8','1','8','1','2012-11-10'),
('8','2','7','1','2012-11-10'),
('8','3','6','1','2012-11-10'),
('8','4','5','1','2012-11-10');

INSERT INTO schedule(round_id,host_id,visitor_id,season_id,date) VALUES
('9','5','14','1','2012-11-17'),
('9','6','4','1','2012-11-17'),
('9','7','3','1','2012-11-17'),
('9','8','2','1','2012-11-17'),
('9','9','1','1','2012-11-17'),
('9','10','13','1','2012-11-17'),
('9','11','12','1','2012-11-17');

INSERT INTO schedule(round_id,host_id,visitor_id,season_id,date) VALUES
('10','14','12','1','2012-11-24'),
('10','13','11','1','2012-11-24'),
('10','1','10','1','2012-11-24'),
('10','2','9','1','2012-11-24'),
('10','3','8','1','2012-11-24'),
('10','4','7','1','2012-11-24'),
('10','5','6','1','2012-11-24');

INSERT INTO schedule(round_id,host_id,visitor_id,season_id,date) VALUES
('11','6','14','1','2012-12-01'),
('11','7','5','1','2012-12-01'),
('11','8','4','1','2012-12-01'),
('11','9','3','1','2012-12-01'),
('11','10','2','1','2012-12-01'),
('11','11','1','1','2012-12-01'),
('11','12','13','1','2012-12-01');

INSERT INTO schedule(round_id,host_id,visitor_id,season_id,date) VALUES
('12','14','13','1','2012-12-08'),
('12','1','12','1','2012-12-08'),   
('12','2','11','1','2012-12-08'),
('12','3','10','1','2012-12-08'),
('12','4','9','1','2012-12-08'),
('12','5','8','1','2012-12-08'),
('12','6','7','1','2012-12-08'),
('13','7','14','1','2012-12-15'),
('13','8','6','1','2012-12-15'),
('13','9','5','1','2012-12-15'),
('13','10','4','1','2012-12-15'),
('13','11','3','1','2012-12-15'),
('13','12','2','1','2012-12-15'),
('13','13','1','1','2012-12-15');

INSERT INTO schedule(round_id,host_id,host_goals,visitor_id,visitor_goals,season_id,date) VALUES
('1', '1','32','14','18','1','2012-09-15'),
('1', '2','26','13','29','1','2012-09-15'),
('1', '3','25','12','19','1','2012-09-15'),
('1', '4','33','11','23','1','2012-09-15'),
('1', '5','35','10','27','1','2012-09-15'),
('1', '6','32','9','21','1','2012-09-15'),
('1', '7','30','8','30','1','2012-09-15'),
('2','14','22','8','24','1','2012-09-22'),
('2', '9','22','7','23','1','2012-09-22'),
('2','10','24','6','34','1','2012-09-22'),
('2','11','22','5','20','1','2012-09-22'),
('2','12','21','4','28','1','2012-09-22'),
('2','3','29','13','26','1','2012-09-22'),
('2','1','37','2','25','1','2012-09-22');

INSERT INTO menu(title,numer) VALUES
('Aktualności', 1),
('Seniorzy', 2),
('Juniorzy', 2),
('Młodzicy', 2),
('Klub', 3);
('Kontakt', 4);

INSERT INTO league_table(position_id,team,thumbnail) VALUES
('6','Energetyk Gryfino',''),
('5','AZS UKW Bydgoszcz',),
('8','Sambor Tczew',),
('4','Sokół Gdańsk',),
('3','Gwardia Koszalin',),
('1','MKS Grudziądz',),
('7','GKS Żukowo',),
('11','MKS Brodnica',),
('13','Szczypiorniak Olsztyn',),
('10','BS Korona Koronowo',),
('2','Alfa 99 Strzelno',),
('14','AZS UWM Olsztyn',),
('9','LKS Kęsowo',),
('12','Tytani Wejherowo',);

INSERT INTO league_table14(position_id,team,thumbnail) VALUES
('9','Energetyk Gryfino','sambortczew.png'),
('6','Sambor Tczew','sambortczew.png'),
('2','Sokół Gdańsk','sokol.png'),
('10','Gwardia Koszalin','alfa.png'),
('4','MKS Grudziądz','mksgrudziadz.png'),
('14','GKS Żukowo','gks_zukowo.png'),
('12','MKS Brodnica','mksbrodnica.png'),
('11','Szczypiorniak Olsztyn','szczypiorniak_olsztyn.png'),
('1','BS Korona Koronowo','korona_koronowo.png'),
('5','Alfa 99 Strzelno','alfa.png'),
('13','SMS II Gdańsk',''),
('8','AZS Toruń','azs_torun'),
('3','pauza',''),
('7','Tytani Wejherowo','tytani.png');


INSERT INTO action_has_user (action_id, user_id) VALUES
(1,1),
(2,1),
(3,1),
(4,1),
(5,1),
(6,1),
(7,1),
(8,1),
(9,1),
(10,1),
(11,1),
(12,1),
(13,1),
(14,1);

INSERT INTO action (action_id, module, controller, action, is_secure) VALUES
(1, 'default', 'index', 'index', 0),
(2, 'default', 'seniorzy', 'index', 0),
(3, 'default', 'juniorzy', 'index', 0),
(4, 'default', 'mlodzicy', 'index', 0),
(5, 'default', 'klub', 'index', 0),
(6, 'default', 'kontakt', 'index', 0),
(7, 'default', 'admin', 'index', 1),
(8, 'default', 'admin', 'add', 1),
(9, 'default', 'admin', 'edit', 1),
(10, 'default', 'admin', 'update', 1),
(11, 'default', 'admin', 'delete', 1),
(12, 'default', 'admin', 'formfoto', 1),
(13, 'default', 'admin', 'savefoto', 1),
(14, 'default', 'admin', 'deletefoto', 1),
(15, 'default', 'index', 'show', 0),
(16, 'default', 'admin', 'schedule', 1),
(17, 'default', 'admin', 'editgame', 1);


INSERT INTO user (fname,sname,email,pass,salt,dtime,username)
VALUES
('Daniel','Trylski','daniel.trylski@gmail.com','86e3e4c0c9b89edc2386b8c69749a3177feda344','5bb99f6c8dd023df5ce82daeb0bf1fc11f186995',NOW(),'admin');

INSERT INTO news (dtime,title,text)
VALUES
(NOW(),'Wygrana na inaugurację','Zawodnicy GKS Żukowo wygrali mecz inaugurujący rozgrywki 2012/13'),
(NOW(),'Wygrana na druga kolejka', 'Zawodnicy GKS Żukowo wygrali drugi mecz');

INSERT INTO news (dtime,title,text)
VALUES
(NOW(),'Wygrana na inaugurację','Zawodnicy GKS Żukowo wygrali mecz inaugurujący rozgrywki 2012/13'),
(NOW(),'Wygrana na druga kolejka', 'Zawodnicy GKS Żukowo wygrali drugi mecz');

