-- MySQL dump 10.13  Distrib 5.5.24, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: gkszukowo
-- ------------------------------------------------------
-- Server version	5.5.24-0ubuntu0.12.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `action`
--

DROP TABLE IF EXISTS `action`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `action` (
  `action_id` int(11) NOT NULL AUTO_INCREMENT,
  `module` varchar(45) COLLATE utf8_polish_ci DEFAULT NULL,
  `controller` varchar(45) COLLATE utf8_polish_ci DEFAULT NULL,
  `action` varchar(45) COLLATE utf8_polish_ci DEFAULT NULL,
  `is_secure` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`action_id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `action`
--

LOCK TABLES `action` WRITE;
/*!40000 ALTER TABLE `action` DISABLE KEYS */;
INSERT INTO `action` VALUES (1,'default','index','index',0),(2,'default','seniorzy','index',0),(3,'default','juniorzy','index',0),(4,'default','mlodzicy','index',0),(5,'default','klub','index',0),(6,'default','kontakt','index',0),(7,'default','admin','index',1),(8,'default','admin','add',1),(9,'default','admin','edit',1),(10,'default','admin','update',1),(11,'default','admin','delete',1),(12,'default','admin','formfoto',1),(13,'default','admin','savefoto',1),(14,'default','admin','deletefoto',1),(15,'default','index','show',0);
/*!40000 ALTER TABLE `action` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `action_has_user`
--

DROP TABLE IF EXISTS `action_has_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `action_has_user` (
  `action_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`action_id`,`user_id`),
  KEY `fk_action_has_user_action1` (`action_id`),
  KEY `fk_action_has_user_user1` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `action_has_user`
--

LOCK TABLES `action_has_user` WRITE;
/*!40000 ALTER TABLE `action_has_user` DISABLE KEYS */;
INSERT INTO `action_has_user` VALUES (1,1),(2,1),(3,1),(4,1),(5,1),(6,1),(7,1),(8,1),(9,1),(10,1),(11,1),(12,1),(13,1),(14,1),(15,1);
/*!40000 ALTER TABLE `action_has_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `article`
--

DROP TABLE IF EXISTS `article`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `article` (
  `article_id` int(11) NOT NULL AUTO_INCREMENT,
  `dtime` datetime DEFAULT NULL,
  `article_lead` varchar(350) COLLATE utf8_polish_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_polish_ci NOT NULL,
  `text` text COLLATE utf8_polish_ci NOT NULL,
  PRIMARY KEY (`article_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `article`
--

LOCK TABLES `article` WRITE;
/*!40000 ALTER TABLE `article` DISABLE KEYS */;
INSERT INTO `article` VALUES (2,'2012-11-02 15:19:32','<p>Alfa 99 Strzelno - GKS &bdquo;Żukowo&rdquo; 32 : 25 ( 17 : 16 )<br><br>GKS Żukowo: Szynszecki, Kochański, Dymek &ndash; Jankowski 3, Suchenek 4, Ratkowski 3, Hara 4, Wenzel 1, Wikarjusz, Lica 4, Kosznik, Plichta 6, Kitowski 1. Trener: Daniel Trylski. Najwięcej bramek dla Strzelna &ndash; Szych,','Przegrana w Strzelnie','<p>Alfa 99 Strzelno - GKS &bdquo;Żukowo&rdquo; 32 : 25 ( 17 : 16 )<br><br>GKS Żukowo: Szynszecki, Kochański, Dymek &ndash; Jankowski 3, Suchenek 4, Ratkowski 3, Hara 4, Wenzel 1, Wikarjusz, Lica 4, Kosznik, Plichta 6, Kitowski 1. Trener: Daniel Trylski. Najwięcej bramek dla Strzelna &ndash; Szych, Fajok, Dobrzański po 6.</p>\r\n<p>Kary: Strzelno &ndash; 8&rsquo;, GKS - 6&rsquo;</p>\r\n<p>Przebieg meczu: 5&rsquo; 2:1, 12&rsquo; 8:2, 17&rsquo; 10:4, 23&rsquo; 15:8, 27&rsquo; 17:11, 30&rsquo; 17:16, 35&rsquo; 17:19, 40&rsquo; 19:21, 45&rsquo; 24:21, 50&rsquo; 27:23, 55 29:23, 60&rsquo; 32:25.</p>\r\n<p>Zapraszamy na mecz GKS Żukowo &ndash; Gwardia Koszalin 18.11.2012r. niedziela godz. 17:00 Hala w Żukowie</p>');
/*!40000 ALTER TABLE `article` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `foto`
--

DROP TABLE IF EXISTS `foto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foto` (
  `foto_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_polish_ci NOT NULL,
  `article_id` int(11) NOT NULL,
  PRIMARY KEY (`foto_id`),
  KEY `article_id` (`article_id`),
  CONSTRAINT `foto_ibfk_1` FOREIGN KEY (`article_id`) REFERENCES `article` (`article_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `foto`
--

LOCK TABLES `foto` WRITE;
/*!40000 ALTER TABLE `foto` DISABLE KEYS */;
/*!40000 ALTER TABLE `foto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `league_table`
--

DROP TABLE IF EXISTS `league_table`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `league_table` (
  `position_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `club_name` varchar(100) COLLATE utf8_polish_ci NOT NULL,
  `points` int(2) NOT NULL,
  PRIMARY KEY (`position_id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `league_table`
--

LOCK TABLES `league_table` WRITE;
/*!40000 ALTER TABLE `league_table` DISABLE KEYS */;
INSERT INTO `league_table` VALUES (1,'Energetyk Gryfino',14),(2,'AZS UKW Bydgoszcz',12),(3,'Sambor Tczew',11),(4,'Sokół Gdańsk',10),(5,'Gwardia Koszalin',8),(6,'MKS Grudziądz',8),(7,'GKS Żukowo',6),(8,'MKS Brodnica',5),(9,'Szczypiorniak Olsztyn',5),(10,'BS Korona Koronowo',4),(11,'Alfa 99 Strzelno',4),(12,'AZS UWM Olsztyn',4),(13,'LKS Kęsowo',3),(14,'Tytani Wejherowo',2);
/*!40000 ALTER TABLE `league_table` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `menu`
--

DROP TABLE IF EXISTS `menu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `menu` (
  `menu_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(128) COLLATE utf8_polish_ci NOT NULL,
  `numer` int(10) unsigned NOT NULL,
  PRIMARY KEY (`menu_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `menu`
--

LOCK TABLES `menu` WRITE;
/*!40000 ALTER TABLE `menu` DISABLE KEYS */;
INSERT INTO `menu` VALUES (1,'Aktualności',1),(2,'Seniorzy',2),(3,'Juniorzy',2),(4,'Młodzicy',2),(5,'Klub',3),(6,'Kontakt',4);
/*!40000 ALTER TABLE `menu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `user_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fname` varchar(100) COLLATE utf8_polish_ci NOT NULL,
  `sname` varchar(100) COLLATE utf8_polish_ci NOT NULL,
  `email` varchar(100) COLLATE utf8_polish_ci NOT NULL,
  `pass` varchar(40) COLLATE utf8_polish_ci NOT NULL,
  `salt` varchar(128) COLLATE utf8_polish_ci NOT NULL,
  `dtime` datetime NOT NULL,
  `username` varchar(128) COLLATE utf8_polish_ci NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'Daniel','Trylski','daniel.trylski@gmail.com','86e3e4c0c9b89edc2386b8c69749a3177feda344','5bb99f6c8dd023df5ce82daeb0bf1fc11f186995','2012-10-29 21:33:53','admin');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2012-11-13 21:54:37
