-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Sep 21, 2013 at 04:32 PM
-- Server version: 5.1.71
-- PHP Version: 5.2.17

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `umocolor_gkszuk`
--

-- --------------------------------------------------------

--
-- Table structure for table `player`
--

CREATE TABLE IF NOT EXISTS `player` (
  `player_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fname` varchar(100) COLLATE utf8_polish_ci NOT NULL,
  `sname` varchar(100) COLLATE utf8_polish_ci NOT NULL,
  `position` varchar(100) COLLATE utf8_polish_ci NOT NULL,
  `email` varchar(100) COLLATE utf8_polish_ci DEFAULT NULL,
  `height` smallint(3) NOT NULL,
  `games` smallint(3) NOT NULL,
  `number` smallint(3) NOT NULL,
  `status` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`player_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci AUTO_INCREMENT=23 ;

--
-- Dumping data for table `player`
--

INSERT INTO `player` (`player_id`, `fname`, `sname`, `position`, `email`, `height`, `games`, `number`, `status`) VALUES
(1, 'Krzysztof', 'Cirocki', '1', NULL, 178, 0, 6, 1),
(2, 'Artur', 'Kaczmarek', '5', NULL, 184, 0, 21, 1),
(3, 'Maciej', 'Hara', '2', NULL, 176, 0, 22, 1),
(4, 'Błażej', 'Jankowski', '1', NULL, 185, 0, 10, 1),
(5, 'Rafał', 'Jereczek', '2', NULL, 183, 0, 7, 1),
(6, 'Robert', 'Węsiora', '4', NULL, 182, 0, 11, 1),
(8, 'Arkadiusz', 'Kosznik', '4', NULL, 184, 0, 9, 1),
(9, 'Filip', 'Wiczkowski', '1', NULL, 195, 0, 13, 1),
(10, 'Łukasz', 'Plichta', '4', NULL, 180, 0, 2, 1),
(11, 'Szymon', 'Marchewicz', '2', NULL, 184, 0, 14, 1),
(12, 'Filip', 'Ratkowski', '3', NULL, 180, 0, 4, 1),
(13, 'Marcin', 'Seń', '4', NULL, 178, 0, 8, 1),
(14, 'Adrian', 'Suchenek', '1', NULL, 190, 0, 15, 1),
(15, 'Damian', 'Rajkowski', '4', NULL, 178, 0, 3, 1),
(16, 'Dawid', 'Szmidka', '4', NULL, 176, 0, 5, 1),
(17, 'Maciej', 'Szynszecki', '5', NULL, 183, 0, 1, 1),
(18, 'Mateusz', 'Wenzel', '1', NULL, 186, 0, 17, 0),
(19, 'Dominik', 'Bonisławski', '3', NULL, 188, 0, 8, 1),
(20, 'Patryk', 'Załuski', '1', NULL, 187, 0, 0, NULL),
(21, 'Maciej', 'Zieliński', '3', NULL, 175, 0, 0, NULL),
(22, 'Dawid', 'Goszka', '4', NULL, 178, 0, 17, 1);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
