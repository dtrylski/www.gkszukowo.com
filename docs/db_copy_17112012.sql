-- MySQL dump 10.13  Distrib 5.5.24, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: gkszukowo
-- ------------------------------------------------------
-- Server version	5.5.24-0ubuntu0.12.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `action`
--

DROP TABLE IF EXISTS `action`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `action` (
  `action_id` int(11) NOT NULL AUTO_INCREMENT,
  `module` varchar(45) COLLATE utf8_polish_ci DEFAULT NULL,
  `controller` varchar(45) COLLATE utf8_polish_ci DEFAULT NULL,
  `action` varchar(45) COLLATE utf8_polish_ci DEFAULT NULL,
  `is_secure` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`action_id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `action`
--

LOCK TABLES `action` WRITE;
/*!40000 ALTER TABLE `action` DISABLE KEYS */;
INSERT INTO `action` VALUES (1,'default','index','index',0),(2,'default','seniorzy','index',0),(3,'default','juniorzy','index',0),(4,'default','mlodzicy','index',0),(5,'default','klub','index',0),(6,'default','kontakt','index',0),(7,'default','admin','index',1),(8,'default','admin','add',1),(9,'default','admin','edit',1),(10,'default','admin','update',1),(11,'default','admin','delete',1),(12,'default','admin','formfoto',1),(13,'default','admin','savefoto',1),(14,'default','admin','deletefoto',1),(15,'default','index','show',0);
/*!40000 ALTER TABLE `action` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `action_has_user`
--

DROP TABLE IF EXISTS `action_has_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `action_has_user` (
  `action_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`action_id`,`user_id`),
  KEY `fk_action_has_user_action1` (`action_id`),
  KEY `fk_action_has_user_user1` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `action_has_user`
--

LOCK TABLES `action_has_user` WRITE;
/*!40000 ALTER TABLE `action_has_user` DISABLE KEYS */;
INSERT INTO `action_has_user` VALUES (1,1),(2,1),(3,1),(4,1),(5,1),(6,1),(7,1),(8,1),(9,1),(10,1),(11,1),(12,1),(13,1),(14,1),(15,1);
/*!40000 ALTER TABLE `action_has_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `article`
--

DROP TABLE IF EXISTS `article`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `article` (
  `article_id` int(11) NOT NULL AUTO_INCREMENT,
  `dtime` datetime DEFAULT NULL,
  `article_lead` varchar(350) COLLATE utf8_polish_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_polish_ci NOT NULL,
  `text` text COLLATE utf8_polish_ci NOT NULL,
  PRIMARY KEY (`article_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `article`
--

LOCK TABLES `article` WRITE;
/*!40000 ALTER TABLE `article` DISABLE KEYS */;
INSERT INTO `article` VALUES (3,'2012-11-15 19:13:58','GKS Żukowo &nbsp;&nbsp; &nbsp;- &nbsp;&nbsp; &nbsp;Sambor Tczew &nbsp;&nbsp; &nbsp;30:30&nbsp;&nbsp; &nbsp;/ 13 - 13 /<br><br>','Remis na inauguracje','GKS Żukowo &nbsp;&nbsp; &nbsp;- &nbsp;&nbsp; &nbsp;Sambor Tczew &nbsp;&nbsp; &nbsp;30:30&nbsp;&nbsp; &nbsp;/ 13 - 13 /<br><br>'),(4,'2012-11-15 19:16:29','<strong>LKS Kęsowo - GKS Żukowo 22 : 23 ( 10 : 9 )</strong><br>GKS Żukowo: Kochański, Dymek &ndash; Kitowski 1, Lica 2, Szmidka, Jereczek 1, Szlas 2, Suchenek 6,Jankowski 4, Wikarjusz 2, Hara 5, Kosznik, Zieliński. Trener: Daniel Trylski.<br><br>Najwięcej bramek dla Kęsowa &ndash; Ludian i Kensik','Wygrana w Kęsowie','<strong>LKS Kęsowo - GKS Żukowo 22 : 23 ( 10 : 9 )</strong><br>GKS Żukowo: Kochański, Dymek &ndash; Kitowski 1, Lica 2, Szmidka, Jereczek 1, Szlas 2, Suchenek 6,Jankowski 4, Wikarjusz 2, Hara 5, Kosznik, Zieliński. Trener: Daniel Trylski.<br><br>Najwięcej bramek dla Kęsowa &ndash; Ludian i Kensik po 5.<br><br>Kary: Kęsowo &ndash; 12&rsquo;, GKS - 8&rsquo;<br>Przebieg meczu: 6&rsquo; 4:1, 15&rsquo; 6:4, 22&rsquo; 9:4, 25&rsquo; 9:7, 30&rsquo; 10:9,<br>36&rsquo; 15:11, 46&rsquo; 17:15, 52&rsquo; 19:17, 57&rsquo; 22:19 , 60&rsquo; 22:23.'),(5,'2012-11-15 19:17:37','<strong>GKS Żukowo &ndash; LKS Korona Koronowo 27 : 31 ( 12 : 18 )</strong><br>GKS Żukowo: Kochański, Dymek &ndash; Jankowski 8, Suchenek 6, Cirocki 5, Marchewicz 3, Plichta 3, Lica 1, Hara 1, Szmidka, Szlas, Ratkowski. Trener: Daniel Trylski.<br>Najwięcej bramek dla Koronowa &ndash; M. Kowalik 13.<br>Kary:','Przespana pierwsza połowa','<strong>GKS Żukowo &ndash; LKS Korona Koronowo 27 : 31 ( 12 : 18 )</strong><br>GKS Żukowo: Kochański, Dymek &ndash; Jankowski 8, Suchenek 6, Cirocki 5, Marchewicz 3, Plichta 3, Lica 1, Hara 1, Szmidka, Szlas, Ratkowski. Trener: Daniel Trylski.<br>Najwięcej bramek dla Koronowa &ndash; M. Kowalik 13.<br>Kary: GKS - 6&rsquo;, Koronowo - 18 &rsquo;.<br>Przebieg meczu: 4&rsquo; 1:4, 12&rsquo; 6:7, 17&rsquo; 6:11, 22&rsquo; 9:13, 27&rsquo; 12:14, 30&rsquo; 12:18,<br>35&rsquo; 13:21, 43&rsquo; 17:23, 47&rsquo; 18:26, 56&rsquo; 25:28, 60&rsquo; 27:31.'),(6,'2012-11-15 19:22:15','<strong>GKS Żukowo &ndash; MKS Brodnica 27 : 29 ( 13 : 13 )</strong><br>GKS Żukowo: Szynszecki, Kochański &ndash; Jankowski 7, Suchenek 4, Ratkowski 4, Cirocki 2,Marchewicz 3, Plichta 3, Lica 1, Kitowski 1, Jereczek 1, Wikarjusz 1, Hara, Szmidka. Trener:Daniel Trylski.<br>Najwięcej bramek dla Brodnicy','Proste błedy w końcowych minutach','<strong>GKS Żukowo &ndash; MKS Brodnica 27 : 29 ( 13 : 13 )</strong><br>GKS Żukowo: Szynszecki, Kochański &ndash; Jankowski 7, Suchenek 4, Ratkowski 4, Cirocki 2,Marchewicz 3, Plichta 3, Lica 1, Kitowski 1, Jereczek 1, Wikarjusz 1, Hara, Szmidka. Trener:Daniel Trylski.<br>Najwięcej bramek dla Brodnicy &ndash; Wajc 9.<br>Kary: GKS - 8&rsquo;, Brodnica - 6 &rsquo;.<br><br>Przebieg meczu: 4&rsquo; 2:1, 7&rsquo; 2:4, 11&rsquo; 5:5, 17&rsquo; 8:8, 23&rsquo; 10:8, 26&rsquo; 12:9, 30&rsquo; 13:13,<br>35&rsquo; 16:14, 37&rsquo; 16:16, 44&rsquo; 22:19, 47&rsquo; 23:23, 51&rsquo; 25:25, 54&rsquo; 26:28, 60&rsquo; 27:29.'),(7,'2012-11-15 19:24:29','GKS Żukowo &nbsp;&nbsp; &nbsp;- &nbsp;&nbsp; &nbsp;Tytani Wejherowo &nbsp;&nbsp; &nbsp;26:22&nbsp;&nbsp; &nbsp;/ 14 - 6 /','Konsekwentna obrona','GKS Żukowo &nbsp;&nbsp; &nbsp;- &nbsp;&nbsp; &nbsp;Tytani Wejherowo &nbsp;&nbsp; &nbsp;26:22&nbsp;&nbsp; &nbsp;/ 14 - 6 /'),(8,'2012-11-15 19:25:24','<strong>Szczypiorniak BS Olsztyn - GKS Żukowo 25 : 25 ( 13 : 10 )</strong><br><br>GKS Żukowo: Szynszecki, Kochański, Dymek &ndash; Jankowski 8, Suchenek 5, Ratkowski 3, Hara 3, Szlas 2,<br>Wenzel 2, Wikarjusz 1, Lica 1, Szmidka, Jereczek. Trener: Daniel Trylski.<br><br>Najwięcej bramek dla &bdquo;Szczypiorniaka&rdquo;','Remis w Olsztynie','<strong>Szczypiorniak BS Olsztyn - GKS Żukowo 25 : 25 ( 13 : 10 )</strong><br><br>GKS Żukowo: Szynszecki, Kochański, Dymek &ndash; Jankowski 8, Suchenek 5, Ratkowski 3, Hara 3, Szlas 2,<br>Wenzel 2, Wikarjusz 1, Lica 1, Szmidka, Jereczek. Trener: Daniel Trylski.<br><br>Najwięcej bramek dla &bdquo;Szczypiorniaka&rdquo; &ndash; Koledziński i Dzido po 7.<br>Kary: Szczypiorniak &ndash; 12&rsquo;, GKS - 4&rsquo;<br><br>Przebieg meczu: 7&rsquo; 4:3, 13&rsquo; 6:6, 20&rsquo; 8:9, 27&rsquo; 12:9, 30&rsquo; 13:10,<br>33&rsquo; 13:12, 41&rsquo; 18:16, 49&rsquo; 23:19, 56&rsquo; 24:23 , 60&rsquo; 25:25.'),(9,'2012-11-15 19:27:15','<strong>GKS Żukowo &ndash; MKS Grudziądz 28 : 32 ( 10 : 16 )</strong><br>GKS Żukowo: Szynszecki, Kochański, Dymek &ndash; Jankowski 2, Suchenek 10, Ratkowski 6, Cirocki 3, Plichta, Lica 2, Jereczek 1, Hara 1, Szmidka 1, Szlas 2, Wikarjusz, Wenzel, Kitowski. Trener: Daniel Trylski.<br>Najwięcej bramek','Porażka z Grudziądzem','<strong>GKS Żukowo &ndash; MKS Grudziądz 28 : 32 ( 10 : 16 )</strong><br>GKS Żukowo: Szynszecki, Kochański, Dymek &ndash; Jankowski 2, Suchenek 10, Ratkowski 6, Cirocki 3, Plichta, Lica 2, Jereczek 1, Hara 1, Szmidka 1, Szlas 2, Wikarjusz, Wenzel, Kitowski. Trener: Daniel Trylski.<br>Najwięcej bramek dla Grudziądza &ndash; Kruszewski 9.<br>Kary: GKS - 18&rsquo;, Grudziądz - 16 &rsquo;.<br>Przebieg meczu: 5&rsquo; 2:2, 15&rsquo; 4:4, 17&rsquo; 5:6, 21&rsquo; 6:10, 25&rsquo; 6: 14, 30&rsquo; 10:16,<br>35&rsquo; 13:19, 44&rsquo; 17:26, 48&rsquo; 21:29, 53&rsquo; 23:32, 60&rsquo; 28:32.'),(10,'2012-11-15 19:28:44','<strong>Alfa 99 Strzelno - GKS Żukowo 32 : 25 ( 17 : 16 )</strong> <br>GKS Żukowo: Szynszecki, Kochański, Dymek &ndash; Jankowski 3, Suchenek 4, Ratkowski 3, Hara 4, Wenzel 1, Wikarjusz, Lica 4, Kosznik, Plichta 6, Kitowski 1. Trener: Daniel Trylski. Najwięcej bramek dla Strzelna &ndash; Szych, Fajok,','Przegrana w Strzelnie','<strong>Alfa 99 Strzelno - GKS Żukowo 32 : 25 ( 17 : 16 )</strong> <br>GKS Żukowo: Szynszecki, Kochański, Dymek &ndash; Jankowski 3, Suchenek 4, Ratkowski 3, Hara 4, Wenzel 1, Wikarjusz, Lica 4, Kosznik, Plichta 6, Kitowski 1. Trener: Daniel Trylski. Najwięcej bramek dla Strzelna &ndash; Szych, Fajok, Dobrzański po 6.<br>Kary: Strzelno &ndash; 8&rsquo;, GKS - 6&rsquo;<br>Przebieg meczu: 5&rsquo; 2:1, 12&rsquo; 8:2, 17&rsquo; 10:4, 23&rsquo; 15:8, 27&rsquo; 17:11, 30&rsquo; 17:16, 35&rsquo; 17:19, 40&rsquo; 19:21, 45&rsquo; 24:21, 50&rsquo; 27:23, 55 29:23, 60&rsquo; 32:25.<br>Zapraszamy na mecz GKS Żukowo &ndash; Gwardia Koszalin 18.11.2012r. niedziela godz. 17:00 Hala w Żukowie');
/*!40000 ALTER TABLE `article` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `foto`
--

DROP TABLE IF EXISTS `foto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foto` (
  `foto_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_polish_ci NOT NULL,
  `article_id` int(11) NOT NULL,
  PRIMARY KEY (`foto_id`),
  KEY `article_id` (`article_id`),
  CONSTRAINT `foto_ibfk_1` FOREIGN KEY (`article_id`) REFERENCES `article` (`article_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `foto`
--

LOCK TABLES `foto` WRITE;
/*!40000 ALTER TABLE `foto` DISABLE KEYS */;
INSERT INTO `foto` VALUES (12,'89419_1235068264_600f_p.jpg',10);
/*!40000 ALTER TABLE `foto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `league_table`
--

DROP TABLE IF EXISTS `league_table`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `league_table` (
  `position_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `club_name` varchar(100) COLLATE utf8_polish_ci NOT NULL,
  `points` int(2) NOT NULL,
  PRIMARY KEY (`position_id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `league_table`
--

LOCK TABLES `league_table` WRITE;
/*!40000 ALTER TABLE `league_table` DISABLE KEYS */;
INSERT INTO `league_table` VALUES (1,'Energetyk Gryfino',14),(2,'AZS UKW Bydgoszcz',12),(3,'Sambor Tczew',11),(4,'Sokół Gdańsk',10),(5,'Gwardia Koszalin',8),(6,'MKS Grudziądz',8),(7,'GKS Żukowo',6),(8,'MKS Brodnica',5),(9,'Szczypiorniak Olsztyn',5),(10,'BS Korona Koronowo',4),(11,'Alfa 99 Strzelno',4),(12,'AZS UWM Olsztyn',4),(13,'LKS Kęsowo',3),(14,'Tytani Wejherowo',2);
/*!40000 ALTER TABLE `league_table` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `menu`
--

DROP TABLE IF EXISTS `menu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `menu` (
  `menu_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(128) COLLATE utf8_polish_ci NOT NULL,
  `numer` int(10) unsigned NOT NULL,
  PRIMARY KEY (`menu_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `menu`
--

LOCK TABLES `menu` WRITE;
/*!40000 ALTER TABLE `menu` DISABLE KEYS */;
INSERT INTO `menu` VALUES (1,'Aktualności',1),(2,'Seniorzy',2),(3,'Juniorzy',2),(4,'Młodzicy',2),(5,'Klub',3),(6,'Kontakt',4);
/*!40000 ALTER TABLE `menu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `user_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fname` varchar(100) COLLATE utf8_polish_ci NOT NULL,
  `sname` varchar(100) COLLATE utf8_polish_ci NOT NULL,
  `email` varchar(100) COLLATE utf8_polish_ci NOT NULL,
  `pass` varchar(40) COLLATE utf8_polish_ci NOT NULL,
  `salt` varchar(128) COLLATE utf8_polish_ci NOT NULL,
  `dtime` datetime NOT NULL,
  `username` varchar(128) COLLATE utf8_polish_ci NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'Daniel','Trylski','daniel.trylski@gmail.com','86e3e4c0c9b89edc2386b8c69749a3177feda344','5bb99f6c8dd023df5ce82daeb0bf1fc11f186995','2012-10-29 21:33:53','admin');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2012-11-17 17:02:30
