-- MySQL dump 10.13  Distrib 5.5.28, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: umocolor_gkszuk
-- ------------------------------------------------------
-- Server version	5.5.28-0ubuntu0.12.04.3

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `action`
--

DROP TABLE IF EXISTS `action`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `action` (
  `action_id` int(11) NOT NULL AUTO_INCREMENT,
  `module` varchar(45) COLLATE utf8_polish_ci DEFAULT NULL,
  `controller` varchar(45) COLLATE utf8_polish_ci DEFAULT NULL,
  `action` varchar(45) COLLATE utf8_polish_ci DEFAULT NULL,
  `is_secure` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`action_id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `action`
--

LOCK TABLES `action` WRITE;
/*!40000 ALTER TABLE `action` DISABLE KEYS */;
INSERT INTO `action` VALUES (1,'default','index','index',0),(2,'default','seniorzy','index',0),(3,'default','juniorzy','index',0),(4,'default','mlodzicy','index',0),(5,'default','admin','index',1),(6,'default','klub','index',0),(7,'default','kontakt','index',1),(8,'default','admin','add',1),(9,'default','admin','edit',1),(10,'default','admin','update',1),(11,'default','admin','delete',1),(12,'default','admin','formfoto',1),(13,'default','admin','savefoto',1),(14,'default','admin','deletefoto',1),(15,'default','admin','show',0),(16,'default','admin','schedule',1),(17,'default','admin','editgame',1),(18,'default','admin','updategame',1);
/*!40000 ALTER TABLE `action` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `action_has_user`
--

DROP TABLE IF EXISTS `action_has_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `action_has_user` (
  `action_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`action_id`,`user_id`),
  KEY `fk_action_has_user_action1` (`action_id`),
  KEY `fk_action_has_user_user1` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `action_has_user`
--

LOCK TABLES `action_has_user` WRITE;
/*!40000 ALTER TABLE `action_has_user` DISABLE KEYS */;
INSERT INTO `action_has_user` VALUES (1,1),(2,1),(3,1),(4,1),(5,1),(6,1),(7,1),(8,1),(9,1),(10,1),(11,1),(12,1),(13,1),(14,1),(15,1),(16,1),(17,1),(18,1);
/*!40000 ALTER TABLE `action_has_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `article`
--

DROP TABLE IF EXISTS `article`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `article` (
  `article_id` int(11) NOT NULL AUTO_INCREMENT,
  `dtime` datetime DEFAULT NULL,
  `article_lead` varchar(350) COLLATE utf8_polish_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_polish_ci NOT NULL,
  `text` text COLLATE utf8_polish_ci NOT NULL,
  PRIMARY KEY (`article_id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `article`
--

LOCK TABLES `article` WRITE;
/*!40000 ALTER TABLE `article` DISABLE KEYS */;
INSERT INTO `article` VALUES (3,'2012-11-15 19:13:58','<strong>GKS Żukowo - Sambor Tczew 30:30 (13 - 13)</strong><br>Po bardzo nerwowym początku z obu stron żadna z drużyn nie mogła zdobyć wiekszej niz 3 bramkowej przewagi. Ostatecznie w końcowych sekundach gościom udało się uratować jeden punkt.','Remis na inauguracje','<strong>GKS Żukowo - Sambor Tczew 30:30 (13 - 13)</strong><br>Po bardzo nerwowym początku z obu stron żadna z drużyn nie mogła zdobyć wiekszej niz 3 bramkowej przewagi. Ostatecznie w końcowych sekundach gościom udało się uratować jeden punkt.'),(4,'2012-11-15 19:16:29','<strong>LKS Kęsowo - GKS Żukowo 22 : 23 ( 10 : 9 )</strong><br>GKS Żukowo: Kochański, Dymek &ndash; Kitowski 1, Lica 2, Szmidka, Jereczek 1, Szlas 2, Suchenek 6,Jankowski 4, Wikarjusz 2, Hara 5, Kosznik, Zieliński. Trener: Daniel Trylski.<br><br>Najwięcej bramek dla Kęsowa &ndash; Ludian i Kensik','Wygrana w Kęsowie','<strong>LKS Kęsowo - GKS Żukowo 22 : 23 ( 10 : 9 )</strong><br>GKS Żukowo: Kochański, Dymek &ndash; Kitowski 1, Lica 2, Szmidka, Jereczek 1, Szlas 2, Suchenek 6,Jankowski 4, Wikarjusz 2, Hara 5, Kosznik, Zieliński. Trener: Daniel Trylski.<br><br>Najwięcej bramek dla Kęsowa &ndash; Ludian i Kensik po 5.<br><br>Kary: Kęsowo &ndash; 12&rsquo;, GKS - 8&rsquo;<br>Przebieg meczu: 6&rsquo; 4:1, 15&rsquo; 6:4, 22&rsquo; 9:4, 25&rsquo; 9:7, 30&rsquo; 10:9,<br>36&rsquo; 15:11, 46&rsquo; 17:15, 52&rsquo; 19:17, 57&rsquo; 22:19 , 60&rsquo; 22:23.'),(5,'2012-11-15 19:17:37','<strong>GKS Żukowo &ndash; LKS Korona Koronowo 27 : 31 ( 12 : 18 )</strong><br>GKS Żukowo: Kochański, Dymek &ndash; Jankowski 8, Suchenek 6, Cirocki 5, Marchewicz 3, Plichta 3, Lica 1, Hara 1, Szmidka, Szlas, Ratkowski. Trener: Daniel Trylski.<br>Najwięcej bramek dla Koronowa &ndash; M. Kowalik 13.<br>Kary:','Przespana pierwsza połowa','<strong>GKS Żukowo &ndash; LKS Korona Koronowo 27 : 31 ( 12 : 18 )</strong><br>GKS Żukowo: Kochański, Dymek &ndash; Jankowski 8, Suchenek 6, Cirocki 5, Marchewicz 3, Plichta 3, Lica 1, Hara 1, Szmidka, Szlas, Ratkowski. Trener: Daniel Trylski.<br>Najwięcej bramek dla Koronowa &ndash; M. Kowalik 13.<br>Kary: GKS - 6&rsquo;, Koronowo - 18 &rsquo;.<br>Przebieg meczu: 4&rsquo; 1:4, 12&rsquo; 6:7, 17&rsquo; 6:11, 22&rsquo; 9:13, 27&rsquo; 12:14, 30&rsquo; 12:18,<br>35&rsquo; 13:21, 43&rsquo; 17:23, 47&rsquo; 18:26, 56&rsquo; 25:28, 60&rsquo; 27:31.'),(6,'2012-11-15 19:22:15','<strong>GKS Żukowo &ndash; MKS Brodnica 27 : 29 ( 13 : 13 )</strong><br>GKS Żukowo: Szynszecki, Kochański &ndash; Jankowski 7, Suchenek 4, Ratkowski 4, Cirocki 2,Marchewicz 3, Plichta 3, Lica 1, Kitowski 1, Jereczek 1, Wikarjusz 1, Hara, Szmidka. Trener:Daniel Trylski.<br>Najwięcej bramek dla Brodnicy','Proste błedy w końcowych minutach','<strong>GKS Żukowo &ndash; MKS Brodnica 27 : 29 ( 13 : 13 )</strong><br>GKS Żukowo: Szynszecki, Kochański &ndash; Jankowski 7, Suchenek 4, Ratkowski 4, Cirocki 2,Marchewicz 3, Plichta 3, Lica 1, Kitowski 1, Jereczek 1, Wikarjusz 1, Hara, Szmidka. Trener:Daniel Trylski.<br>Najwięcej bramek dla Brodnicy &ndash; Wajc 9.<br>Kary: GKS - 8&rsquo;, Brodnica - 6 &rsquo;.<br><br>Przebieg meczu: 4&rsquo; 2:1, 7&rsquo; 2:4, 11&rsquo; 5:5, 17&rsquo; 8:8, 23&rsquo; 10:8, 26&rsquo; 12:9, 30&rsquo; 13:13,<br>35&rsquo; 16:14, 37&rsquo; 16:16, 44&rsquo; 22:19, 47&rsquo; 23:23, 51&rsquo; 25:25, 54&rsquo; 26:28, 60&rsquo; 27:29.'),(7,'2012-11-15 19:24:29','<strong>GKS Żukowo &nbsp;&nbsp; &nbsp;- &nbsp;&nbsp; &nbsp;Tytani Wejherowo &nbsp;&nbsp; &nbsp;26:22&nbsp;&nbsp; &nbsp;/ 14 - 6 /</strong><br><br>Bardzo dobra pierwsza połowa w wykonaniu naszych zawodnik&oacute;w. Twarda obrona i skuteczność w ataku przyczyniły się do wyniku 6:0 w 10 min meczu.','Konsekwentna obrona','<strong>GKS Żukowo &nbsp;&nbsp; &nbsp;- &nbsp;&nbsp; &nbsp;Tytani Wejherowo &nbsp;&nbsp; &nbsp;26:22&nbsp;&nbsp; &nbsp;/ 14 - 6 /</strong><br><br>Bardzo dobra pierwsza połowa w wykonaniu naszych zawodnik&oacute;w. Twarda obrona i skuteczność w ataku przyczyniły się do wyniku 6:0 w 10 min meczu. Przez pierwsze 30 min pozwoliliśmy gościom rzucić tylko 6 bramek zdobywając 14. W całkiem przyzwoitym stylu zdobywamy dwa punkty.'),(8,'2012-11-15 19:25:24','<strong>Szczypiorniak BS Olsztyn - GKS Żukowo 25 : 25 ( 13 : 10 )</strong><br><br>GKS Żukowo: Szynszecki, Kochański, Dymek &ndash; Jankowski 8, Suchenek 5, Ratkowski 3, Hara 3, Szlas 2,<br>Wenzel 2, Wikarjusz 1, Lica 1, Szmidka, Jereczek. Trener: Daniel Trylski.<br><br>Najwięcej bramek dla &bdquo;Szczypiorniaka&rdquo;','Remis w Olsztynie','<strong>Szczypiorniak BS Olsztyn - GKS Żukowo 25 : 25 ( 13 : 10 )</strong><br><br>GKS Żukowo: Szynszecki, Kochański, Dymek &ndash; Jankowski 8, Suchenek 5, Ratkowski 3, Hara 3, Szlas 2,<br>Wenzel 2, Wikarjusz 1, Lica 1, Szmidka, Jereczek. Trener: Daniel Trylski.<br><br>Najwięcej bramek dla &bdquo;Szczypiorniaka&rdquo; &ndash; Koledziński i Dzido po 7.<br>Kary: Szczypiorniak &ndash; 12&rsquo;, GKS - 4&rsquo;<br><br>Przebieg meczu: 7&rsquo; 4:3, 13&rsquo; 6:6, 20&rsquo; 8:9, 27&rsquo; 12:9, 30&rsquo; 13:10,<br>33&rsquo; 13:12, 41&rsquo; 18:16, 49&rsquo; 23:19, 56&rsquo; 24:23 , 60&rsquo; 25:25.'),(9,'2012-11-15 19:27:15','<strong>GKS Żukowo &ndash; MKS Grudziądz 28 : 32 ( 10 : 16 )</strong><br>GKS Żukowo: Szynszecki, Kochański, Dymek &ndash; Jankowski 2, Suchenek 10, Ratkowski 6, Cirocki 3, Plichta, Lica 2, Jereczek 1, Hara 1, Szmidka 1, Szlas 2, Wikarjusz, Wenzel, Kitowski. Trener: Daniel Trylski.<br>Najwięcej bramek','Porażka z Grudziądzem','<strong>GKS Żukowo &ndash; MKS Grudziądz 28 : 32 ( 10 : 16 )</strong><br>GKS Żukowo: Szynszecki, Kochański, Dymek &ndash; Jankowski 2, Suchenek 10, Ratkowski 6, Cirocki 3, Plichta, Lica 2, Jereczek 1, Hara 1, Szmidka 1, Szlas 2, Wikarjusz, Wenzel, Kitowski. Trener: Daniel Trylski.<br>Najwięcej bramek dla Grudziądza &ndash; Kruszewski 9.<br>Kary: GKS - 18&rsquo;, Grudziądz - 16 &rsquo;.<br>Przebieg meczu: 5&rsquo; 2:2, 15&rsquo; 4:4, 17&rsquo; 5:6, 21&rsquo; 6:10, 25&rsquo; 6: 14, 30&rsquo; 10:16,<br>35&rsquo; 13:19, 44&rsquo; 17:26, 48&rsquo; 21:29, 53&rsquo; 23:32, 60&rsquo; 28:32.'),(10,'2012-11-15 19:28:44','<strong>Alfa 99 Strzelno - GKS Żukowo 32 : 25 ( 17 : 16 )</strong> <br>GKS Żukowo: Szynszecki, Kochański, Dymek &ndash; Jankowski 3, Suchenek 4, Ratkowski 3, Hara 4, Wenzel 1, Wikarjusz, Lica 4, Kosznik, Plichta 6, Kitowski 1. Trener: Daniel Trylski. Najwięcej bramek dla Strzelna &ndash; Szych, Fajok,','Przegrana w Strzelnie','<strong>Alfa 99 Strzelno - GKS Żukowo 32 : 25 ( 17 : 16 )</strong> <br>GKS Żukowo: Szynszecki, Kochański, Dymek &ndash; Jankowski 3, Suchenek 4, Ratkowski 3, Hara 4, Wenzel 1, Wikarjusz, Lica 4, Kosznik, Plichta 6, Kitowski 1. Trener: Daniel Trylski. Najwięcej bramek dla Strzelna &ndash; Szych, Fajok, Dobrzański po 6.<br>Kary: Strzelno &ndash; 8&rsquo;, GKS - 6&rsquo;<br>Przebieg meczu: 5&rsquo; 2:1, 12&rsquo; 8:2, 17&rsquo; 10:4, 23&rsquo; 15:8, 27&rsquo; 17:11, 30&rsquo; 17:16, 35&rsquo; 17:19, 40&rsquo; 19:21, 45&rsquo; 24:21, 50&rsquo; 27:23, 55 29:23, 60&rsquo; 32:25.<br>Zapraszamy na mecz GKS Żukowo &ndash; Gwardia Koszalin 18.11.2012r. niedziela godz. 17:00 Hala w Żukowie'),(11,'2012-11-19 23:09:47','<strong>GKS Żukowo &ndash; Gwardia Koszalin 22 : 22 ( 10 : 7 )</strong><br><br>GKS Żukowo: Szynszecki, Kochański, Dymek &ndash; Suchenek 5, Ratkowski 5, Cirocki 5, Lica 4, Hara 2, Szmidka, Wenzel, Kitowski, Seń 1, Kosznik. Trener: Daniel Trylski. Najwięcej bramek dla Koszalina &ndash; Czerwiński','Remis z Koszalinem','<strong>GKS Żukowo &ndash; Gwardia Koszalin 22 : 22 ( 10 : 7 )</strong><br><br>GKS Żukowo: Szynszecki, Kochański, Dymek &ndash; Suchenek 5, Ratkowski 5, Cirocki 5, Lica 4, Hara 2, Szmidka, Wenzel, Kitowski, Seń 1, Kosznik. Trener: Daniel Trylski. Najwięcej bramek dla Koszalina &ndash; Czerwiński 9.<br>Kary: GKS - 8&rsquo;, Koszalin - 20 &rsquo;.<br><br>Przebieg meczu: 4&rsquo; 0:3, 9&rsquo; 2:4, 14&rsquo; 4:4, 19&rsquo; 6:6, 25&rsquo; 8: 6, 30&rsquo; 10:7,<br>36&rsquo; 12:8, 38&rsquo; 14:9, 42&rsquo; 15:11, 48&rsquo; 18:14, 50&rsquo; 20:16, 55&rsquo; 20:19, 60&rsquo; 22:22.'),(12,'2012-12-05 09:22:00','<strong>Sok&oacute;ł Gdańsk - GKS Żukowo 18 : 30 ( 9 : 12 )</strong><br><br>Wygraliśmy bardzo ważny mecz na wyjeżdzie. Dwa punkty ciężko wywalczone w Gdańsku. Sok&oacute;ł postawił trudne warunki ale ostatecznie po bardzo dobrej grze w&nbsp; wykonaniu naszych zawodnik&oacute;w zrealizowaliśmy','Wygrana w Gdańsku','<strong>Sok&oacute;ł Gdańsk - GKS Żukowo 18 : 30 ( 9 : 12 )</strong><br><br>Wygraliśmy bardzo ważny mecz na wyjeżdzie. Dwa punkty ciężko wywalczone w Gdańsku. Sok&oacute;ł postawił trudne warunki ale ostatecznie po bardzo dobrej grze w&nbsp; wykonaniu naszych zawodnik&oacute;w zrealizowaliśmy założenia i dwa punkty zasiliły nasze konto. Konsekwentna i twarda gra w obronie przyczyniła się do zwycięstwa. Skład GKS-u Żukowo: Szynszecki, Kochański, Plichta, Kitowski, Suchenek, Lica, Hara, Jankowski, Wenzel, Cirocki, Seń, Wikarjusz, Ratkowski.<br><br>Kolejny mecz GKS Żukowo rozegra 1 grudnia o godz.1800 na hali w Żukowie z<br>zajmującym drugie miejsce AZS UKW Bydgoszcz - serdecznie zapraszamy.'),(13,'2012-12-05 10:06:33','<strong>GKS Żukowo &ndash;&nbsp; AZS UKW Bydgoszcz&nbsp; 27 : 25&nbsp; ( 8 : 12 )</strong><br><br>GKS Żukowo: Szynszecki, Kochański &ndash;&nbsp; Suchenek 4, Ratkowski 6, Cirocki 5,&nbsp; Lica 2,&nbsp; Hara 2, Plichta 3, Marchewicz 2, Jereczek 2, Jankowski 1, Seń, Kitowski, Kosznik, Szlas, Wikariusz.<br>Najwięcej','Wygrana z AZS Bydgoszcz','<strong>GKS Żukowo &ndash;&nbsp; AZS UKW Bydgoszcz&nbsp; 27 : 25&nbsp; ( 8 : 12 )</strong><br><br>GKS Żukowo: Szynszecki, Kochański &ndash;&nbsp; Suchenek 4, Ratkowski 6, Cirocki 5,&nbsp; Lica 2,&nbsp; Hara 2, Plichta 3, Marchewicz 2, Jereczek 2, Jankowski 1, Seń, Kitowski, Kosznik, Szlas, Wikariusz.<br>Najwięcej bramek dla Bydgoszczy &ndash;&nbsp; Jaszyk 7.<br><br>Kary: GKS - 14&rsquo;, Bydgoszcz - 6 &rsquo;.<br><br>Przebieg meczu:&nbsp; 7&rsquo;&nbsp; 2:2, 9&rsquo;&nbsp; 2:4, 16&rsquo; 6:4,&nbsp; 19&rsquo;&nbsp; 6:6, 26&rsquo; 6: 10, 30&rsquo;&nbsp; 8:12,<br>33&rsquo;&nbsp; 10:13, 38&rsquo;&nbsp; 13:16,&nbsp; 41&rsquo; 13:19,&nbsp; 45&rsquo;&nbsp; 16:19, 48&rsquo;&nbsp; 20:21,&nbsp; 54&rsquo;&nbsp; 22:25,&nbsp; 60&rsquo;&nbsp; 27:25.<br><br><strong>Następny mecz GKS Żukowo rozegra 8 grudnia w Gryfinie z &bdquo;Energetykiem&rdquo;.</strong>');
/*!40000 ALTER TABLE `article` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `foto`
--

DROP TABLE IF EXISTS `foto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foto` (
  `foto_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_polish_ci NOT NULL,
  `article_id` int(11) NOT NULL,
  PRIMARY KEY (`foto_id`),
  KEY `article_id` (`article_id`),
  CONSTRAINT `foto_ibfk_1` FOREIGN KEY (`article_id`) REFERENCES `article` (`article_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=55 DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `foto`
--

LOCK TABLES `foto` WRITE;
/*!40000 ALTER TABLE `foto` DISABLE KEYS */;
INSERT INTO `foto` VALUES (15,'DSC_4368.jpg',9),(16,'DSC_4375.jpg',9),(17,'DSC_4376.jpg',9),(18,'DSC_4377.jpg',9),(19,'DSC_4378.jpg',9),(20,'DSC_4383.jpg',9),(21,'DSC_4386.jpg',9),(22,'DSC_8658.jpg',6),(23,'DSC_8665.jpg',6),(24,'DSC_8812.jpg',6),(25,'DSC_8822.jpg',6),(26,'DSC_8828.jpg',6),(27,'DSC_8839.jpg',6),(29,'DSC_8384_1.jpg',5),(30,'DSC_8391.jpg',5),(31,'DSC_8405.jpg',5),(32,'DSC_8415.jpg',5),(33,'DSC_8452.jpg',5),(34,'DSC_8457.jpg',5),(35,'DSC_9095.jpg',11),(36,'DSC_9112.jpg',11),(37,'DSC_9115.jpg',11),(38,'DSC_9118.jpg',11),(39,'DSC_9155.jpg',11),(40,'DSC_9166.jpg',11),(41,'DSC_9203.jpg',11),(42,'DSC_9229.jpg',11),(43,'DSC_9275.jpg',11),(44,'DSC_4529.jpg',13),(45,'DSC_4533.jpg',13),(46,'DSC_4538.jpg',13),(47,'DSC_4547.jpg',13),(48,'DSC_4548.jpg',13),(49,'DSC_4552.jpg',13),(50,'DSC_4587.jpg',13),(51,'DSC_4590.jpg',13),(52,'DSC_4599.jpg',13),(53,'DSC_4608.jpg',13),(54,'DSC_4617.jpg',13);
/*!40000 ALTER TABLE `foto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `league_table`
--

DROP TABLE IF EXISTS `league_table`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `league_table` (
  `position_id` int(10) unsigned NOT NULL,
  `team` varchar(100) COLLATE utf8_polish_ci NOT NULL,
  `points` int(2) NOT NULL,
  `wins` int(2) NOT NULL,
  `losses` int(2) NOT NULL,
  `ties` int(2) NOT NULL,
  `goals_scored` int(3) NOT NULL,
  `goals_allowed` int(3) NOT NULL,
  PRIMARY KEY (`position_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `league_table`
--

LOCK TABLES `league_table` WRITE;
/*!40000 ALTER TABLE `league_table` DISABLE KEYS */;
INSERT INTO `league_table` VALUES (1,'MKS Grudziądz',18,11,2,0,383,339),(2,'Alfa 99 Strzelno',14,9,4,0,377,386),(3,'Gwardia Koszalin',10,7,4,2,349,356),(4,'Sokół Gdańsk',18,11,2,0,408,344),(5,'AZS UKW Bydgoszcz',22,11,2,0,406,343),(6,'Energetyk Gryfino',22,12,1,0,432,343),(7,'GKS Żukowo',13,7,3,3,344,353),(8,'Sambor Tczew',12,8,3,2,344,371),(9,'LKS Kęsowo',5,6,6,1,325,392),(10,'BS Korona Koronowo',8,8,5,0,353,389),(11,'MKS Brodnica',7,6,6,1,343,381),(12,'Tytani Wejherowo',13,9,3,1,335,345),(13,'Szczypiorniak Olsztyn',13,6,4,3,314,326),(14,'AZS UWM Olsztyn',7,5,7,1,332,377);
/*!40000 ALTER TABLE `league_table` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `menu`
--

DROP TABLE IF EXISTS `menu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `menu` (
  `menu_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(128) COLLATE utf8_polish_ci NOT NULL,
  `numer` int(10) unsigned NOT NULL,
  PRIMARY KEY (`menu_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `menu`
--

LOCK TABLES `menu` WRITE;
/*!40000 ALTER TABLE `menu` DISABLE KEYS */;
INSERT INTO `menu` VALUES (1,'Aktualności',1),(2,'Seniorzy',2),(3,'Juniorzy',2),(4,'Młodzicy',2),(5,'Klub',3),(6,'Kontakt',4);
/*!40000 ALTER TABLE `menu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `player`
--

DROP TABLE IF EXISTS `player`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `player` (
  `player_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fname` varchar(100) COLLATE utf8_polish_ci NOT NULL,
  `sname` varchar(100) COLLATE utf8_polish_ci NOT NULL,
  `position` varchar(100) COLLATE utf8_polish_ci NOT NULL,
  `email` varchar(100) COLLATE utf8_polish_ci DEFAULT NULL,
  `height` smallint(3) NOT NULL,
  `games` smallint(3) NOT NULL,
  `number` smallint(3) NOT NULL,
  PRIMARY KEY (`player_id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `player`
--

LOCK TABLES `player` WRITE;
/*!40000 ALTER TABLE `player` DISABLE KEYS */;
INSERT INTO `player` VALUES (1,'Krzysztof','Cirocki','1',NULL,178,0,6),(2,'Robert','Dymek','5',NULL,184,0,21),(3,'Maciej','Hara','2',NULL,176,0,22),(4,'Błażej','Jankowski','1',NULL,185,0,10),(5,'Rafał','Jereczek','2',NULL,183,0,7),(6,'Artur','Kitowski','4',NULL,182,0,11),(7,'Mateusz','Kochański','5',NULL,180,0,12),(8,'Arkadiusz','Kosznik','4',NULL,184,0,9),(9,'Sławomir','Lica','1',NULL,195,0,13),(10,'Damian','Łoboda','2',NULL,180,0,0),(11,'Szymon','Marchewicz','2',NULL,184,0,14),(12,'Filip','Ratkowski','3',NULL,180,0,4),(13,'Marcin','Seń','4',NULL,178,0,8),(14,'Adrian','Suchenek','1',NULL,190,0,15),(15,'Paweł','Szlas','4',NULL,178,0,3),(16,'Dawid','Szmidka','4',NULL,176,0,5),(17,'Maciej','Szynszecki','5',NULL,183,0,1),(18,'Mateusz','Wenzel','1',NULL,186,0,17),(19,'Paweł','Wikarjusz','3',NULL,188,0,0),(20,'Patryk','Załuski','1',NULL,187,0,0),(21,'Maciej','Zieliński','3',NULL,175,0,0),(22,'Paweł','Wysokiński','4',NULL,178,0,17);
/*!40000 ALTER TABLE `player` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `player_to_season`
--

DROP TABLE IF EXISTS `player_to_season`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `player_to_season` (
  `player_id` int(11) NOT NULL,
  `season_id` smallint(3) NOT NULL,
  PRIMARY KEY (`player_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `player_to_season`
--

LOCK TABLES `player_to_season` WRITE;
/*!40000 ALTER TABLE `player_to_season` DISABLE KEYS */;
INSERT INTO `player_to_season` VALUES (1,1),(2,1),(3,1),(4,1),(5,1),(6,1),(7,1),(8,1),(9,1),(10,1),(11,1),(12,1),(13,1),(14,1),(15,1),(16,1),(17,1),(18,1),(19,1),(20,1),(21,1),(22,2);
/*!40000 ALTER TABLE `player_to_season` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `position`
--

DROP TABLE IF EXISTS `position`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `position` (
  `position_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `position` varchar(100) COLLATE utf8_polish_ci NOT NULL,
  PRIMARY KEY (`position_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `position`
--

LOCK TABLES `position` WRITE;
/*!40000 ALTER TABLE `position` DISABLE KEYS */;
INSERT INTO `position` VALUES (1,'rozgrywający'),(2,'środkowy'),(3,'obrotowy'),(4,'skrzydłowy'),(5,'bramkarz');
/*!40000 ALTER TABLE `position` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `schedule`
--

DROP TABLE IF EXISTS `schedule`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `schedule` (
  `game_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `round_id` int(10) unsigned NOT NULL,
  `host_id` int(2) NOT NULL,
  `host_goals` int(2) NOT NULL,
  `visitor_id` int(2) NOT NULL,
  `visitor_goals` int(2) NOT NULL,
  `season_id` int(2) NOT NULL,
  `date` datetime DEFAULT NULL,
  PRIMARY KEY (`game_id`),
  KEY `fk_season_id` (`season_id`)
) ENGINE=InnoDB AUTO_INCREMENT=183 DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `schedule`
--

LOCK TABLES `schedule` WRITE;
/*!40000 ALTER TABLE `schedule` DISABLE KEYS */;
INSERT INTO `schedule` VALUES (1,1,1,32,14,18,1,'2012-09-15 18:00:00'),(2,1,2,26,13,29,1,'2012-09-15 18:00:00'),(3,1,3,25,12,19,1,'2012-09-15 18:00:00'),(4,1,4,33,11,23,1,'2012-09-15 18:00:00'),(5,1,5,35,10,27,1,'2012-09-15 18:00:00'),(6,1,6,32,9,21,1,'2012-09-15 18:00:00'),(7,1,7,30,8,30,1,'2012-09-15 18:00:00'),(8,2,14,22,8,24,1,'2012-09-22 18:00:00'),(9,2,9,22,7,23,1,'2012-09-22 18:00:00'),(10,2,10,24,6,34,1,'2012-09-22 18:00:00'),(11,2,11,20,5,22,1,'2012-09-22 18:00:00'),(12,2,12,21,4,28,1,'2012-09-22 18:00:00'),(13,2,3,28,13,19,1,'2012-09-22 18:00:00'),(14,2,1,37,2,25,1,'2012-09-22 18:00:00'),(15,3,2,28,14,26,1,'2012-09-29 18:00:00'),(16,3,3,30,1,25,1,'2012-09-29 18:00:00'),(17,3,4,32,13,20,1,'2012-09-29 18:00:00'),(18,3,5,31,12,25,1,'2012-09-29 18:00:00'),(19,3,6,28,11,31,1,'2012-09-29 18:00:00'),(20,3,7,27,10,31,1,'2012-09-29 18:00:00'),(21,3,8,30,9,29,1,'2012-09-29 18:00:00'),(22,4,14,31,9,26,1,'2012-10-06 18:00:00'),(23,4,10,30,8,29,1,'2012-10-06 18:00:00'),(24,4,11,29,7,27,1,'2012-10-06 18:00:00'),(25,4,12,24,6,33,1,'2012-10-06 18:00:00'),(26,4,13,27,5,25,1,'2012-10-06 18:00:00'),(27,4,1,30,4,28,1,'2012-10-06 18:00:00'),(28,4,2,31,3,30,1,'2012-10-06 18:00:00'),(29,5,3,31,14,27,1,'2012-10-13 18:00:00'),(30,5,4,34,2,28,1,'2012-10-13 18:00:00'),(31,5,5,32,1,30,1,'2012-10-13 18:00:00'),(32,5,6,33,13,27,1,'2012-10-13 18:00:00'),(33,5,7,26,12,22,1,'2012-10-13 18:00:00'),(34,5,8,28,11,23,1,'2012-10-13 18:00:00'),(35,5,9,33,10,31,1,'2012-10-13 18:00:00'),(36,6,14,30,10,25,1,'2012-10-20 18:00:00'),(37,6,11,35,9,35,1,'2012-10-20 18:00:00'),(38,6,12,20,8,28,1,'2012-10-20 18:00:00'),(39,6,13,25,7,25,1,'2012-10-20 18:00:00'),(40,6,1,30,6,39,1,'2012-10-20 18:00:00'),(41,6,2,29,5,31,1,'2012-10-20 18:00:00'),(42,6,3,24,4,30,1,'2012-10-20 18:00:00'),(43,7,4,42,14,26,1,'2012-10-27 18:00:00'),(44,7,5,32,3,27,1,'2012-10-27 18:00:00'),(45,7,6,32,2,23,1,'2012-10-27 18:00:00'),(46,7,7,28,1,32,1,'2012-10-27 18:00:00'),(47,7,8,22,13,21,1,'2012-10-27 18:00:00'),(48,7,9,22,12,35,1,'2012-10-27 18:00:00'),(49,7,10,32,11,23,1,'2012-10-27 18:00:00'),(50,8,14,31,11,27,1,'2012-11-10 18:00:00'),(51,8,12,30,10,25,1,'2012-11-10 18:00:00'),(52,8,13,23,9,18,1,'2012-11-10 18:00:00'),(53,8,1,29,8,25,1,'2012-11-10 18:00:00'),(54,8,2,32,7,27,1,'2012-11-10 18:00:00'),(55,8,3,30,6,37,1,'2012-11-10 18:00:00'),(56,8,4,29,5,32,1,'2012-11-10 18:00:00'),(57,9,5,35,14,25,1,'2012-11-17 18:00:00'),(58,9,6,31,4,26,1,'2012-11-17 18:00:00'),(59,9,7,22,3,22,1,'2012-11-17 18:00:00'),(60,9,8,30,2,33,1,'2012-11-17 18:00:00'),(61,9,9,14,1,28,1,'2012-11-17 18:00:00'),(62,9,10,28,13,30,1,'2012-11-17 18:00:00'),(63,9,11,25,12,27,1,'2012-11-17 18:00:00'),(64,10,14,26,12,29,1,'2012-11-24 18:00:00'),(65,10,13,22,11,17,1,'2012-11-24 18:00:00'),(66,10,1,27,10,20,1,'2012-11-24 18:00:00'),(67,10,2,30,9,28,1,'2012-11-24 18:00:00'),(68,10,3,27,8,27,1,'2012-11-24 18:00:00'),(69,10,4,18,7,30,1,'2012-11-24 18:00:00'),(70,10,5,37,6,31,1,'2012-11-24 18:00:00'),(71,11,6,29,14,23,1,'2012-12-01 18:00:00'),(72,11,7,27,5,25,1,'2012-12-01 18:00:00'),(73,11,8,25,4,31,1,'2012-12-01 18:00:00'),(74,11,9,30,3,27,1,'2012-12-01 18:00:00'),(75,11,10,24,2,32,1,'2012-12-01 18:00:00'),(76,11,11,30,1,34,1,'2012-12-01 18:00:00'),(77,11,12,27,13,27,1,'2012-12-01 18:00:00'),(78,12,14,20,13,20,1,'2012-12-08 18:00:00'),(79,12,1,24,12,26,1,'2012-12-08 18:00:00'),(80,12,2,35,11,28,1,'2012-12-08 18:00:00'),(81,12,3,21,10,25,1,'2012-12-08 18:00:00'),(82,12,4,39,9,23,1,'2012-12-08 18:00:00'),(83,12,5,41,8,22,1,'2012-12-08 18:00:00'),(84,12,6,38,7,23,1,'2012-12-08 18:00:00'),(85,13,7,29,14,27,1,'2012-12-15 18:00:00'),(86,13,8,24,6,35,1,'2012-12-15 18:00:00'),(87,13,9,24,5,28,1,'2012-12-15 18:00:00'),(88,13,10,31,4,38,1,'2012-12-15 18:00:00'),(89,13,11,32,3,27,1,'2012-12-15 18:00:00'),(90,13,12,30,2,25,1,'2012-12-15 18:00:00'),(91,13,13,24,1,25,1,'2012-12-15 18:00:00'),(92,14,14,0,1,0,1,'2013-01-26 18:00:00'),(93,14,13,0,2,0,1,'2013-01-26 18:00:00'),(94,14,12,0,3,0,1,'2013-01-26 18:00:00'),(95,14,11,0,4,0,1,'2013-01-26 18:00:00'),(96,14,10,0,5,0,1,'2013-01-26 18:00:00'),(97,14,9,0,6,0,1,'2013-01-26 18:00:00'),(98,14,8,0,7,0,1,'2013-01-26 18:00:00'),(99,15,8,0,14,0,1,'2013-02-02 18:00:00'),(100,15,7,0,9,0,1,'2013-02-02 18:00:00'),(101,15,6,0,10,0,1,'2013-02-02 18:00:00'),(102,15,5,0,11,0,1,'2013-02-02 18:00:00'),(103,15,4,0,12,0,1,'2013-02-02 18:00:00'),(104,15,13,0,3,0,1,'2013-02-02 18:00:00'),(105,15,2,0,1,0,1,'2013-02-02 18:00:00'),(106,16,14,0,2,0,1,'2013-02-09 18:00:00'),(107,16,1,0,3,0,1,'2013-02-09 18:00:00'),(108,16,13,0,4,0,1,'2013-02-09 18:00:00'),(109,16,12,0,5,0,1,'2013-02-09 18:00:00'),(110,16,11,0,6,0,1,'2013-02-09 18:00:00'),(111,16,10,0,7,0,1,'2013-02-09 18:00:00'),(112,16,9,0,8,0,1,'2013-02-09 18:00:00'),(113,17,9,0,14,0,1,'2013-02-16 18:00:00'),(114,17,8,0,10,0,1,'2013-02-16 18:00:00'),(115,17,7,0,11,0,1,'2013-02-16 18:00:00'),(116,17,6,0,12,0,1,'2013-02-16 18:00:00'),(117,17,5,0,13,0,1,'2013-02-16 18:00:00'),(118,17,17,0,1,0,1,'2013-02-16 18:00:00'),(119,17,3,0,2,0,1,'2013-02-16 18:00:00'),(120,18,14,0,3,0,1,'2013-02-23 18:00:00'),(121,18,2,0,4,0,1,'2013-02-23 18:00:00'),(122,18,1,0,5,0,1,'2013-02-23 18:00:00'),(123,18,13,0,6,0,1,'2013-02-23 18:00:00'),(124,18,12,0,7,0,1,'2013-02-23 18:00:00'),(125,18,11,0,8,0,1,'2013-02-23 18:00:00'),(126,18,10,0,9,0,1,'2013-02-23 18:00:00'),(127,19,10,0,14,0,1,'2013-03-02 18:00:00'),(128,19,9,0,11,0,1,'2013-03-02 18:00:00'),(129,19,8,0,12,0,1,'2013-03-02 18:00:00'),(130,19,7,0,13,0,1,'2013-03-02 18:00:00'),(131,19,6,0,1,0,1,'2013-03-02 18:00:00'),(132,19,5,0,2,0,1,'2013-03-02 18:00:00'),(133,19,4,0,3,0,1,'2013-03-02 18:00:00'),(134,20,14,0,4,0,1,'2013-03-16 18:00:00'),(135,20,3,0,5,0,1,'2013-03-16 18:00:00'),(136,20,2,0,6,0,1,'2013-03-16 18:00:00'),(137,20,1,0,7,0,1,'2013-03-16 18:00:00'),(138,20,13,0,8,0,1,'2013-03-16 18:00:00'),(139,20,12,0,9,0,1,'2013-03-16 18:00:00'),(140,20,11,0,10,0,1,'2013-03-16 18:00:00'),(141,21,11,0,14,0,1,'2013-03-23 18:00:00'),(142,21,10,0,12,0,1,'2013-03-23 18:00:00'),(143,21,9,0,13,0,1,'2013-03-23 18:00:00'),(144,21,8,0,1,0,1,'2013-03-23 18:00:00'),(145,21,7,0,2,0,1,'2013-03-23 18:00:00'),(146,21,6,0,3,0,1,'2013-03-23 18:00:00'),(147,21,5,0,4,0,1,'2013-03-23 18:00:00'),(148,22,14,0,5,0,1,'2013-04-06 18:00:00'),(149,22,4,0,6,0,1,'2013-04-06 18:00:00'),(150,22,3,0,7,0,1,'2013-04-06 18:00:00'),(151,22,2,0,8,0,1,'2013-04-06 18:00:00'),(152,22,1,0,9,0,1,'2013-04-06 18:00:00'),(153,22,13,0,10,0,1,'2013-04-06 18:00:00'),(154,22,12,0,11,0,1,'2013-04-06 18:00:00'),(155,23,12,0,14,0,1,'2013-04-13 18:00:00'),(156,23,11,0,13,0,1,'2013-04-13 18:00:00'),(157,23,10,0,1,0,1,'2013-04-13 18:00:00'),(158,23,9,0,2,0,1,'2013-04-13 18:00:00'),(159,23,8,0,3,0,1,'2013-04-13 18:00:00'),(160,23,7,0,4,0,1,'2013-04-13 18:00:00'),(161,23,6,0,5,0,1,'2013-04-13 18:00:00'),(162,24,14,0,6,0,1,'2013-04-20 18:00:00'),(163,24,5,0,7,0,1,'2013-04-20 18:00:00'),(164,24,4,0,8,0,1,'2013-04-20 18:00:00'),(165,24,3,0,9,0,1,'2013-04-20 18:00:00'),(166,24,2,0,10,0,1,'2013-04-20 18:00:00'),(167,24,1,0,11,0,1,'2013-04-20 18:00:00'),(168,24,13,0,12,0,1,'2013-04-20 18:00:00'),(169,25,13,0,14,0,1,'2013-04-27 18:00:00'),(170,25,12,0,1,0,1,'2013-04-27 18:00:00'),(171,25,11,0,2,0,1,'2013-04-27 18:00:00'),(172,25,10,0,3,0,1,'2013-04-27 18:00:00'),(173,25,9,0,4,0,1,'2013-04-27 18:00:00'),(174,25,8,0,5,0,1,'2013-04-27 18:00:00'),(175,25,7,0,6,0,1,'2013-04-27 18:00:00'),(176,26,14,0,7,0,1,'2013-05-04 18:00:00'),(177,26,6,0,8,0,1,'2013-05-04 18:00:00'),(178,26,5,0,9,0,1,'2013-05-04 18:00:00'),(179,26,4,0,10,0,1,'2013-05-04 18:00:00'),(180,26,3,0,11,0,1,'2013-05-04 18:00:00'),(181,26,2,0,12,0,1,'2013-05-04 18:00:00'),(182,26,1,0,13,0,1,'2013-05-04 18:00:00');
/*!40000 ALTER TABLE `schedule` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `season`
--

DROP TABLE IF EXISTS `season`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `season` (
  `season_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `season` varchar(100) COLLATE utf8_polish_ci NOT NULL,
  PRIMARY KEY (`season_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `season`
--

LOCK TABLES `season` WRITE;
/*!40000 ALTER TABLE `season` DISABLE KEYS */;
INSERT INTO `season` VALUES (1,'2012/2013'),(2,'2011/2012');
/*!40000 ALTER TABLE `season` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `user_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fname` varchar(100) COLLATE utf8_polish_ci NOT NULL,
  `sname` varchar(100) COLLATE utf8_polish_ci NOT NULL,
  `email` varchar(100) COLLATE utf8_polish_ci NOT NULL,
  `pass` varchar(40) COLLATE utf8_polish_ci NOT NULL,
  `salt` varchar(128) COLLATE utf8_polish_ci NOT NULL,
  `dtime` datetime NOT NULL,
  `username` varchar(128) COLLATE utf8_polish_ci NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'Daniel','Trylski','daniel.trylski@gmail.com','86e3e4c0c9b89edc2386b8c69749a3177feda344','5bb99f6c8dd023df5ce82daeb0bf1fc11f186995','2012-10-29 21:33:53','admin');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2013-01-06 21:32:06
