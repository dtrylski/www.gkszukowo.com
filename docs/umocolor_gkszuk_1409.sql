-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Sep 14, 2013 at 10:05 PM
-- Server version: 5.1.71
-- PHP Version: 5.2.17

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `umocolor_gkszuk`
--

-- --------------------------------------------------------

--
-- Table structure for table `action`
--

CREATE TABLE IF NOT EXISTS `action` (
  `action_id` int(11) NOT NULL AUTO_INCREMENT,
  `module` varchar(45) COLLATE utf8_polish_ci DEFAULT NULL,
  `controller` varchar(45) COLLATE utf8_polish_ci DEFAULT NULL,
  `action` varchar(45) COLLATE utf8_polish_ci DEFAULT NULL,
  `is_secure` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`action_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci AUTO_INCREMENT=19 ;

--
-- Dumping data for table `action`
--

INSERT INTO `action` (`action_id`, `module`, `controller`, `action`, `is_secure`) VALUES
(1, 'default', 'index', 'index', 0),
(2, 'default', 'seniorzy', 'index', 0),
(3, 'default', 'juniorzy', 'index', 0),
(4, 'default', 'mlodzicy', 'index', 0),
(5, 'default', 'admin', 'index', 1),
(6, 'default', 'klub', 'index', 0),
(7, 'default', 'kontakt', 'index', 0),
(8, 'default', 'admin', 'add', 1),
(9, 'default', 'admin', 'edit', 1),
(10, 'default', 'admin', 'update', 1),
(11, 'default', 'admin', 'delete', 1),
(12, 'default', 'admin', 'formfoto', 1),
(13, 'default', 'admin', 'savefoto', 1),
(14, 'default', 'admin', 'deletefoto', 1),
(15, 'default', 'index', 'show', 0),
(16, 'default', 'admin', 'schedule', 1),
(17, 'default', 'admin', 'editgame', 1),
(18, 'default', 'admin', 'updategame', 1);

-- --------------------------------------------------------

--
-- Table structure for table `action_has_user`
--

CREATE TABLE IF NOT EXISTS `action_has_user` (
  `action_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`action_id`,`user_id`),
  KEY `fk_action_has_user_action1` (`action_id`),
  KEY `fk_action_has_user_user1` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Dumping data for table `action_has_user`
--

INSERT INTO `action_has_user` (`action_id`, `user_id`) VALUES
(1, 1),
(2, 1),
(3, 1),
(4, 1),
(5, 1),
(6, 1),
(7, 1),
(8, 1),
(9, 1),
(10, 1),
(11, 1),
(12, 1),
(13, 1),
(14, 1),
(15, 1),
(16, 1),
(17, 1),
(18, 1);

-- --------------------------------------------------------

--
-- Table structure for table `article`
--

CREATE TABLE IF NOT EXISTS `article` (
  `article_id` int(11) NOT NULL AUTO_INCREMENT,
  `dtime` datetime DEFAULT NULL,
  `article_lead` varchar(350) COLLATE utf8_polish_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_polish_ci NOT NULL,
  `text` text COLLATE utf8_polish_ci NOT NULL,
  PRIMARY KEY (`article_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci AUTO_INCREMENT=23 ;

--
-- Dumping data for table `article`
--

INSERT INTO `article` (`article_id`, `dtime`, `article_lead`, `title`, `text`) VALUES
(3, '2012-11-15 19:13:58', '<strong>GKS Żukowo - Sambor Tczew 30:30 (13 - 13)</strong><br>Po bardzo nerwowym początku z obu stron żadna z drużyn nie mogła zdobyć wiekszej niz 3 bramkowej przewagi. Ostatecznie w końcowych sekundach gościom udało się uratować jeden punkt.', 'Remis na inauguracje', '<strong>GKS Żukowo - Sambor Tczew 30:30 (13 - 13)</strong><br>Po bardzo nerwowym początku z obu stron żadna z drużyn nie mogła zdobyć wiekszej niz 3 bramkowej przewagi. Ostatecznie w końcowych sekundach gościom udało się uratować jeden punkt.'),
(4, '2012-11-15 19:16:29', '<strong>LKS Kęsowo - GKS Żukowo 22 : 23 ( 10 : 9 )</strong><br>GKS Żukowo: Kochański, Dymek &ndash; Kitowski 1, Lica 2, Szmidka, Jereczek 1, Szlas 2, Suchenek 6,Jankowski 4, Wikarjusz 2, Hara 5, Kosznik, Zieliński. Trener: Daniel Trylski.<br><br>Najwięcej bramek dla Kęsowa &ndash; Ludian i Kensik', 'Wygrana w Kęsowie', '<strong>LKS Kęsowo - GKS Żukowo 22 : 23 ( 10 : 9 )</strong><br>GKS Żukowo: Kochański, Dymek &ndash; Kitowski 1, Lica 2, Szmidka, Jereczek 1, Szlas 2, Suchenek 6,Jankowski 4, Wikarjusz 2, Hara 5, Kosznik, Zieliński. Trener: Daniel Trylski.<br><br>Najwięcej bramek dla Kęsowa &ndash; Ludian i Kensik po 5.<br><br>Kary: Kęsowo &ndash; 12&rsquo;, GKS - 8&rsquo;<br>Przebieg meczu: 6&rsquo; 4:1, 15&rsquo; 6:4, 22&rsquo; 9:4, 25&rsquo; 9:7, 30&rsquo; 10:9,<br>36&rsquo; 15:11, 46&rsquo; 17:15, 52&rsquo; 19:17, 57&rsquo; 22:19 , 60&rsquo; 22:23.'),
(5, '2012-11-15 19:17:37', '<strong>GKS Żukowo &ndash; LKS Korona Koronowo 27 : 31 ( 12 : 18 )</strong><br>GKS Żukowo: Kochański, Dymek &ndash; Jankowski 8, Suchenek 6, Cirocki 5, Marchewicz 3, Plichta 3, Lica 1, Hara 1, Szmidka, Szlas, Ratkowski. Trener: Daniel Trylski.<br>Najwięcej bramek dla Koronowa &ndash; M. Kowalik 13.<br>Kary:', 'Przespana pierwsza połowa', '<strong>GKS Żukowo &ndash; LKS Korona Koronowo 27 : 31 ( 12 : 18 )</strong><br>GKS Żukowo: Kochański, Dymek &ndash; Jankowski 8, Suchenek 6, Cirocki 5, Marchewicz 3, Plichta 3, Lica 1, Hara 1, Szmidka, Szlas, Ratkowski. Trener: Daniel Trylski.<br>Najwięcej bramek dla Koronowa &ndash; M. Kowalik 13.<br>Kary: GKS - 6&rsquo;, Koronowo - 18 &rsquo;.<br>Przebieg meczu: 4&rsquo; 1:4, 12&rsquo; 6:7, 17&rsquo; 6:11, 22&rsquo; 9:13, 27&rsquo; 12:14, 30&rsquo; 12:18,<br>35&rsquo; 13:21, 43&rsquo; 17:23, 47&rsquo; 18:26, 56&rsquo; 25:28, 60&rsquo; 27:31.'),
(6, '2012-11-15 19:22:15', '<strong>GKS Żukowo &ndash; MKS Brodnica 27 : 29 ( 13 : 13 )</strong><br>GKS Żukowo: Szynszecki, Kochański &ndash; Jankowski 7, Suchenek 4, Ratkowski 4, Cirocki 2,Marchewicz 3, Plichta 3, Lica 1, Kitowski 1, Jereczek 1, Wikarjusz 1, Hara, Szmidka. Trener:Daniel Trylski.<br>Najwięcej bramek dla Brodnicy', 'Proste błedy w końcowych minutach', '<strong>GKS Żukowo &ndash; MKS Brodnica 27 : 29 ( 13 : 13 )</strong><br>GKS Żukowo: Szynszecki, Kochański &ndash; Jankowski 7, Suchenek 4, Ratkowski 4, Cirocki 2,Marchewicz 3, Plichta 3, Lica 1, Kitowski 1, Jereczek 1, Wikarjusz 1, Hara, Szmidka. Trener:Daniel Trylski.<br>Najwięcej bramek dla Brodnicy &ndash; Wajc 9.<br>Kary: GKS - 8&rsquo;, Brodnica - 6 &rsquo;.<br><br>Przebieg meczu: 4&rsquo; 2:1, 7&rsquo; 2:4, 11&rsquo; 5:5, 17&rsquo; 8:8, 23&rsquo; 10:8, 26&rsquo; 12:9, 30&rsquo; 13:13,<br>35&rsquo; 16:14, 37&rsquo; 16:16, 44&rsquo; 22:19, 47&rsquo; 23:23, 51&rsquo; 25:25, 54&rsquo; 26:28, 60&rsquo; 27:29.'),
(7, '2012-11-15 19:24:29', '<strong>GKS Żukowo &nbsp;&nbsp; &nbsp;- &nbsp;&nbsp; &nbsp;Tytani Wejherowo &nbsp;&nbsp; &nbsp;26:22&nbsp;&nbsp; &nbsp;/ 14 - 6 /</strong><br><br>Bardzo dobra pierwsza połowa w wykonaniu naszych zawodnik&oacute;w. Twarda obrona i skuteczność w ataku przyczyniły się do wyniku 6:0 w 10 min meczu.', 'Konsekwentna obrona', '<strong>GKS Żukowo &nbsp;&nbsp; &nbsp;- &nbsp;&nbsp; &nbsp;Tytani Wejherowo &nbsp;&nbsp; &nbsp;26:22&nbsp;&nbsp; &nbsp;/ 14 - 6 /</strong><br><br>Bardzo dobra pierwsza połowa w wykonaniu naszych zawodnik&oacute;w. Twarda obrona i skuteczność w ataku przyczyniły się do wyniku 6:0 w 10 min meczu. Przez pierwsze 30 min pozwoliliśmy gościom rzucić tylko 6 bramek zdobywając 14. W całkiem przyzwoitym stylu zdobywamy dwa punkty.'),
(8, '2012-11-15 19:25:24', '<strong>Szczypiorniak BS Olsztyn - GKS Żukowo 25 : 25 ( 13 : 10 )</strong><br><br>GKS Żukowo: Szynszecki, Kochański, Dymek &ndash; Jankowski 8, Suchenek 5, Ratkowski 3, Hara 3, Szlas 2,<br>Wenzel 2, Wikarjusz 1, Lica 1, Szmidka, Jereczek. Trener: Daniel Trylski.<br><br>Najwięcej bramek dla &bdquo;Szczypiorniaka&rdquo;', 'Remis w Olsztynie', '<strong>Szczypiorniak BS Olsztyn - GKS Żukowo 25 : 25 ( 13 : 10 )</strong><br><br>GKS Żukowo: Szynszecki, Kochański, Dymek &ndash; Jankowski 8, Suchenek 5, Ratkowski 3, Hara 3, Szlas 2,<br>Wenzel 2, Wikarjusz 1, Lica 1, Szmidka, Jereczek. Trener: Daniel Trylski.<br><br>Najwięcej bramek dla &bdquo;Szczypiorniaka&rdquo; &ndash; Koledziński i Dzido po 7.<br>Kary: Szczypiorniak &ndash; 12&rsquo;, GKS - 4&rsquo;<br><br>Przebieg meczu: 7&rsquo; 4:3, 13&rsquo; 6:6, 20&rsquo; 8:9, 27&rsquo; 12:9, 30&rsquo; 13:10,<br>33&rsquo; 13:12, 41&rsquo; 18:16, 49&rsquo; 23:19, 56&rsquo; 24:23 , 60&rsquo; 25:25.'),
(9, '2012-11-15 19:27:15', '<strong>GKS Żukowo &ndash; MKS Grudziądz 28 : 32 ( 10 : 16 )</strong><br>GKS Żukowo: Szynszecki, Kochański, Dymek &ndash; Jankowski 2, Suchenek 10, Ratkowski 6, Cirocki 3, Plichta, Lica 2, Jereczek 1, Hara 1, Szmidka 1, Szlas 2, Wikarjusz, Wenzel, Kitowski. Trener: Daniel Trylski.<br>Najwięcej bramek', 'Porażka z Grudziądzem', '<strong>GKS Żukowo &ndash; MKS Grudziądz 28 : 32 ( 10 : 16 )</strong><br>GKS Żukowo: Szynszecki, Kochański, Dymek &ndash; Jankowski 2, Suchenek 10, Ratkowski 6, Cirocki 3, Plichta, Lica 2, Jereczek 1, Hara 1, Szmidka 1, Szlas 2, Wikarjusz, Wenzel, Kitowski. Trener: Daniel Trylski.<br>Najwięcej bramek dla Grudziądza &ndash; Kruszewski 9.<br>Kary: GKS - 18&rsquo;, Grudziądz - 16 &rsquo;.<br>Przebieg meczu: 5&rsquo; 2:2, 15&rsquo; 4:4, 17&rsquo; 5:6, 21&rsquo; 6:10, 25&rsquo; 6: 14, 30&rsquo; 10:16,<br>35&rsquo; 13:19, 44&rsquo; 17:26, 48&rsquo; 21:29, 53&rsquo; 23:32, 60&rsquo; 28:32.'),
(10, '2012-11-15 19:28:44', '<strong>Alfa 99 Strzelno - GKS Żukowo 32 : 25 ( 17 : 16 )</strong> <br>GKS Żukowo: Szynszecki, Kochański, Dymek &ndash; Jankowski 3, Suchenek 4, Ratkowski 3, Hara 4, Wenzel 1, Wikarjusz, Lica 4, Kosznik, Plichta 6, Kitowski 1. Trener: Daniel Trylski. Najwięcej bramek dla Strzelna &ndash; Szych, Fajok,', 'Przegrana w Strzelnie', '<strong>Alfa 99 Strzelno - GKS Żukowo 32 : 25 ( 17 : 16 )</strong> <br>GKS Żukowo: Szynszecki, Kochański, Dymek &ndash; Jankowski 3, Suchenek 4, Ratkowski 3, Hara 4, Wenzel 1, Wikarjusz, Lica 4, Kosznik, Plichta 6, Kitowski 1. Trener: Daniel Trylski. Najwięcej bramek dla Strzelna &ndash; Szych, Fajok, Dobrzański po 6.<br>Kary: Strzelno &ndash; 8&rsquo;, GKS - 6&rsquo;<br>Przebieg meczu: 5&rsquo; 2:1, 12&rsquo; 8:2, 17&rsquo; 10:4, 23&rsquo; 15:8, 27&rsquo; 17:11, 30&rsquo; 17:16, 35&rsquo; 17:19, 40&rsquo; 19:21, 45&rsquo; 24:21, 50&rsquo; 27:23, 55 29:23, 60&rsquo; 32:25.<br>Zapraszamy na mecz GKS Żukowo &ndash; Gwardia Koszalin 18.11.2012r. niedziela godz. 17:00 Hala w Żukowie'),
(11, '2012-11-19 23:09:47', '<strong>GKS Żukowo &ndash; Gwardia Koszalin 22 : 22 ( 10 : 7 )</strong><br><br>GKS Żukowo: Szynszecki, Kochański, Dymek &ndash; Suchenek 5, Ratkowski 5, Cirocki 5, Lica 4, Hara 2, Szmidka, Wenzel, Kitowski, Seń 1, Kosznik. Trener: Daniel Trylski. Najwięcej bramek dla Koszalina &ndash; Czerwiński', 'Remis z Koszalinem', '<strong>GKS Żukowo &ndash; Gwardia Koszalin 22 : 22 ( 10 : 7 )</strong><br><br>GKS Żukowo: Szynszecki, Kochański, Dymek &ndash; Suchenek 5, Ratkowski 5, Cirocki 5, Lica 4, Hara 2, Szmidka, Wenzel, Kitowski, Seń 1, Kosznik. Trener: Daniel Trylski. Najwięcej bramek dla Koszalina &ndash; Czerwiński 9.<br>Kary: GKS - 8&rsquo;, Koszalin - 20 &rsquo;.<br><br>Przebieg meczu: 4&rsquo; 0:3, 9&rsquo; 2:4, 14&rsquo; 4:4, 19&rsquo; 6:6, 25&rsquo; 8: 6, 30&rsquo; 10:7,<br>36&rsquo; 12:8, 38&rsquo; 14:9, 42&rsquo; 15:11, 48&rsquo; 18:14, 50&rsquo; 20:16, 55&rsquo; 20:19, 60&rsquo; 22:22.'),
(12, '2012-12-05 09:22:00', '<strong>Sok&oacute;ł Gdańsk - GKS Żukowo 18 : 30 ( 9 : 12 )</strong><br><br>Wygraliśmy bardzo ważny mecz na wyjeżdzie. Dwa punkty ciężko wywalczone w Gdańsku. Sok&oacute;ł postawił trudne warunki ale ostatecznie po bardzo dobrej grze w&nbsp; wykonaniu naszych zawodnik&oacute;w zrealizowaliśmy', 'Wygrana w Gdańsku', '<strong>Sok&oacute;ł Gdańsk - GKS Żukowo 18 : 30 ( 9 : 12 )</strong><br><br>Wygraliśmy bardzo ważny mecz na wyjeżdzie. Dwa punkty ciężko wywalczone w Gdańsku. Sok&oacute;ł postawił trudne warunki ale ostatecznie po bardzo dobrej grze w&nbsp; wykonaniu naszych zawodnik&oacute;w zrealizowaliśmy założenia i dwa punkty zasiliły nasze konto. Konsekwentna i twarda gra w obronie przyczyniła się do zwycięstwa. Skład GKS-u Żukowo: Szynszecki, Kochański, Plichta, Kitowski, Suchenek, Lica, Hara, Jankowski, Wenzel, Cirocki, Seń, Wikarjusz, Ratkowski.<br><br>Kolejny mecz GKS Żukowo rozegra 1 grudnia o godz.1800 na hali w Żukowie z<br>zajmującym drugie miejsce AZS UKW Bydgoszcz - serdecznie zapraszamy.'),
(13, '2012-12-05 10:06:33', '<strong>GKS Żukowo &ndash;&nbsp; AZS UKW Bydgoszcz&nbsp; 27 : 25&nbsp; ( 8 : 12 )</strong><br><br>GKS Żukowo: Szynszecki, Kochański &ndash;&nbsp; Suchenek 4, Ratkowski 6, Cirocki 5,&nbsp; Lica 2,&nbsp; Hara 2, Plichta 3, Marchewicz 2, Jereczek 2, Jankowski 1, Seń, Kitowski, Kosznik, Szlas, Wikariusz.<br>Najwięcej', 'Wygrana z AZS Bydgoszcz', '<strong>GKS Żukowo &ndash;&nbsp; AZS UKW Bydgoszcz&nbsp; 27 : 25&nbsp; ( 8 : 12 )</strong><br><br>GKS Żukowo: Szynszecki, Kochański &ndash;&nbsp; Suchenek 4, Ratkowski 6, Cirocki 5,&nbsp; Lica 2,&nbsp; Hara 2, Plichta 3, Marchewicz 2, Jereczek 2, Jankowski 1, Seń, Kitowski, Kosznik, Szlas, Wikariusz.<br>Najwięcej bramek dla Bydgoszczy &ndash;&nbsp; Jaszyk 7.<br><br>Kary: GKS - 14&rsquo;, Bydgoszcz - 6 &rsquo;.<br><br>Przebieg meczu:&nbsp; 7&rsquo;&nbsp; 2:2, 9&rsquo;&nbsp; 2:4, 16&rsquo; 6:4,&nbsp; 19&rsquo;&nbsp; 6:6, 26&rsquo; 6: 10, 30&rsquo;&nbsp; 8:12,<br>33&rsquo;&nbsp; 10:13, 38&rsquo;&nbsp; 13:16,&nbsp; 41&rsquo; 13:19,&nbsp; 45&rsquo;&nbsp; 16:19, 48&rsquo;&nbsp; 20:21,&nbsp; 54&rsquo;&nbsp; 22:25,&nbsp; 60&rsquo;&nbsp; 27:25.<br><br><strong>Następny mecz GKS Żukowo rozegra 8 grudnia w Gryfinie z &bdquo;Energetykiem&rdquo;.</strong>'),
(14, '2013-01-20 12:05:40', 'Energetyk Gryfino wykorzystał atut własnej hali i wygrał 38:23 (22:9).', 'Wyjazd do Gryfina', 'Energetyk Gryfino wykorzystał atut własnej hali i wygrał 38:23 (22:9).'),
(15, '2013-01-20 12:06:12', '<p><strong>GKS Żukowo &ndash; AZS UWM Olsztyn 29 : 27 ( 19 : 10 )</strong><br>GKS Żukowo: Szynszecki, Kochański, Dymek &ndash; Suchenek 7, Cirocki 1, Lica 2, Hara 2, Seń 4,<br>Plichta 4, Marchewicz 3, Jankowski 6, Wikarjusz, Jereczek, Szmidka, Szlas, Kosznik.<br><br>Najwięcej bramek dla Olsztyna', '2 punkty w ostatniej kolejce', '<p><strong>GKS Żukowo &ndash; AZS UWM Olsztyn 29 : 27 ( 19 : 10 )</strong><br>GKS Żukowo: Szynszecki, Kochański, Dymek &ndash; Suchenek 7, Cirocki 1, Lica 2, Hara 2, Seń 4,<br>Plichta 4, Marchewicz 3, Jankowski 6, Wikarjusz, Jereczek, Szmidka, Szlas, Kosznik.<br><br>Najwięcej bramek dla Olsztyna &ndash; Olszewski 7.<br><br>Kary: GKS - 6&rsquo;, Olsztyn - 0 &rsquo;.<br><br>Przebieg meczu: 6&rsquo; 1:3, 10&rsquo; 4:4, 15&rsquo; 7:4, 21&rsquo; 13:4, 25&rsquo; 15:7, 30&rsquo; 19:10,<br>33&rsquo; 21:11, 40&rsquo; 24:16, 49&rsquo; 26:22, 55&rsquo; 27:25, 60&rsquo; 29:27.<br><br>Po przerwie, kt&oacute;ra potrwa do 26 stycznia 2013r., GKS Żukowo rozegra mecz na wyjeździe<br>z &bdquo;Samborem&rdquo; Tczew.</p>'),
(16, '2013-01-23 15:21:41', '<h3>I Turniej P&oacute;łfinałowy Wojew&oacute;dzkiej Ligi Piłki Ręcznej Młodzik&oacute;w</h3>\r\n<p>W dniu 19.01.2013r. odbył sie I Turniej P&oacute;łfinałowy Wojew&oacute;dzkiej Ligi Piłki Ręcznej Młodzik&oacute;w. Organizatorem był MKS Sambor Tczew.Uczestnicy:GKS Żukowo,MTS Kwidzyn II, UKS', 'Liga Piłki Ręcznej Młodzików', '<h3>I Turniej P&oacute;łfinałowy Wojew&oacute;dzkiej Ligi Piłki Ręcznej Młodzik&oacute;w</h3>\r\n<p>W dniu 19.01.2013r. odbył sie I Turniej P&oacute;łfinałowy Wojew&oacute;dzkiej Ligi Piłki Ręcznej Młodzik&oacute;w. Organizatorem był MKS Sambor Tczew.Uczestnicy:GKS Żukowo,MTS Kwidzyn II, UKS Conrad II Wybrzeże Gdańsk, MKS Sambor I Tczew.</p>\r\n<h3>Wyniki:</h3>\r\n<table>\r\n<tbody>\r\n<tr>\r\n<td>GKS Żukowo</td>\r\n<td>Sambor I Tczew</td>\r\n<td>10:22</td>\r\n<td>(5:11)</td>\r\n</tr>\r\n<tr>\r\n<td>Conrad II Wybrzeże</td>\r\n<td>MTS Kwidzyn II</td>\r\n<td>13:9</td>\r\n<td>(7:4)</td>\r\n</tr>\r\n<tr>\r\n<td>GKS Żukowo</td>\r\n<td>Conrad II Wybrzeże</td>\r\n<td>17:19</td>\r\n<td>(7:8)</td>\r\n</tr>\r\n<tr>\r\n<td>Sambor I Tczew</td>\r\n<td>MTS Kwidzyn II</td>\r\n<td>17:13</td>\r\n<td>(6:5)</td>\r\n</tr>\r\n<tr>\r\n<td>Sambor I Tczew</td>\r\n<td>Conrad II Wybrzeże</td>\r\n<td>16:15</td>\r\n<td>(8:7)</td>\r\n</tr>\r\n<tr>\r\n<td>MTS Kwidzyn II</td>\r\n<td>GKS Żukowo</td>\r\n<td>13:23</td>\r\n<td>(8:12)</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<h3>Tabela I turnieju:</h3>\r\n<table>\r\n<tbody>\r\n<tr>\r\n<td>1.</td>\r\n<td>MKS Sambor I Tczew</td>\r\n<td>6 pkt</td>\r\n<td>55-38</td>\r\n</tr>\r\n<tr>\r\n<td>2.</td>\r\n<td>UKS Conrad II Wybrzeże Gdańsk</td>\r\n<td>4 pkt</td>\r\n<td>47-42</td>\r\n</tr>\r\n<tr>\r\n<td>3.</td>\r\n<td>GKS Żukowo</td>\r\n<td>2 pkt</td>\r\n<td>50-54</td>\r\n</tr>\r\n<tr>\r\n<td>4.</td>\r\n<td>MTS Kwidzyn II</td>\r\n<td>0 pkt</td>\r\n<td>35-53</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<h3>Tabela fair-play:</h3>\r\n<table>\r\n<tbody>\r\n<tr>\r\n<td>1.</td>\r\n<td>MKS Sambor Tczew</td>\r\n<td>6 min.</td>\r\n<td>7 up.</td>\r\n</tr>\r\n<tr>\r\n<td>2.</td>\r\n<td>UKS Conrad II Wybrzeże Gdańsk</td>\r\n<td>8 min.</td>\r\n<td>7 up.</td>\r\n</tr>\r\n<tr>\r\n<td>3.</td>\r\n<td>GKS Żukowo</td>\r\n<td>10 min.</td>\r\n<td>7 up.</td>\r\n</tr>\r\n<tr>\r\n<td>4.</td>\r\n<td>MTS Kwidzyn II</td>\r\n<td>12 min.</td>\r\n<td>8 up.</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<h3>Tabela najlepszych strzelc&oacute;w:</h3>\r\n<table>\r\n<tbody>\r\n<tr>\r\n<td>1.</td>\r\n<td>23 bramki</td>\r\n<td>Alan Guziewicz (Kwidzyn II);</td>\r\n</tr>\r\n<tr>\r\n<td>2.</td>\r\n<td>17 bramek</td>\r\n<td>Paweł Woźniak (Sambor I);</td>\r\n</tr>\r\n<tr>\r\n<td>3.</td>\r\n<td>16 bramek</td>\r\n<td>Konrad Pohnke (Żukowo), Paweł Kwiatkowski (Conrad II)</td>\r\n</tr>\r\n<tr>\r\n<td>4.</td>\r\n<td>12 bramek</td>\r\n<td>Rafał Waruszewski (Sambor I)</td>\r\n</tr>\r\n<tr>\r\n<td>5.</td>\r\n<td>11 bramek</td>\r\n<td>Kacper Krason (Żukowo)</td>\r\n</tr>\r\n<tr>\r\n<td>6.</td>\r\n<td>10 bramek</td>\r\n<td>Łukasz Gałka (Conrad II)</td>\r\n</tr>\r\n<tr>\r\n<td>7.</td>\r\n<td>9 bramek</td>\r\n<td>Kacper Sadowski (Sambor I), Aleks Jączyński (Sambor I), Tymon Laskowski (Conrad II)</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<h3>Drużyna GKS Żukowo grała w składzie:</h3>\r\n<p>Marcin R&oacute;żyński, Adrian Maciejewski, Marcin Stencel, Bartłomiej Richert, Jarosław Warowny, Miłosz Szymczak, Wojciech Wreza, Grzegorz Kąkol, Kacper Krason, Konrad Pohnke, Kamil Groth, Alan Benkowski, Oskar Wolf, Kacper Młyński, Kamil Klimkiewicz, Mateusz Wadowski.</p>'),
(17, '2013-01-27 13:04:32', 'Piłkarze Ręczni <strong>GKS Żukowo</strong> zainaugurowali 2 rundę 2 Ligi Piłki Ręcznej Mężczyzn. W sobotę o godz. 19:30 w Tczewie w hali przy ul. Kr&oacute;lowej Marysieńki miejscowy Sambor podejmował naszą drużynę. Mecz rozpoczął się od bardzo dobrej gry w obronie naszych zawodnik&oacute;w', 'Ruszyła 2 Liga Piłki Ręcznej', 'Piłkarze Ręczni <strong>GKS Żukowo</strong> zainaugurowali 2 rundę 2 Ligi Piłki Ręcznej Mężczyzn. W sobotę o godz. 19:30 w Tczewie w hali przy ul. Kr&oacute;lowej Marysieńki miejscowy Sambor podejmował naszą drużynę. Mecz rozpoczął się od bardzo dobrej gry w obronie naszych zawodnik&oacute;w w 15 minucie na tablicy widniał wynik 2:9. Kolejne 15 minut to dalsze powiększanie przewagi i ostateczne na przerwe schodzimy przy wyniku 7:18. W drugiej połowie kontrolowalismy wydarzenia na boisku i ostatecznie wynik ustalił Jereczek Rafał na&nbsp; 23:29 rzucajac swoją 4 bramkę w tym spotkaniu.<br><br><strong>Sambor Tczew - GKS Żukowo 23 : 29 ( 7 : 18 )</strong><br>GKS Żukowo: Szynszecki, &ndash; Suchenek 6, Cirocki 6, Lica 4, Hara 2, Seń 4,<br>Marchewicz, Jankowski 6, Zieliński, Wikarjusz 3, Jereczek 4, Szlas, Kosznik.'),
(18, '2013-02-04 08:21:54', 'W drugiej kolejce II Ligi Piłki Ręcznej Mężczyzn nasi piłkarze ręczni podejmowali LKS Kęsowo. Doświadczony zesp&oacute;ł z Kęsowa postawił trudne warunki. Pierwsze piętnaście minut to wyr&oacute;wnane zawody z niewielką przewagą gospodarzy. Kolejne minuty należały juz zdecydowanie do', 'GKS Żukowo kontra LKS Kęsowo', 'W drugiej kolejce II Ligi Piłki Ręcznej Mężczyzn nasi piłkarze ręczni podejmowali LKS Kęsowo. Doświadczony zesp&oacute;ł z Kęsowa postawił trudne warunki. Pierwsze piętnaście minut to wyr&oacute;wnane zawody z niewielką przewagą gospodarzy. Kolejne minuty należały juz zdecydowanie do zawodnik&oacute;w GKS Żukowo. Skuteczna obrona i mniej skuteczny szybki atak (5/9) spowodowały, że w&nbsp; 25 minucie na tablicy widniał wynik 14:8. <br>Wszystko wskazywało na to, że GKS Żukowo rozstrzygnie losy tego spotkania jeszcze przed przerwą. Jednak sześć bramek przewagi nie zrobiło wrażenia na zawodnikach z Kęsowa i postanowili wykorzystać brak koncetracji gospodarzy. Na przerwę schodzimy przy wyniku 15:12.<br> <br>Druga część spotkania to ambitna walka zawodnik&oacute;w z Kęsowa. W 49 minucie LKS Kęsowo zdobywa 2 bramkową przewagę. Kolejne 4 minuty to pięć bramek z rzędu zawodnik&oacute;w GKS Żukowo i na tablicy pojawił się wynik 26:23. Ostatecznie wygrywamy 31:28.<br> <br>GKS Żukowo - Szynszecki, Dymek,&nbsp; Plichta 2, Suchenek 7, Lica 2, Hara 1, Marchewicz 2, Jankowski 3, Wikarjusz 1, Szmidka, Cirocki 1, Seń 4, Kosznik, Zieliński, Szlas, Jereczek 8<br><br>Zapraszamy na zajęcia dzieci rocznik 2002-04!<br><br>-chłopc&oacute;w klas IV rocznik 2002, <strong>wtorek 16.15 - 17.45, czwartek 16.15 - 17.45&nbsp;&nbsp; </strong><br>-chłopcy klas II - III rocznik 2003 - 2004 <strong>czwartek 17.00 - 18.30.</strong>'),
(19, '2013-02-08 15:44:57', 'Juniorzy GKS Żukowo zremisowali wyjazdowy mecz z UKS Conrad Gdańsk. Pierwsza połowa była bardzo wyr&oacute;wnana, żadnej z drużyn nie udało się zdobyć wiekszej niż dwubramkowej przewagi. Wynik do przerwy to 15:15. <br><br>Druga połowa to przest&oacute;j naszych zawodnik&oacute;w i w 49 minucie', 'GKS Żukowo - UKS Conrad II - Juniorzy', 'Juniorzy GKS Żukowo zremisowali wyjazdowy mecz z UKS Conrad Gdańsk. Pierwsza połowa była bardzo wyr&oacute;wnana, żadnej z drużyn nie udało się zdobyć wiekszej niż dwubramkowej przewagi. Wynik do przerwy to 15:15. <br><br>Druga połowa to przest&oacute;j naszych zawodnik&oacute;w i w 49 minucie przewaga Conrada wzrasta do 5 bramek (24:19). W ostatnich 11 minutach przeciwnik zdobywa tylko dwie bramki a my bierzemy się do pracy i zdobywamy bramki seriami. <br><br>Mecz kończy się remisem 26:26. Zwycięstwo było blisko.<br><br>Skład GKS Żukowo :<br>Smentoch Radosław, Bastian Szymon 4, Formela Konrad,Fularczyk Damian, Krason Kacper 8,Rejniak Piotr, Reseman Kacper 3, Richert Filip 8, Block Artur, Richert Przemysław, Szynszecki Damian 3,Chiczewski Szymon<br><br>trener Suchenek Adrian'),
(20, '2013-02-10 13:44:05', 'W trzeciej kolejce czekał nas wyjazd do Koronowa. Na mecz udaliśmy się w nieco okrojonym składzie. Poczatek spotkania to prowadzenie gospodarzy 2:0. Trochę mobilizacji i w 10 minucie prowadzimy 4:6. Kolejne minuty to walka bramka za bramkę. Od 24 minuty gospodarze zdobywają po naszych błedach', 'Trzecia kolejka 2 Ligi Piłki Ręcznej', 'W trzeciej kolejce czekał nas wyjazd do Koronowa. Na mecz udaliśmy się w nieco okrojonym składzie. Poczatek spotkania to prowadzenie gospodarzy 2:0. Trochę mobilizacji i w 10 minucie prowadzimy 4:6. Kolejne minuty to walka bramka za bramkę. Od 24 minuty gospodarze zdobywają po naszych błedach trzy bramki. Wynik do przerwy 15:11 dla Koronowa.<br><br>Druga połowa to w naszym wykonaniu zdecydowanie lepsza gra w obronie. Przez 20 minut ciężko pracujemy żeby odrobić przespaną końc&oacute;wke pierwszej połowy. Na dziesięć minut przed końcem po bardzo efektownej i skutecznie wykonanej akcji wychodzimy na prowadzenie 21:22. Gospodarze do końca walczyli o jeden punkt. Spotkanie kończy się wynikiem 25:25.<br><br>GKS Żukowo: Kochański, Rajkowski, Marchewicz 2, Zieliński 3, Szmidka 2, Kosznik, Jereczek 7, Lica 1, Suchenek 4, Hara 4,Jankowski 2.<br><br>Kolejne spotkanie zagramy r&oacute;wnież na wyjeździe z MKS Brodnica.'),
(21, '2013-03-12 20:55:44', 'Bardzo dobre mecze rozegrali nasi piłkarze ręczni w II Turnieju P&oacute;łfinałowym Wojew&oacute;dzkiej Ligi Piłki Ręcznej Młodzik&oacute;w, wygrywając zdecydowanie dwa decydujące o awansie mecze z MTS-em Kwidzyn i Gdańskim &bdquo;Wybrzeżem&rdquo;. Jedno bramkowa porażka z Samborem Tczew', 'Młodzicy w finale', 'Bardzo dobre mecze rozegrali nasi piłkarze ręczni w II Turnieju P&oacute;łfinałowym Wojew&oacute;dzkiej Ligi Piłki Ręcznej Młodzik&oacute;w, wygrywając zdecydowanie dwa decydujące o awansie mecze z MTS-em Kwidzyn i Gdańskim &bdquo;Wybrzeżem&rdquo;. Jedno bramkowa porażka z Samborem Tczew nie miała już znaczenia przy awansie do fazy finałowej, w kt&oacute;rej wystąpią cztery najlepsze drużyny z wojew&oacute;dztwa pomorskiego: UKS &bdquo;Conrad&rdquo; Gdańsk, UKS &bdquo;Si&oacute;demka-Osowa&rdquo; Gdańsk, MKS &bdquo;Sambor&rdquo; Tczew i GKS Żukowo.<br><br><strong>Skład naszej drużyny:</strong> Marcin R&oacute;żyński i Adrian Maciejewski w bramce, Kacper Krason, Kamil Groth, Konrad Pohnke,&nbsp; Oskar Wolf, Miłosz Szymczak, Alan Benkowski, Jarek Warowny, Kacper Młyński, Mateusz Wadowski, Grzegorz Kąkol, Bartek Richert, Jakub Richert, Marcin Stencel.<br><br>Wyniki mecz&oacute;w:\r\n<ul>\r\n<li><strong>Żukowo</strong> &ndash; Kwidzyn 22 : 11 (10:4)</li>\r\n<li><strong>Żukowo</strong> &ndash; Gdańsk 23 : 12 (10:5)</li>\r\n<li>Tczew &ndash; <strong>Żukowo</strong> 18 : 17 (11:10)</li>\r\n</ul>'),
(22, '2013-06-14 12:00:35', '<p><strong>Młodzicy na III miejscu w Lidze Wojew&oacute;dzkiej</strong><br>Duży sukces odnieśli nasi chłopcy w Wojew&oacute;dzkiej Lidze Piłki Ręcznej Młodzik&oacute;w w sezonie 2012/13 zdobywając brązowy medal. Drużyna grała w składzie: Marcin R&oacute;żyński, Kacper Krason, Konrad Pohnke,', 'Młodzicy na III miejscu w Lidze Wojewódzkiej ', '<p><strong>Młodzicy na III miejscu w Lidze Wojew&oacute;dzkiej</strong><br>Duży sukces odnieśli nasi chłopcy w Wojew&oacute;dzkiej Lidze Piłki Ręcznej Młodzik&oacute;w w sezonie 2012/13 zdobywając brązowy medal. Drużyna grała w składzie: Marcin R&oacute;żyński, Kacper Krason, Konrad Pohnke, Kamil Groth, Oskar Wolf, Miłosz Szymczak, Marcin Stencel, Mateusz Wadowski, Grzegorz Kąkol, Bartłomiej Richert, Jakub Richert, Adrian Maciejewski, Alan Benkowski, Wojciech Wreza, Jarosław Warowny. Wszyscy w/w są uczniami Gimnazjum w Żukowie sportowych klas II B i I B o profilu piłki ręcznej. Trenerem drużyny jest Marek Marchewicz.<br><br>Tabela Najlepszych Strzelc&oacute;w<br><br></p>\r\n<table class=\\"article_table\\">\r\n<tbody>\r\n<tr>\r\n<td>\r\n<p><strong>1.</strong></p>\r\n</td>\r\n<td>\r\n<p><strong>KRASON</strong></p>\r\n</td>\r\n<td>\r\n<p><strong>KACPER</strong></p>\r\n</td>\r\n<td>\r\n<p><strong>G K S Żukowo</strong></p>\r\n</td>\r\n<td>\r\n<p><strong>146 bramek</strong></p>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td>\r\n<p>2.</p>\r\n</td>\r\n<td>\r\n<p>MISZCZAK</p>\r\n</td>\r\n<td>\r\n<p>MIKOŁAJ</p>\r\n</td>\r\n<td>\r\n<p>ZSP 1 Sok&oacute;ł Kościerzyna</p>\r\n</td>\r\n<td>\r\n<p>135</p>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td>\r\n<p>3.</p>\r\n</td>\r\n<td>\r\n<p>KWIATKOWSKI</p>\r\n</td>\r\n<td>\r\n<p>PAWEŁ</p>\r\n</td>\r\n<td>\r\n<p>Conrad &ndash; Wybrzeże Gdańsk</p>\r\n</td>\r\n<td>\r\n<p>129 &bdquo;</p>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td>\r\n<p>4.</p>\r\n</td>\r\n<td>\r\n<p>GUZIEWICZ</p>\r\n</td>\r\n<td>\r\n<p>ALAN</p>\r\n</td>\r\n<td>\r\n<p>MTS II Kwidzyn</p>\r\n</td>\r\n<td>\r\n<p>121 &bdquo;</p>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td>\r\n<p><strong>5.</strong></p>\r\n</td>\r\n<td>\r\n<p><strong>POHNKE</strong></p>\r\n</td>\r\n<td>\r\n<p><strong>KONRAD</strong></p>\r\n</td>\r\n<td>\r\n<p><strong>GKS Żukowo</strong></p>\r\n</td>\r\n<td>\r\n<p><strong>105</strong></p>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td>\r\n<p>6.</p>\r\n</td>\r\n<td>\r\n<p>WOŻNIAK</p>\r\n</td>\r\n<td>\r\n<p>PAWEŁ</p>\r\n</td>\r\n<td>\r\n<p>MKS Sambor I Tczew</p>\r\n</td>\r\n<td>\r\n<p>102</p>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td>\r\n<p>7.</p>\r\n</td>\r\n<td>\r\n<p>CYGERT</p>\r\n</td>\r\n<td>\r\n<p>MIKOŁAJ</p>\r\n</td>\r\n<td>\r\n<p>Conrad - Osowa</p>\r\n</td>\r\n<td>\r\n<p>101 &bdquo;</p>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td>\r\n<p>8.</p>\r\n</td>\r\n<td>\r\n<p>WARUSZEWSKI</p>\r\n</td>\r\n<td>\r\n<p>RAFAŁ</p>\r\n</td>\r\n<td>\r\n<p>MKS Sambor I Tczew</p>\r\n</td>\r\n<td>\r\n<p>93</p>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td>\r\n<p>9.</p>\r\n</td>\r\n<td>\r\n<p>SZKUDLAREK</p>\r\n</td>\r\n<td>\r\n<p>LESZEK</p>\r\n</td>\r\n<td>\r\n<p>Conrad Gdańsk</p>\r\n</td>\r\n<td>\r\n<p>87</p>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td>\r\n<p>9.</p>\r\n</td>\r\n<td>\r\n<p>WILKOWSKI</p>\r\n</td>\r\n<td>\r\n<p>MARCIN</p>\r\n</td>\r\n<td>\r\n<p>Conrad Gdańsk</p>\r\n</td>\r\n<td>\r\n<p>87</p>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<br>Ostateczna kolejność sezonu 2012-2013<br><br>\r\n<table class=\\"article_table\\">\r\n<tbody>\r\n<tr>\r\n<td>\r\n<p>1.</p>\r\n</td>\r\n<td>\r\n<p>MKS&nbsp; Sambor</p>\r\n</td>\r\n<td>\r\n<p>Tczew</p>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td>\r\n<p>2.</p>\r\n</td>\r\n<td>\r\n<p>UKS&nbsp; Conrad</p>\r\n</td>\r\n<td>\r\n<p>Gdańsk</p>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td>\r\n<p><strong>3.</strong></p>\r\n</td>\r\n<td>\r\n<p><strong>GKS</strong></p>\r\n</td>\r\n<td>\r\n<p><strong>Żukowo</strong></p>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td>\r\n<p>4.</p>\r\n</td>\r\n<td>\r\n<p>UKS&nbsp; Conrad &ndash; Osowa</p>\r\n</td>\r\n<td>\r\n<p>Gdańsk - Osowa</p>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td>\r\n<p>5.</p>\r\n</td>\r\n<td>\r\n<p>UKS Wybrzeże - Conrad</p>\r\n</td>\r\n<td>\r\n<p>Gdańsk</p>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td>\r\n<p>6.</p>\r\n</td>\r\n<td>\r\n<p>ZSP 1&nbsp; Sok&oacute;ł</p>\r\n</td>\r\n<td>\r\n<p>Kościerzyna</p>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td>\r\n<p>7.</p>\r\n</td>\r\n<td>\r\n<p>MTS&nbsp;&nbsp; I</p>\r\n</td>\r\n<td>\r\n<p>Kwidzyn</p>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td>\r\n<p>8.</p>\r\n</td>\r\n<td>\r\n<p>MTS&nbsp;&nbsp; II</p>\r\n</td>\r\n<td>\r\n<p>Kwidzyn</p>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td>\r\n<p>9.</p>\r\n</td>\r\n<td>\r\n<p>U K S</p>\r\n</td>\r\n<td>\r\n<p>Nowa&nbsp; Karczma</p>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td>\r\n<p>10.</p>\r\n</td>\r\n<td>\r\n<p>GUKS&nbsp;&nbsp;&nbsp;&nbsp; Bellator</p>\r\n</td>\r\n<td>\r\n<p>Ryjewo</p>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td>\r\n<p>11.</p>\r\n</td>\r\n<td>\r\n<p>GKS&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Cartusia</p>\r\n</td>\r\n<td>\r\n<p>Kartuzy</p>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td>\r\n<p>12.</p>\r\n</td>\r\n<td>\r\n<p>MKS&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Sambor&nbsp;&nbsp; II</p>\r\n</td>\r\n<td>\r\n<p>Tczew</p>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td>\r\n<p>13.</p>\r\n</td>\r\n<td>\r\n<p>UKS&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Olimp</p>\r\n</td>\r\n<td>\r\n<p>Bolszewo</p>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td>\r\n<p>14.</p>\r\n</td>\r\n<td>\r\n<p>MLKS&nbsp;&nbsp;&nbsp; Borowiak</p>\r\n</td>\r\n<td>\r\n<p>Czersk</p>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td>\r\n<p>15.</p>\r\n</td>\r\n<td>\r\n<p>ZSP 1&nbsp;&nbsp;&nbsp;&nbsp; Sok&oacute;ł&nbsp; &nbsp;II</p>\r\n</td>\r\n<td>\r\n<p>Kościerzyna</p>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>');

-- --------------------------------------------------------

--
-- Table structure for table `foto`
--

CREATE TABLE IF NOT EXISTS `foto` (
  `foto_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_polish_ci NOT NULL,
  `article_id` int(11) NOT NULL,
  PRIMARY KEY (`foto_id`),
  KEY `article_id` (`article_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci AUTO_INCREMENT=106 ;

--
-- Dumping data for table `foto`
--

INSERT INTO `foto` (`foto_id`, `name`, `article_id`) VALUES
(15, 'DSC_4368.jpg', 9),
(16, 'DSC_4375.jpg', 9),
(17, 'DSC_4376.jpg', 9),
(18, 'DSC_4377.jpg', 9),
(19, 'DSC_4378.jpg', 9),
(20, 'DSC_4383.jpg', 9),
(21, 'DSC_4386.jpg', 9),
(22, 'DSC_8658.jpg', 6),
(23, 'DSC_8665.jpg', 6),
(24, 'DSC_8812.jpg', 6),
(25, 'DSC_8822.jpg', 6),
(26, 'DSC_8828.jpg', 6),
(27, 'DSC_8839.jpg', 6),
(29, 'DSC_8384_1.jpg', 5),
(30, 'DSC_8391.jpg', 5),
(31, 'DSC_8405.jpg', 5),
(32, 'DSC_8415.jpg', 5),
(33, 'DSC_8452.jpg', 5),
(34, 'DSC_8457.jpg', 5),
(35, 'DSC_9095.jpg', 11),
(36, 'DSC_9112.jpg', 11),
(37, 'DSC_9115.jpg', 11),
(38, 'DSC_9118.jpg', 11),
(39, 'DSC_9155.jpg', 11),
(40, 'DSC_9166.jpg', 11),
(41, 'DSC_9203.jpg', 11),
(42, 'DSC_9229.jpg', 11),
(43, 'DSC_9275.jpg', 11),
(44, 'DSC_4529.jpg', 13),
(45, 'DSC_4533.jpg', 13),
(46, 'DSC_4538.jpg', 13),
(47, 'DSC_4547.jpg', 13),
(48, 'DSC_4548.jpg', 13),
(49, 'DSC_4552.jpg', 13),
(50, 'DSC_4587.jpg', 13),
(51, 'DSC_4590.jpg', 13),
(52, 'DSC_4599.jpg', 13),
(53, 'DSC_4608.jpg', 13),
(54, 'DSC_4617.jpg', 13),
(55, 'DSC_9384.jpg', 15),
(56, 'DSC_9385.jpg', 15),
(57, 'DSC_9432.jpg', 15),
(58, 'DSC_9436.jpg', 15),
(59, 'DSC_9439.jpg', 15),
(60, 'DSC_9464.jpg', 15),
(61, 'DSC_9473.jpg', 15),
(62, 'DSC_9479.jpg', 15),
(63, 'DSC_0371.jpg', 16),
(64, 'DSC_0373.jpg', 16),
(65, 'DSC_0375.jpg', 16),
(66, 'DSC_0379.jpg', 16),
(67, 'DSC_0381.jpg', 16),
(68, 'DSC_0382.jpg', 16),
(69, 'DSC_0377.jpg', 16),
(70, 'DSC_0103.jpg', 18),
(71, 'DSC_0027.jpg', 18),
(72, 'DSC_0028.jpg', 18),
(73, 'DSC_0030.jpg', 18),
(74, 'DSC_0034.jpg', 18),
(75, 'DSC_0044.jpg', 18),
(76, 'DSC_0045.jpg', 18),
(77, 'DSC_0058.jpg', 18),
(78, 'DSC_0059.jpg', 18),
(79, 'DSC_0061.jpg', 18),
(80, 'DSC_0064.jpg', 18),
(81, 'DSC_0066.jpg', 18),
(82, 'DSC_0067.jpg', 18),
(83, 'DSC_0069.jpg', 18),
(84, 'DSC_0075.jpg', 18),
(85, 'DSC_0078.jpg', 18),
(86, 'DSC_0090.jpg', 18),
(87, 'DSC_0080.jpg', 18),
(88, 'DSC_0097.jpg', 18),
(89, 'DSC_0104.jpg', 18),
(90, 'DSC_0105.jpg', 18),
(91, 'DSC_0125.jpg', 18),
(92, 'DSC_0185.jpg', 18),
(93, 'DSC_0174.jpg', 18),
(94, 'DSC_0127.jpg', 18),
(95, 'DSC_0145.jpg', 18),
(96, 'DSC_0179.jpg', 18),
(97, 'juniorzy_97.jpg', 19),
(98, 'DSC_0550.JPG', 21),
(100, 'DSC_0503.jpg', 22),
(101, 'DSC_0564.jpg', 22),
(102, 'DSC_0914.jpg', 22),
(103, 'DSC_0927.jpg', 22),
(104, 'DSC_1111.jpg', 22),
(105, 'DSC_0499.jpg', 22);

-- --------------------------------------------------------

--
-- Table structure for table `league_table`
--

CREATE TABLE IF NOT EXISTS `league_table` (
  `position_id` int(10) unsigned NOT NULL,
  `team` varchar(100) COLLATE utf8_polish_ci NOT NULL,
  `points` int(2) NOT NULL,
  `wins` int(2) NOT NULL,
  `losses` int(2) NOT NULL,
  `ties` int(2) NOT NULL,
  `goals_scored` int(3) NOT NULL,
  `goals_allowed` int(3) NOT NULL,
  `thumbnail` varchar(100) COLLATE utf8_polish_ci DEFAULT NULL,
  PRIMARY KEY (`position_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Dumping data for table `league_table`
--

INSERT INTO `league_table` (`position_id`, `team`, `points`, `wins`, `losses`, `ties`, `goals_scored`, `goals_allowed`, `thumbnail`) VALUES
(1, 'MKS Grudziądz', 29, 17, 4, 1, 636, 565, 'mksgrudziadz.png'),
(2, 'Alfa 99 Strzelno', 16, 14, 7, 0, 604, 650, 'alfa.png'),
(3, 'Gwardia Koszalin', 21, 12, 6, 3, 558, 554, 'KS_Gwardia_Koszalin.png'),
(4, 'Sokół Gdańsk', 33, 18, 3, 1, 707, 569, 'sokol.png'),
(5, 'AZS UKW Bydgoszcz', 36, 17, 3, 2, 666, 572, 'bydgoszcz.png'),
(6, 'Energetyk Gryfino', 38, 22, 1, 0, 754, 603, 'ks_energetyk_gryfino.png'),
(7, 'GKS Żukowo', 20, 12, 6, 4, 574, 600, 'gks_zukowo.png'),
(8, 'Sambor Tczew', 19, 14, 5, 3, 591, 642, 'sambortczew.png'),
(9, 'LKS Kęsowo', 8, 11, 9, 2, 527, 654, 'lkskesowo.png'),
(10, 'BS Korona Koronowo', 9, 12, 9, 1, 589, 684, 'korona_koronowo.png'),
(11, 'MKS Brodnica', 19, 13, 8, 1, 610, 634, 'mksbrodnica.png'),
(12, 'Tytani Wejherowo', 22, 15, 5, 2, 582, 582, 'tytani.png'),
(13, 'Szczypiorniak Olsztyn', 21, 11, 6, 5, 533, 556, 'szczypiorniak_olsztyn.png'),
(14, 'AZS UWM Olsztyn', 17, 14, 8, 1, 589, 655, 'azs_uwm_olsztyn.png');

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE IF NOT EXISTS `menu` (
  `menu_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(128) COLLATE utf8_polish_ci NOT NULL,
  `numer` int(10) unsigned NOT NULL,
  PRIMARY KEY (`menu_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci AUTO_INCREMENT=7 ;

--
-- Dumping data for table `menu`
--

INSERT INTO `menu` (`menu_id`, `title`, `numer`) VALUES
(1, 'Aktualności', 1),
(2, '2 Liga', 2),
(3, 'Juniorzy', 2),
(4, 'Młodzicy', 2),
(5, 'Klub', 3),
(6, 'Kontakt', 4);

-- --------------------------------------------------------

--
-- Table structure for table `player`
--

CREATE TABLE IF NOT EXISTS `player` (
  `player_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fname` varchar(100) COLLATE utf8_polish_ci NOT NULL,
  `sname` varchar(100) COLLATE utf8_polish_ci NOT NULL,
  `position` varchar(100) COLLATE utf8_polish_ci NOT NULL,
  `email` varchar(100) COLLATE utf8_polish_ci DEFAULT NULL,
  `height` smallint(3) NOT NULL,
  `games` smallint(3) NOT NULL,
  `number` smallint(3) NOT NULL,
  `status` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`player_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci AUTO_INCREMENT=23 ;

--
-- Dumping data for table `player`
--

INSERT INTO `player` (`player_id`, `fname`, `sname`, `position`, `email`, `height`, `games`, `number`, `status`) VALUES
(1, 'Krzysztof', 'Cirocki', '1', NULL, 178, 0, 6, 1),
(2, 'Robert', 'Dymek', '5', NULL, 184, 0, 21, 1),
(3, 'Maciej', 'Hara', '2', NULL, 176, 0, 22, 1),
(4, 'Błażej', 'Jankowski', '1', NULL, 185, 0, 10, 1),
(5, 'Rafał', 'Jereczek', '2', NULL, 183, 0, 7, 1),
(6, 'Artur', 'Kitowski', '4', NULL, 182, 0, 11, 1),
(8, 'Arkadiusz', 'Kosznik', '4', NULL, 184, 0, 9, 1),
(9, 'Sławomir', 'Lica', '1', NULL, 195, 0, 13, 1),
(10, 'Łukasz', 'Plichta', '4', NULL, 180, 0, 2, 1),
(11, 'Szymon', 'Marchewicz', '2', NULL, 184, 0, 14, 1),
(12, 'Filip', 'Ratkowski', '3', NULL, 180, 0, 4, 1),
(13, 'Marcin', 'Seń', '4', NULL, 178, 0, 8, 1),
(14, 'Adrian', 'Suchenek', '1', NULL, 190, 0, 15, 1),
(15, 'Paweł', 'Szlas', '4', NULL, 178, 0, 3, 1),
(16, 'Dawid', 'Szmidka', '4', NULL, 176, 0, 5, 1),
(17, 'Maciej', 'Szynszecki', '5', NULL, 183, 0, 1, 1),
(18, 'Mateusz', 'Wenzel', '1', NULL, 186, 0, 17, 0),
(19, 'Paweł', 'Wikarjusz', '3', NULL, 188, 0, 8, 1),
(20, 'Patryk', 'Załuski', '1', NULL, 187, 0, 0, NULL),
(21, 'Maciej', 'Zieliński', '3', NULL, 175, 0, 0, NULL),
(22, 'Paweł', 'Wysokiński', '4', NULL, 178, 0, 17, 1);

-- --------------------------------------------------------

--
-- Table structure for table `player_to_season`
--

CREATE TABLE IF NOT EXISTS `player_to_season` (
  `player_id` int(11) NOT NULL,
  `season_id` smallint(3) NOT NULL,
  PRIMARY KEY (`player_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Dumping data for table `player_to_season`
--

INSERT INTO `player_to_season` (`player_id`, `season_id`) VALUES
(1, 1),
(2, 1),
(3, 1),
(4, 1),
(5, 1),
(6, 1),
(7, 1),
(8, 1),
(9, 1),
(10, 1),
(11, 1),
(12, 1),
(13, 1),
(14, 1),
(15, 1),
(16, 1),
(17, 1),
(18, 1),
(19, 1),
(20, 1),
(21, 1),
(22, 2);

-- --------------------------------------------------------

--
-- Table structure for table `position`
--

CREATE TABLE IF NOT EXISTS `position` (
  `position_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `position` varchar(100) COLLATE utf8_polish_ci NOT NULL,
  PRIMARY KEY (`position_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci AUTO_INCREMENT=6 ;

--
-- Dumping data for table `position`
--

INSERT INTO `position` (`position_id`, `position`) VALUES
(1, 'rozgrywający'),
(2, 'środkowy'),
(3, 'obrotowy'),
(4, 'skrzydłowy'),
(5, 'bramkarz');

-- --------------------------------------------------------

--
-- Table structure for table `schedule`
--

CREATE TABLE IF NOT EXISTS `schedule` (
  `game_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `round_id` int(10) unsigned NOT NULL,
  `host_id` int(2) NOT NULL,
  `host_goals` int(2) NOT NULL,
  `visitor_id` int(2) NOT NULL,
  `visitor_goals` int(2) NOT NULL,
  `season_id` int(2) NOT NULL,
  `date` datetime DEFAULT NULL,
  PRIMARY KEY (`game_id`),
  KEY `fk_season_id` (`season_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci AUTO_INCREMENT=183 ;

--
-- Dumping data for table `schedule`
--

INSERT INTO `schedule` (`game_id`, `round_id`, `host_id`, `host_goals`, `visitor_id`, `visitor_goals`, `season_id`, `date`) VALUES
(1, 1, 1, 32, 14, 18, 1, '2012-09-15 18:00:00'),
(2, 1, 2, 26, 13, 29, 1, '2012-09-15 18:00:00'),
(3, 1, 3, 25, 12, 19, 1, '2012-09-15 18:00:00'),
(4, 1, 4, 33, 11, 23, 1, '2012-09-15 18:00:00'),
(5, 1, 5, 35, 10, 27, 1, '2012-09-15 18:00:00'),
(6, 1, 6, 32, 9, 21, 1, '2012-09-15 18:00:00'),
(7, 1, 7, 30, 8, 30, 1, '2012-09-15 18:00:00'),
(8, 2, 14, 22, 8, 24, 1, '2012-09-22 18:00:00'),
(9, 2, 9, 22, 7, 23, 1, '2012-09-22 18:00:00'),
(10, 2, 10, 24, 6, 34, 1, '2012-09-22 18:00:00'),
(11, 2, 11, 20, 5, 22, 1, '2012-09-22 18:00:00'),
(12, 2, 12, 21, 4, 28, 1, '2012-09-22 18:00:00'),
(13, 2, 3, 28, 13, 19, 1, '2012-09-22 18:00:00'),
(14, 2, 1, 37, 2, 25, 1, '2012-09-22 18:00:00'),
(15, 3, 2, 28, 14, 26, 1, '2012-09-29 18:00:00'),
(16, 3, 3, 30, 1, 25, 1, '2012-09-29 18:00:00'),
(17, 3, 4, 32, 13, 20, 1, '2012-09-29 18:00:00'),
(18, 3, 5, 31, 12, 25, 1, '2012-09-29 18:00:00'),
(19, 3, 6, 28, 11, 31, 1, '2012-09-29 18:00:00'),
(20, 3, 7, 27, 10, 31, 1, '2012-09-29 18:00:00'),
(21, 3, 8, 30, 9, 29, 1, '2012-09-29 18:00:00'),
(22, 4, 14, 31, 9, 26, 1, '2012-10-06 18:00:00'),
(23, 4, 10, 30, 8, 29, 1, '2012-10-06 18:00:00'),
(24, 4, 11, 29, 7, 27, 1, '2012-10-06 18:00:00'),
(25, 4, 12, 24, 6, 33, 1, '2012-10-06 18:00:00'),
(26, 4, 13, 27, 5, 25, 1, '2012-10-06 18:00:00'),
(27, 4, 1, 30, 4, 28, 1, '2012-10-06 18:00:00'),
(28, 4, 2, 31, 3, 30, 1, '2012-10-06 18:00:00'),
(29, 5, 3, 31, 14, 27, 1, '2012-10-13 18:00:00'),
(30, 5, 4, 34, 2, 28, 1, '2012-10-13 18:00:00'),
(31, 5, 5, 32, 1, 30, 1, '2012-10-13 18:00:00'),
(32, 5, 6, 33, 13, 27, 1, '2012-10-13 18:00:00'),
(33, 5, 7, 26, 12, 22, 1, '2012-10-13 18:00:00'),
(34, 5, 8, 28, 11, 23, 1, '2012-10-13 18:00:00'),
(35, 5, 9, 33, 10, 31, 1, '2012-10-13 18:00:00'),
(36, 6, 14, 30, 10, 25, 1, '2012-10-20 18:00:00'),
(37, 6, 11, 35, 9, 35, 1, '2012-10-20 18:00:00'),
(38, 6, 12, 20, 8, 28, 1, '2012-10-20 18:00:00'),
(39, 6, 13, 25, 7, 25, 1, '2012-10-20 18:00:00'),
(40, 6, 1, 30, 6, 39, 1, '2012-10-20 18:00:00'),
(41, 6, 2, 29, 5, 31, 1, '2012-10-20 18:00:00'),
(42, 6, 3, 24, 4, 30, 1, '2012-10-20 18:00:00'),
(43, 7, 4, 42, 14, 26, 1, '2012-10-27 18:00:00'),
(44, 7, 5, 32, 3, 27, 1, '2012-10-27 18:00:00'),
(45, 7, 6, 32, 2, 23, 1, '2012-10-27 18:00:00'),
(46, 7, 7, 28, 1, 32, 1, '2012-10-27 18:00:00'),
(47, 7, 8, 22, 13, 21, 1, '2012-10-27 18:00:00'),
(48, 7, 9, 22, 12, 35, 1, '2012-10-27 18:00:00'),
(49, 7, 10, 32, 11, 23, 1, '2012-10-27 18:00:00'),
(50, 8, 14, 31, 11, 27, 1, '2012-11-10 18:00:00'),
(51, 8, 12, 30, 10, 25, 1, '2012-11-10 18:00:00'),
(52, 8, 13, 23, 9, 18, 1, '2012-11-10 18:00:00'),
(53, 8, 1, 29, 8, 25, 1, '2012-11-10 18:00:00'),
(54, 8, 2, 32, 7, 27, 1, '2012-11-10 18:00:00'),
(55, 8, 3, 30, 6, 37, 1, '2012-11-10 18:00:00'),
(56, 8, 4, 29, 5, 32, 1, '2012-11-10 18:00:00'),
(57, 9, 5, 35, 14, 25, 1, '2012-11-17 18:00:00'),
(58, 9, 6, 31, 4, 26, 1, '2012-11-17 18:00:00'),
(59, 9, 7, 22, 3, 22, 1, '2012-11-17 18:00:00'),
(60, 9, 8, 30, 2, 33, 1, '2012-11-17 18:00:00'),
(61, 9, 9, 14, 1, 28, 1, '2012-11-17 18:00:00'),
(62, 9, 10, 28, 13, 30, 1, '2012-11-17 18:00:00'),
(63, 9, 11, 25, 12, 27, 1, '2012-11-17 18:00:00'),
(64, 10, 14, 26, 12, 29, 1, '2012-11-24 18:00:00'),
(65, 10, 13, 22, 11, 17, 1, '2012-11-24 18:00:00'),
(66, 10, 1, 27, 10, 20, 1, '2012-11-24 18:00:00'),
(67, 10, 2, 30, 9, 28, 1, '2012-11-24 18:00:00'),
(68, 10, 3, 27, 8, 27, 1, '2012-11-24 18:00:00'),
(69, 10, 4, 18, 7, 30, 1, '2012-11-24 18:00:00'),
(70, 10, 5, 37, 6, 31, 1, '2012-11-24 18:00:00'),
(71, 11, 6, 29, 14, 23, 1, '2012-12-01 18:00:00'),
(72, 11, 7, 27, 5, 25, 1, '2012-12-01 18:00:00'),
(73, 11, 8, 25, 4, 31, 1, '2012-12-01 18:00:00'),
(74, 11, 9, 30, 3, 27, 1, '2012-12-01 18:00:00'),
(75, 11, 10, 24, 2, 32, 1, '2012-12-01 18:00:00'),
(76, 11, 11, 30, 1, 34, 1, '2012-12-01 18:00:00'),
(77, 11, 12, 27, 13, 27, 1, '2012-12-01 18:00:00'),
(78, 12, 14, 20, 13, 20, 1, '2012-12-08 18:00:00'),
(79, 12, 1, 24, 12, 26, 1, '2012-12-08 18:00:00'),
(80, 12, 2, 35, 11, 28, 1, '2012-12-08 18:00:00'),
(81, 12, 3, 21, 10, 25, 1, '2012-12-08 18:00:00'),
(82, 12, 4, 39, 9, 23, 1, '2012-12-08 18:00:00'),
(83, 12, 5, 41, 8, 22, 1, '2012-12-08 18:00:00'),
(84, 12, 6, 38, 7, 23, 1, '2012-12-08 18:00:00'),
(85, 13, 7, 29, 14, 27, 1, '2012-12-15 18:00:00'),
(86, 13, 8, 24, 6, 35, 1, '2012-12-15 18:00:00'),
(87, 13, 9, 24, 5, 28, 1, '2012-12-15 18:00:00'),
(88, 13, 10, 31, 4, 38, 1, '2012-12-15 18:00:00'),
(89, 13, 11, 32, 3, 27, 1, '2012-12-15 18:00:00'),
(90, 13, 12, 30, 2, 25, 1, '2012-12-15 18:00:00'),
(91, 13, 13, 24, 1, 25, 1, '2012-12-15 18:00:00'),
(92, 14, 14, 26, 1, 25, 1, '2013-01-26 18:00:00'),
(93, 14, 13, 26, 2, 23, 1, '2013-01-26 18:00:00'),
(94, 14, 12, 27, 3, 29, 1, '2013-01-26 18:00:00'),
(95, 14, 11, 27, 4, 22, 1, '2013-01-26 18:00:00'),
(96, 14, 10, 23, 5, 26, 1, '2013-01-26 18:00:00'),
(97, 14, 9, 20, 6, 35, 1, '2013-01-26 18:00:00'),
(98, 14, 8, 23, 7, 29, 1, '2013-01-26 18:00:00'),
(99, 15, 8, 21, 14, 31, 1, '2013-02-02 18:00:00'),
(100, 15, 7, 31, 9, 28, 1, '2013-02-02 18:00:00'),
(101, 15, 6, 35, 10, 22, 1, '2013-02-02 18:00:00'),
(102, 15, 5, 28, 11, 18, 1, '2013-02-02 18:00:00'),
(103, 15, 4, 28, 12, 23, 1, '2013-02-02 18:00:00'),
(104, 15, 13, 22, 3, 22, 1, '2013-02-02 18:00:00'),
(105, 15, 2, 31, 1, 38, 1, '2013-02-02 18:00:00'),
(106, 16, 14, 33, 2, 31, 1, '2013-02-09 18:00:00'),
(107, 16, 1, 17, 3, 20, 1, '2013-02-09 18:00:00'),
(108, 16, 13, 22, 4, 28, 1, '2013-02-09 18:00:00'),
(109, 16, 12, 23, 5, 25, 1, '2013-02-09 18:00:00'),
(110, 16, 11, 25, 6, 37, 1, '2013-02-09 18:00:00'),
(111, 16, 10, 25, 7, 25, 1, '2013-02-09 18:00:00'),
(112, 16, 9, 22, 8, 27, 1, '2013-02-09 18:00:00'),
(113, 17, 9, 20, 14, 22, 1, '2013-02-16 18:00:00'),
(114, 17, 8, 36, 10, 28, 1, '2013-02-16 18:00:00'),
(115, 17, 11, 30, 7, 27, 1, '2013-02-17 15:00:00'),
(116, 17, 6, 28, 12, 23, 1, '2013-02-16 18:00:00'),
(117, 17, 5, 34, 13, 25, 1, '2013-02-16 18:00:00'),
(118, 17, 4, 34, 1, 21, 1, '2013-02-16 18:00:00'),
(119, 17, 3, 0, 2, 0, 1, '2013-02-16 18:00:00'),
(120, 18, 14, 21, 3, 22, 1, '2013-02-23 18:00:00'),
(121, 18, 2, 31, 4, 40, 1, '2013-02-23 18:00:00'),
(122, 18, 1, 25, 5, 25, 1, '2013-02-23 18:00:00'),
(123, 18, 13, 22, 6, 28, 1, '2013-02-23 18:00:00'),
(124, 18, 12, 28, 7, 22, 1, '2013-02-23 17:00:00'),
(125, 18, 11, 38, 8, 30, 1, '2013-02-23 18:00:00'),
(126, 18, 10, 27, 9, 29, 1, '2013-02-23 18:00:00'),
(127, 19, 10, 26, 14, 28, 1, '2013-03-02 18:00:00'),
(128, 19, 9, 20, 11, 35, 1, '2013-03-02 18:00:00'),
(129, 19, 8, 29, 12, 29, 1, '2013-03-02 18:00:00'),
(130, 19, 7, 23, 13, 29, 1, '2013-03-02 18:00:00'),
(131, 19, 6, 28, 1, 31, 1, '2013-03-02 18:00:00'),
(132, 19, 5, 32, 2, 28, 1, '2013-03-02 18:00:00'),
(133, 19, 4, 35, 3, 24, 1, '2013-03-02 18:00:00'),
(134, 20, 14, 21, 4, 38, 1, '2013-03-16 18:00:00'),
(135, 20, 3, 34, 5, 26, 1, '2013-03-16 18:00:00'),
(136, 20, 2, 28, 6, 37, 1, '2013-03-16 18:00:00'),
(137, 20, 1, 37, 7, 18, 1, '2013-03-16 18:00:00'),
(138, 20, 13, 33, 8, 23, 1, '2013-03-16 18:00:00'),
(139, 20, 12, 31, 9, 20, 1, '2013-03-16 18:00:00'),
(140, 20, 11, 43, 10, 32, 1, '2013-03-16 18:00:00'),
(141, 21, 11, 32, 14, 28, 1, '2013-03-23 18:00:00'),
(142, 21, 10, 29, 12, 31, 1, '2013-03-23 18:00:00'),
(143, 21, 9, 22, 13, 22, 1, '2013-03-23 18:00:00'),
(144, 21, 8, 26, 1, 32, 1, '2013-03-23 18:00:00'),
(145, 21, 7, 26, 2, 28, 1, '2013-03-23 18:00:00'),
(146, 21, 6, 31, 3, 29, 1, '2013-03-23 18:00:00'),
(147, 21, 5, 32, 4, 32, 1, '2013-03-23 18:00:00'),
(148, 22, 14, 0, 5, 0, 1, '2013-04-06 18:00:00'),
(149, 22, 4, 0, 6, 0, 1, '2013-04-06 18:00:00'),
(150, 22, 3, 0, 7, 0, 1, '2013-04-06 18:00:00'),
(151, 22, 2, 0, 8, 0, 1, '2013-04-06 18:00:00'),
(152, 22, 1, 0, 9, 0, 1, '2013-04-06 18:00:00'),
(153, 22, 13, 0, 10, 0, 1, '2013-04-06 18:00:00'),
(154, 22, 12, 0, 11, 0, 1, '2013-04-06 18:00:00'),
(155, 23, 12, 0, 14, 0, 1, '2013-04-13 18:00:00'),
(156, 23, 11, 0, 13, 0, 1, '2013-04-13 18:00:00'),
(157, 23, 10, 0, 1, 0, 1, '2013-04-13 18:00:00'),
(158, 23, 9, 0, 2, 0, 1, '2013-04-13 18:00:00'),
(159, 23, 8, 0, 3, 0, 1, '2013-04-13 18:00:00'),
(160, 23, 7, 0, 4, 0, 1, '2013-04-13 18:00:00'),
(161, 23, 6, 0, 5, 0, 1, '2013-04-13 18:00:00'),
(162, 24, 14, 28, 6, 34, 1, '2013-04-20 18:00:00'),
(163, 24, 5, 0, 7, 0, 1, '2013-04-20 18:00:00'),
(164, 24, 4, 0, 8, 0, 1, '2013-04-20 18:00:00'),
(165, 24, 3, 0, 9, 0, 1, '2013-04-20 18:00:00'),
(166, 24, 2, 0, 10, 0, 1, '2013-04-20 18:00:00'),
(167, 24, 1, 0, 11, 0, 1, '2013-04-20 18:00:00'),
(168, 24, 13, 0, 12, 0, 1, '2013-04-20 18:00:00'),
(169, 25, 13, 0, 14, 0, 1, '2013-04-27 18:00:00'),
(170, 25, 12, 0, 1, 0, 1, '2013-04-27 18:00:00'),
(171, 25, 11, 0, 2, 0, 1, '2013-04-27 18:00:00'),
(172, 25, 10, 0, 3, 0, 1, '2013-04-27 18:00:00'),
(173, 25, 9, 0, 4, 0, 1, '2013-04-27 18:00:00'),
(174, 25, 8, 0, 5, 0, 1, '2013-04-27 18:00:00'),
(175, 25, 7, 0, 6, 0, 1, '2013-04-27 18:00:00'),
(176, 26, 14, 19, 7, 29, 1, '2013-05-04 18:00:00'),
(177, 26, 6, 29, 8, 32, 1, '2013-05-04 18:00:00'),
(178, 26, 5, 32, 9, 21, 1, '2013-05-04 18:00:00'),
(179, 26, 4, 42, 10, 24, 1, '2013-05-04 18:00:00'),
(180, 26, 3, 29, 11, 19, 1, '2013-05-04 18:00:00'),
(181, 26, 2, 27, 12, 32, 1, '2013-05-04 18:00:00'),
(182, 26, 1, 27, 13, 18, 1, '2013-05-04 18:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `season`
--

CREATE TABLE IF NOT EXISTS `season` (
  `season_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `season` varchar(100) COLLATE utf8_polish_ci NOT NULL,
  PRIMARY KEY (`season_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `season`
--

INSERT INTO `season` (`season_id`, `season`) VALUES
(1, '2012/2013'),
(2, '2011/2012');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `user_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fname` varchar(100) COLLATE utf8_polish_ci NOT NULL,
  `sname` varchar(100) COLLATE utf8_polish_ci NOT NULL,
  `email` varchar(100) COLLATE utf8_polish_ci NOT NULL,
  `pass` varchar(40) COLLATE utf8_polish_ci NOT NULL,
  `salt` varchar(128) COLLATE utf8_polish_ci NOT NULL,
  `dtime` datetime NOT NULL,
  `username` varchar(128) COLLATE utf8_polish_ci NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`user_id`, `fname`, `sname`, `email`, `pass`, `salt`, `dtime`, `username`) VALUES
(1, 'Daniel', 'Trylski', 'daniel.trylski@gmail.com', '4a83630dec13e5d530a1c87e5bd2a237b95a0d37', 'f64932c80ab8dc9de8b41fe8dfe832a975def522', '2012-10-29 21:33:53', 'admin');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `foto`
--
ALTER TABLE `foto`
  ADD CONSTRAINT `foto_ibfk_1` FOREIGN KEY (`article_id`) REFERENCES `article` (`article_id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
