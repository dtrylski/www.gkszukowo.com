<?php
class My_League_Player{
    private $_id;
    private $_fname;
    private $_sname;
    private $_position;
    private $_email;
    private $_height;
    private $_games;
    private $_number;
    private $_status;
    private $_season_id;
    private $_season;

    public function __construct($id,$fname,$sname,$position,$email,$height,$games,$number,$status,$season_id,$season){
        $this->_id = $id;
        $this->_fname = $fname;
        $this->_sname = $sname;
        $this->_position = $position;
        $this->_email = $email;
        $this->_height = $height;
        $this->_games = $games;
        $this->_number = $number;
        $this->_status = $status;
        $this->_season_id = $season_id;
        $this->_season = $season;
    }
    public function getId(){
        return $this->_sname;
    }
    public function getName(){
        return $this->_sname .' '. $this->_fname;
    }
    public function getHeight(){
        return $this->_height;
    }
}
?>
