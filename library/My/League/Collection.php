<?php

class My_League_Collection implements IteratorAggregate {

    private $_members = array();    // elementy kolekcji
    private $_onload;               // funkcja zwrotna
    private $_isLoaded = false;     // flaga okrelaj¹ca, czy funkcja zwrotna

    // zosta³a ju¿ wywo³ana

    public function addItem($obj, $key = null) {
        $this->_checkCallback();     // _checkCallback zdefiniowano nieco później
        if ($key) {
            if (isset($this->_members[$key])) {
                echo "Klucz .$key. jest już zajęty!";
            } else {
                $this->_members[$key] = $obj;
            }
        } else {
            $this->_members[] = $obj;
        }
    }

    public function removeItem($key) {
        $this->_checkCallback();

        if (isset($this->_members[$key])) {
            unset($this->_members[$key]);
        } else {
            throw new KeyInvalidException("B³êdny klucz \"$key\"!");
        }
    }

    public function getItem($key) {
        $this->_checkCallback();

        if (isset($this->_members[$key])) {
            return $this->_members[$key];
        } else {
            throw new KeyInvalidException("B³êdny klucz \"$key\"!");
        }
    }

    public function keys() {
        $this->_checkCallback();
        return array_keys($this->_members);
    }

    public function length() {
        $this->_checkCallback();
        return sizeof($this->_members);
    }

    public function exists($key) {
        $this->_checkCallback();
        return (isset($this->_members[$key]));
    }

    public function getIterator() {
        $this->_checkCallback();
        return new CollectionIterator($this);
    }

    /**
     * Ta metoda pozwala na zdefiniowanie funkcji,
     * któr¹ nale¿y wywo³aæ, aby wype³niæ kolekcjê.
     * Jedynym parametrem tej funkcji powinna byæ
     * kolekcja do wype³nienia.
     */
    public function setLoadCallback($functionName, $objOrClass = null) {
        if ($objOrClass) {
            $callback = array($objOrClass, $functionName);
        } else {
            $callback = $functionName;
        }

        // sprawdzenie, czy funkcjê zwrotn¹ da siê wywo³aæ

        if (!is_callable($callback, true, $callableName)) {
            throw new Exception("Funkcja zwrotna $callableName nieprawidłowa!");
            return false;
        }
        $this->_onload = $callback;
    }

    /**
     * Sprawdzenie, czy funkcja zwrotna zosta³a zdefiniowana,
     * a jeli tak, czy zosta³a ju¿ wywo³ana. Jeli nie,
     * zostaje ona wywo³ana.
     */
    private function _checkCallback() {
        if (isset($this->_onload) && !$this->_isLoaded) {
            $this->_isLoaded = true;
            call_user_func($this->_onload, $this);
        }
    }

}

?>
