<?php

class My_League_Schedule {

    private $allrounds;
    private $teams;
    private $schedule;

    public function __construct($schedule = 0, $allrounds = 0, $teams = 0) {
        $allrounds = $this->allrounds;
        $teams = $this->teams;
        $schedule = $this->schedule;
    }

    function init() {
        $games = new Application_Model_DbTable_Schedule();
        $schedule = $games->getGames();
        return $schedule;
    }

    /* pobieramy wszystkie mecze i układamy wg kolejek */

    public function getAllrounds() {
        $schedule = $this->init();
//        die(var_dump($schedule));
        foreach ($schedule as $r) {
            if (!isset($allrounds[$r['round_id']]))
                $allrounds[$r['round_id']] = array();
            $allrounds[$r['round_id']][$r['game_id']] = $r;
        }
//        die(var_dump($allrounds));
        return $allrounds;
    }

    /* sortujem rundy wg klucz wartość */

    public function getkeyRounds() {
        foreach (array_keys($this->getAllrounds()) as $k => $v) {
            $tmp_arr[$v] = $v;
        }
        $allrounds = $tmp_arr;
        return $allrounds;
    }

    /* pobieramy wszystkie drużyny z tabeli */

    public function getTeams() {
        $schedule = $this->init();
        foreach ($schedule as $r) {
            if (!isset($teams[$r['host_id']]) && !isset($team[$r['visitor_id']])) {
                $teams[$r['host_id']] = array();
                $teams[$r['visitor_id']] = array();
            }
        }
        return $teams;
    }

    /* tworzymy tablice z drużynami  */

    public function createLeagueArr() {
        $teams = $this->getTeams();
        foreach ($teams as $k => $v) {
            $teams[$k]['points'] = 0;
            $teams[$k]['goals_scored'] = 0;
            $teams[$k]['goals_allowed'] = 0;
            $teams[$k]['wins'] = 0;
            $teams[$k]['losses'] = 0;
            $teams[$k]['ties'] = 0;
        }
//        die(var_dump($teams));
        return $teams;
    }

    /* wypełniamy tablice punkty,bramki stracone, zdobyte  */

    public function countGames() {
        $teams = $this->createLeagueArr();
        foreach ($this->init() as $g) {
            $a = $g['host_goals'];
            $b = $g['visitor_goals'];
            if (!empty($a) && !empty($b)) {
                if ($a > $b) {
                    $teams[$g['host_id']]['points'] += 2;
                    $teams[$g['host_id']]['wins']++;
                    $teams[$g['visitor_id']]['losses']++;
                    $teams[$g['host_id']]['goals_scored'] += $g['host_goals'];
                    $teams[$g['host_id']]['goals_allowed'] += $g['visitor_goals'];
                    $teams[$g['visitor_id']]['goals_scored'] += $g['visitor_goals'];
                    $teams[$g['visitor_id']]['goals_allowed'] += $g['host_goals'];
                } elseif ($a < $b) {
                    $teams[$g['visitor_id']]['points'] += 2;
                    $teams[$g['visitor_id']]['wins']++;
                    $teams[$g['host_id']]['wins']++;
                    $teams[$g['visitor_id']]['goals_scored'] += $g['visitor_goals'];
                    $teams[$g['visitor_id']]['goals_allowed'] += $g['host_goals'];
                    $teams[$g['host_id']]['goals_scored'] += $g['host_goals'];
                    $teams[$g['host_id']]['goals_allowed'] += $g['visitor_goals'];
                } else {
                    $teams[$g['host_id']]['points'] += 1;
                    $teams[$g['host_id']]['ties']++;
                    $teams[$g['visitor_id']]['ties']++;
                    $teams[$g['host_id']]['goals_scored'] += $g['host_goals'];
                    $teams[$g['host_id']]['goals_allowed'] += $g['host_goals'];
                    $teams[$g['visitor_id']]['points'] += 1;
                    $teams[$g['visitor_id']]['goals_scored'] += $g['visitor_goals'];
                    $teams[$g['visitor_id']]['goals_allowed'] += $g['visitor_goals'];
                }
            }
        }
        return $teams;
    }

    function LastNextGame() {
        $teams = $this->getTeams();
//        die(var_dump($teams));
        $schedule = $this->init();
        foreach ($schedule as $k => $v) {
//            die(var_dump($v));
            $teams[$v['host_id']][] = $v;
            $teams[$v['visitor_id']][] = $v;
        }
        $locale = new Zend_Locale('pl_PL');
        $date = new Zend_Date($locale);
        $date->now();
        $tmp_arr = array();
//        die(var_dump($teams["GKS Żukowo"]));

        foreach ($teams["GKS Żukowo"] as $k => $v) {
            if ($date->isEarlier($v['date'])) {
                $tmp_arr[0][] = $v;
            } else {
                $tmp_arr[1][] = $v;
            }
        }
//        die(var_dump($teams["GKS Żukowo"]));
//        die(var_dump($tmp_arr));
        $lastnext[0] = end($tmp_arr[1]);
        $lastnext[1] = $tmp_arr[0][0];
        $lastnext[0]['date'] = substr($lastnext[0]['date'], 0, 16);
        $lastnext[1]['date'] = substr($lastnext[1]['date'], 0, 16);
//die(var_dump($lastnext));
        return $lastnext;
    }

    function nextRound() {
        $schedule = new Application_Model_DbTable_Schedule();
        $nextround = $schedule->getNextRound();
        return $nextround;
    }

}

?>
