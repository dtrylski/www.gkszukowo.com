<?php

class My_League_Players {

    private $players;

    public function __construct() {
        $player = new Application_Model_DbTable_Player();
        $this->players = $player->getAllplayers();
        $players  = $this->players; 
        return $players;
    }

    public function playersposition() {
        $position = array();
        foreach ($this->players as $k => $v) {
            if (!isset($positions[$v['position']]))
            $position[$v['position']] = $v['position'];
            $positions[$v['position']][] = $v;
        }
        return $position;
    }
    public function getplayers(){
        return $this->players;
    }

}

?>
