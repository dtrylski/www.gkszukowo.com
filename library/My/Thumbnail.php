<?php
class My_Thumbnail
{
    public static function gdThumbnailObj($AImg, $AWidth, $AHeight)
    {
        if (!$AImg) {
            die('gd_thumbnail_obj() - $AImg error');
        }
        $AImg_X = ImageSX($AImg);
        $AImg_Y = ImageSY($AImg);

        $tmp_Y  = ($AWidth / $AImg_X) * $AImg_Y;
        $tmp_X  = ($AHeight / $AImg_Y) * $AImg_X;

        if ($tmp_Y <= $AHeight){
            $thumbnail_X = $AWidth;
            $thumbnail_Y = $tmp_Y;
        } else {
            $thumbnail_X = $tmp_X;
            $thumbnail_Y = $AHeight;
        }

        $thumbnail = ImageCreateTrueColor(
            $thumbnail_X,
            $thumbnail_Y
        );

        imageAlphaBlending($thumbnail, false);
        imageSaveAlpha($thumbnail, true);

        ImageCopyResized(
            $thumbnail, $AImg,          //miejsce docelowe, źródło
            0, 0,                       //gdzie ma trafić w miejscu docelowym
            0, 0,                       //skąd ma pochodzić ze źródła
            $thumbnail_X, $thumbnail_Y, //wymiary, jakie ma zająć w miejscu docelowym
            $AImg_X, $AImg_Y            //wymiary pobierane ze źródła
        );

        return $thumbnail;
    }

    public static function gdThumbnailFile(
        $AFileName, $AWidth, $AHeight, $destFilename, $quality = 95
    ) { 
        $img = imagecreatefromjpeg($AFileName);
        $mini = self::gdThumbnailObj($img, $AWidth, $AHeight);
        imagejpeg($mini, $destFilename, $quality);
    }
}
?>