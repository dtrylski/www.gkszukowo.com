<?php

class My_Crud_Auth_Controller extends My_Crud_Controller
{

    public function preDispatch()
    {

        $module = $this->getRequest()->getModuleName();
        $controller = $this->getRequest()->getControllerName();
        $action = $this->getRequest()->getActionName();

        $Action = new Application_Model_DbTable_Action();

        $auth = Zend_Auth::getInstance();
        if ($auth->hasIdentity()) {
            $identity = $auth->getIdentity();
        } else {
            $identity = null;
        }

        if (!$Action->userHasAccess($identity, $module, $controller, $action)) {
            return $this->_helper->redirector(
                'index',
                'auth',
                $module
            );
        }
        $this->view->identity = $identity;
    }

}