<?php

require_once 'Zend/Tool/Project/Provider/Abstract.php';
require_once 'Zend/Tool/Project/Provider/Exception.php';

class AuthProvider extends Zend_Tool_Project_Provider_Abstract
{

    public function init()
    {

    }

    public function allowActionAccess($action = 'index', $controller = 'index', $module = 'default')
    {
        $this->init();

        $ActionTable = new Application_Model_DbTable_Action();
        $select = $ActionTable->select()
            ->where('module = ?', $module)
            ->where('controller = ?', $controller)
            ->where('action = ?', $action);

        $objAction = $ActionTable->fetchRow($select);
        if (!$objAction) {
            $dane = array(
                'module'     => $module,
                'controller' => $controller,
                'action'     => $action,
                'is_secure'  => 0
            );
            $ActionTable->insert($dane);
        }
        $objAction['is_secure'] = 0;
        $objAction->save();
    }


    public function disallowActionAccess($action = 'index', $controller = 'index', $module = 'default')
    {
        $this->init();

        $ActionTable = new Application_Model_DbTable_Action();
        $select = $ActionTable->select()
            ->where('module = ?', $module)
            ->where('controller = ?', $controller)
            ->where('action = ?', $action);

        $objAction = $ActionTable->fetchRow($select);
        if (!$objAction) {
            $dane = array(
                'module'     => $module,
                'controller' => $controller,
                'action'     => $action,
                'is_secure'  => 1
            );
            $ActionTable->insert($dane);
        }
        $objAction['is_secure'] = 1;
        $objAction->save();
    }

    public function grant($username, $action = 'index', $controller = 'index', $module = 'default')
    {
        $this->init();

        $ActionTable = new Application_Model_DbTable_Action();
        $select = $ActionTable->select()
            ->where('module = ?', $module)
            ->where('controller = ?', $controller)
            ->where('action = ?', $action);

        $objAction = $ActionTable->fetchRow($select);
        if (!$objAction) {
            $dane = array(
                'module'     => $module,
                'controller' => $controller,
                'action'     => $action,
                'is_secure'  => 1
            );
            $action_id = $ActionTable->insert($dane);
        } else {
            $action_id = $objAction['action_id'];
        }


        $UserTable = new Application_Model_DbTable_User();
        $select = $UserTable->select()->where('username = ?', $username);

        $objUser = $UserTable->fetchRow($select);
        if (!$objUser) {
            $this->_registry
                ->getResponse()
                ->appendContent("Podany uzytkownik nie istnieje!");
        }


        $ahu = new Application_Model_DbTable_ActionHasUser();
        $dane = array(
            'action_id' => $action_id,
            'user_id'   => $objUser['user_id'],
        );

        try {
            $ahu->insert($dane);
        } catch (Exception $e) {
            $this->_registry
                ->getResponse()
                ->appendContent('Uzytkownik ma juz podane uprawnienia.');
        }


    }

    public function revoke($username = '', $action = 'index', $controller = 'index', $module = 'default')
    {
        $this->init();

        $ActionTable = new Application_Model_DbTable_Action();
        $select = $ActionTable->select()
            ->where('module = ?', $module)
            ->where('controller = ?', $controller)
            ->where('action = ?', $action);

        $objAction = $ActionTable->fetchRow($select);
        if (!$objAction) {
            $this->_registry
                ->getResponse()
                ->appendContent("Podana akcja nie istnieje!");
            return;
        }


        $UserTable = new Application_Model_DbTable_User();
        $select = $UserTable->select()->where('username = ?', $username);
        $objUser = $UserTable->fetchRow($select);
        if (!$objUser) {
            $this->_registry
                ->getResponse()
                ->appendContent("Podany uzytkownik nie istnieje!");
            return;
        }

        $ahu = new Application_Model_DbTable_ActionHasUser();
        $select = $ahu->select()
            ->where('action_id = ?', $objAction['action_id'])
            ->where('user_id = ?', $objUser['user_id']);
        $obj = $ahu->fetchRow($select);

        if (!$obj) {
            $this->_registry
                ->getResponse()
                ->appendContent("Podany uzytkownik nie nie ma podanego uprawnienia!");
            return;
        }

        $obj->delete();

    }


    public function setReadable($controller = 'index', $module = 'default')
    {
        $this->init();
        $this->allowActionAccess('index', $controller, $module);
        $this->allowActionAccess('show', $controller, $module);
    }

    public function setUnreadable($controller = 'index', $module = 'default')
    {
        $this->init();
        $this->disallowActionAccess('index', $controller, $module);
        $this->disallowActionAccess('show', $controller, $module);
    }


    public function grantReaderRules($username, $controller = 'index', $module = 'default')
    {
        $this->init();
        $this->grant($username, 'index', $controller, $module);
        $this->grant($username, 'show', $controller, $module);
    }

    public function revokeReaderRules($username, $controller = 'index', $module = 'default')
    {
        $this->init();
        $this->revoke($username, 'index', $controller, $module);
        $this->revoke($username, 'show', $controller, $module);
    }


    public function grantEditorRules($username, $controller = 'index', $module = 'default')
    {
        $this->init();
        $this->grant($username, 'index', $controller, $module);
        $this->grant($username, 'show', $controller, $module);
        $this->grant($username, 'createform', $controller, $module);
        $this->grant($username, 'create', $controller, $module);
        $this->grant($username, 'edit', $controller, $module);
        $this->grant($username, 'update', $controller, $module);
        $this->grant($username, 'delete', $controller, $module);
    }

    public function revokeEditorRules($username, $controller = 'index', $module = 'default')
    {
        $this->init();
        $this->revoke($username, 'index', $controller, $module);
        $this->revoke($username, 'show', $controller, $module);
        $this->revoke($username, 'createform', $controller, $module);
        $this->revoke($username, 'create', $controller, $module);
        $this->revoke($username, 'edit', $controller, $module);
        $this->revoke($username, 'update', $controller, $module);
        $this->revoke($username, 'delete', $controller, $module);
    }


    public function clear()
    {
        $this->init();
        $ahu = new Application_Model_DbTable_ActionHasUser();
        $ahu->delete('');
        $a = new Application_Model_DbTable_Action();
        $a->delete('');
    }


}

