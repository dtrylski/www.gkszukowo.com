<?php

require_once 'Zend/Tool/Project/Provider/Abstract.php';
require_once 'Zend/Tool/Project/Provider/Exception.php';

class UserProvider extends Zend_Tool_Project_Provider_Abstract
{

    public function create($username = '', $password = '')
    {

        $username = trim(strtolower($username));
        $password = trim(strtolower($password));

        if ((!$username) || (!$password)) {
            $this->_registry
                ->getResponse()
                ->appendContent("Podaj nazwe konta i haslo.");
            return;
        }

        $User = new Application_Model_DbTable_User();
        $salt = My_Salt::getSalt();

        $dane = array(
            'username' => $username,
            'password' => md5($password . $salt),
            'salt'     => $salt,
        );

        $User->createRow($dane)->save();
    }

}
