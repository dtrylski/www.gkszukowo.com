$(function(){

    $('div.article_foto a.lightbox').lightBox({
        txtImage : 'Fotka',
        txtOf: 'z',
        imageLoading: '/images/lightbox-ico-loading.gif',
        imageBtnPrev: '/images/lightbox-btn-prev.gif',
        imageBtnNext: '/images/lightbox-btn-next.gif',
        imageBtnClose: '/images/lightbox-btn-close.gif',
        imageBlank: '/images/lightbox-blank.gif'
    });

    $('div.article_foto a.lightbox').fotkapopup('mini/', 'popup/');

});